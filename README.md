# K-Modulation Application for PSB and PS
Authors:

This application uses the k-modulation method to measure the mean-beta function inside the quadrupoles of the CERN PSB and PS machines.

There are two different measurement modes: the cycle-by-cycle modulation and the periodic modulation. In the cycle-by-cycle modulation, the quadrupole strength is shifted by a user-specified value and the vartical tune is measured at different c-times (Single Cycle Measurement). The option to automatically measure for multiple cycles and different dk settings (random values within a user-specified range) is also possible (Multi Cycle Measurement). In the periodic modulation the deltak is varied periodically.

In the case of the PSB, the beta function can be used to calculate the optimal correction strengths for correcting the injection chicane beta-beating.

## Installation


## Usage


## Contributing


## License

