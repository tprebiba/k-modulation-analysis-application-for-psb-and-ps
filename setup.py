from pathlib import Path
from setuptools import setup, find_packages

HERE = Path(__file__).parent.absolute()
with (HERE / "README.md").open("rt") as fh:
	LONG_DESCRIPTION = fh.read().strip()

#from PyQt5 import uic
#with open('k_mod_app/windows/mainWindow1.py', 'w') as file:
#	uic.compileUi("k_mod_app/windows/mainWindow1.ui", file)


DEPENDENCIES = [
	"PyQt5>=5.12",
	"pyqtgraph>=0.11.0",
	'accwidgets>=1.1.1',
	#"pyqtgraph==0.11.0.post0", # k-mod-app-venv-2
	#"accwidgets @ git+https://gitlab.cern.ch/isinkare/accsoft-gui-pyqt-widgets.git@f015095176a44fdfaf2983cee8153abac3476fa5#egg=accwidgets[graph,log_console]", # k-mod-app-venv-2
	#"qt_lsa_selector @ git+https://gitlab.cern.ch/kli/qt-lsa-selector.git@ad7157dcf031ba5c9044008ba4065fb01779cbc0",]
	"numpy>=1.18.1",
	"scipy>=1.5.2",
	"pandas>=1.1.4",
	"cpymad>=1.6.2",
	"pjlsa>=0.2.4",
	"pyjapc>=2.2.1",
	"papc>=0.5.2",
	"JPype1>=1.2.1",
	"qtawesome>=0.7.2",
	"cmmnbuild-dep-manager>=2.8.0",
	#"pyjapcscout>=0.2.3"
]


setup(
	name='k_mod_app',
	version='0.9.72', # 0.9.13 # pyqtgraph=0.11.0.post0 for Ivan
	author='Tirsi Prebibaj',
	author_email='tirsi.prebibaj@cern.ch',
	description = 'A k-modulation application for the CERN PSB and PS',
	long_description=LONG_DESCRIPTION,
	url='https://gitlab.cern.ch/tprebiba/k-modulation-analysis-application-for-psb-and-ps',
	python_requires=">=3.6",
	packages=find_packages(),
	keywords='k-modulation beta-beating',
	license="MIT",
	install_requires=DEPENDENCIES,
	)
