from k_mod_app.analysis_tools import *
import numpy as np
import scipy.optimize as opt

# if true, the intensity samples will be recorded
record_bct = True

###################################################################
# Tune Algorithm LSAMakeRule
###################################################################

c_matrix = (
    # horizontal
    np.array([[0.99991100, 3.3021790],
              [-7.5700e-5, 0.9998390]]),

    # vertical
    np.array([[0.97287200, 3.2698120],
              [-0.0229030, 0.9509070]])
)


def v_matrix(k_f, k_d):
    l_3 = 0.65300
    l_f = 0.50357
    l_d = 0.43947
    # after reviewing calibration curve
    #l_3 = 0.65300 - 0.00047412911004740543 / 2 + 0.0011086159402869944 / 2
    #l_f = 0.50357 / 0.99905935
    #l_d = 0.43947 / 1.002529
    sqrt_k_f = np.sqrt(k_f)
    sqrt_k_d = np.sqrt(-k_d)

    # horizontal
    a = np.cosh(sqrt_k_d * l_d)
    b = np.sinh(sqrt_k_d * l_d) / sqrt_k_d
    c = np.sinh(sqrt_k_d * l_d) * sqrt_k_d
    d = np.cos(sqrt_k_f * l_f)
    f = np.sin(sqrt_k_f * l_f) / sqrt_k_f
    g = -np.sin(sqrt_k_f * l_f) * sqrt_k_f
    matr_h = np.array([[a * d + (a * l_3 + b) * g, a * f + (a * l_3 + b) * d],
                       [c * d + (c * l_3 + a) * g, c * f + (c * l_3 + a) * d]])

    # vertical
    a = np.cos(sqrt_k_d * l_d)
    b = np.sin(sqrt_k_d * l_d) / sqrt_k_d
    c = -np.sin(sqrt_k_d * l_d) * sqrt_k_d
    d = np.cosh(sqrt_k_f * l_f)
    f = np.sinh(sqrt_k_f * l_f) / sqrt_k_f
    g = np.sinh(sqrt_k_f * l_f) * sqrt_k_f
    matr_v = np.array([[a * d + (a * l_3 + b) * g, a * f + (a * l_3 + b) * d],
                       [c * d + (c * l_3 + a) * g, c * f + (c * l_3 + a) * d]])

    return matr_h, matr_v


def calc_q(k_f, k_d):
    tunes = ()
    for c, v in zip(c_matrix, v_matrix(k_f, k_d)):  # iterate over two-plane tuples
        #cell = v @ c
        cell = np.matmul(v, c)
        tune = (8 / np.pi) * np.arccos(cell[0, 0] * cell[1, 1] + cell[1, 0] * cell[0, 1])
        tunes += (tune,)
    return tunes


def tune_eq(qx, qy):
    target = np.array([qx, qy])
    return lambda k: np.array(calc_q(k[0], k[1])) - target

# Example
#result = opt.root(tune_eq(4.4, 4.479), x0=np.array([0.786140, -0.786140]))  # initial guess is "natural" tune
#print(result)
#k = result['x']
#print('found K')
#print(k)
#print('resulting Q')
#print(calc_q(k[0], k[1]))
#result = opt.root(tune_eq(4.4, 4.3), x0=np.array([0.786140, -0.786140]))
#print(result['x']) # gives [ 0.76344753 -0.76868102]
#print(calc_q(0.7641600195312502, -0.7667747656250004)) # gives: (4.4148732685978915, 4.2590274068708505)
#print(calc_q(0.7634494356, -0.765292189)) # gives: (4.4148732685978915, 4.2590274068708505)





class TuneRecord():

    def __init__(self, parameters, simulation_parameters, machine, output_path, ring, japc, lsa, lsa_context, bbq_device_number, record_rawDataHV=False):
        self.parameters = parameters
        self.simulation_parameters = simulation_parameters
        self.machine = machine
        self.output_path = output_path
        self.ring = ring

        self.bbq_device, self.bct_device = self.machine.get_device_names(self.ring, bbq_device_number)

        self.japc = japc
        self.lsa = lsa
        self.lsa_context = lsa_context
        self.record_rawDataHV = record_rawDataHV


    def start_tune_recording(self):
        self.japc.clearSubscriptions()
        if record_bct:
            #self.japc.subscribeParam("%s/Samples" % self.bct_device, self.bct_callback, getHeader=True) # monitoring BCT
            self.japc.subscribeParam("%s/Tuples" % self.bct_device, self.bct_callback, getHeader=True)  # monitoring BCT
        self.japc.subscribeParam("%s/Acquisition" % self.bbq_device, self.callback, getHeader=True)

        print('Subscriptions started.')
        print('-----------------------')
        self.japc.startSubscriptions()


    def stop_tune_recording(self):

        self.japc.stopSubscriptions()
        self.japc.clearSubscriptions()
        print('All subscriptions stopped and cleared.')


    def bct_callback(self, parameter, value, header):

        # check synchronization issues TBA
        self.bct_cycleStamp = datetime.timestamp(header['cycleStamp'])
        #self.bct_samples    = list(value['samples'])
        #self.bct_samples = list(value['tuples'][0]['Y']) # changed naming convention only for PyJapcScout
        #self.bct_times = list(value['tuples'][0]['X']) # changed naming convention only for PyJapcScout
        self.bct_samples = list(value['tuples'][0][1]) # changed naming convention only for PyJapc
        self.bct_times = list(value['tuples'][0][0]) # changed naming convention only for PyJapc




    def callback(self, parameter, value, header):

        if header['isFirstUpdate']:
            print('Initial call of the bbq_callback. Ignoring.')
        else:
            cycleStamp = datetime.timestamp(header['cycleStamp'])
            #estimatedTuneH = value['estimatedTuneH'].tolist()
            #estimatedTuneV = value['estimatedTuneV'].tolist()
            estimatedTuneH = value['tuneH'].tolist()
            estimatedTuneV = value['tuneV'].tolist()
            ctime = value['measStamp']
            try:
                ctime = ctime.tolist()
            except:
                # if only one measurement, BBQ gives a number instead of an array
                ctime = [float(ctime)]

            data = {'cycleStamp': cycleStamp,
                    'ctime': ctime,
                    'estimatedTuneH': estimatedTuneH,
                    'estimatedTuneV': estimatedTuneV}

            if self.record_rawDataHV:
                data['rawDataH'] = value['rawDataH'].tolist()
                data['rawDataV'] = value['rawDataV'].tolist()

            if record_bct:
                data['bct_cycleStamp'] = self.bct_cycleStamp
                data['bct_samples'] =  self.bct_samples
                data['bct_times'] = self.bct_times

            now = datetime.now()
            output_file = 'tune_record_R%s_%s.json' %(self.ring, now.strftime("%Y.%m.%d-%H.%M.%S.%f"))

            if not os.path.exists('%soutputs/%s/tune_evolution/%s' %(self.output_path, self.parameters.machine_flag, now.strftime("%Y.%m.%d"))):
                os.makedirs('%soutputs/%s/tune_evolution/%s' %(self.output_path, self.parameters.machine_flag, now.strftime("%Y.%m.%d")))
            output_path = '%soutputs/%s/tune_evolution/%s/' %(self.output_path, self.parameters.machine_flag, now.strftime("%Y.%m.%d"))
            save_data_to_json(data, output_path, output_file)

            print('Shot saved at %s'%output_file)

            return data, (output_path + output_file)



class TuneCorrection():

    def __init__(self, parameters, simulation_parameters, machine, output_path, ring, japc, lsa, lsa_context, bbq_device_number):
        self.parameters = parameters
        self.simulation_parameters = simulation_parameters
        self.machine = machine
        self.output_path = output_path
        self.ring = ring
        self.bbq_device_number = bbq_device_number

        self.bbq_device, self.bct_device = self.machine.get_device_names(self.ring, self.bbq_device_number)

        self.japc = japc
        self.lsa = lsa
        self.lsa_context = lsa_context

        self.nominal_ctimes = {}
        self.nominal_strengths = {}


    def get_deltak(self, quad_parameter_id):
        cycle = self.lsa_context
        lsa = self.lsa
        with lsa.java_api():
            from cern.lsa.client import ServiceLocator, ContextService, ParameterService, SettingService
            from cern.lsa.domain.settings import ContextSettingsRequest, Settings
            from java.util import Collections

            contextService = ServiceLocator.getService(ContextService)
            parameterService = ServiceLocator.getService(ParameterService)
            settingService = ServiceLocator.getService(SettingService)

            cycle = contextService.findStandAloneCycle(cycle)
            parameter = parameterService.findParameterByName("logical.%s/K" % quad_parameter_id)
            cycleSettings = settingService.findContextSettings(
                ContextSettingsRequest.byStandAloneContextAndParameters(cycle, Collections.singleton(parameter)))

            function = Settings.getFunction(cycleSettings, parameter)
            ctimes = np.array(function.toXArray()).tolist()
            deltak = np.array(function.toYArray()).tolist()

        return ctimes, deltak


    def set_deltak(self, ctime_list, deltak_list, quad_parameter_id, setDescription):
        try:
            cycle = self.lsa_context
            lsa = self.lsa
            with lsa.java_api():
                from cern.lsa.client import ServiceLocator, ContextService, ParameterService, TrimService
                from cern.lsa.domain.settings import TrimRequest
                from cern.accsoft.commons.value import ValueFactory

                contextService = ServiceLocator.getService(ContextService)
                parameterService = ServiceLocator.getService(ParameterService)
                trimService = ServiceLocator.getService(TrimService)

                cycle = contextService.findStandAloneCycle(cycle)
                parameter = parameterService.findParameterByName("logical.%s/K" % quad_parameter_id)

                function = ValueFactory.createFunction(
                    ctime_list,
                    deltak_list
                )

                trimRequest = TrimRequest.builder() \
                    .setContext(cycle) \
                    .addFunction(parameter, function) \
                    .setDescription(setDescription) \
                    .build()
                trimService.trimSettings(trimRequest)

        except:
            #except Exception as ex:
            print('LSA tune correction setting failed...')
            #print(ex)


    def read_nominal_strengths(self):

        self.nominal_ctimes['F'], self.nominal_strengths['F'] = self.get_deltak('BR%s.QFO'%self.ring)
        self.nominal_ctimes['D'], self.nominal_strengths['D'] = self.get_deltak('BR%s.QDE' % self.ring)


    def set_tune_correction_strengths(self, ctimes_kf, kf, ctimes_kd, kd):

        self.set_deltak(ctimes_kf, kf, 'BR%s.QFO_CORR'%self.ring, 'Tune correction trim for injection chicane tune perturbation (k-mod-app).')
        self.set_deltak(ctimes_kd, kd, 'BR%s.QDE_CORR'%self.ring, 'Tune correction trim for injection chicane tune perturbation (k-mod-app).')

    def calculate_correction_trims(self, Qx_measured_list, Qy_measured_list, Qx_measured_list_ctimes, Qy_measured_list_ctimes,
                                   option, ctimeini, ctimefin, target_model_qh, target_model_qv):

        # Option 1: initial strengths from LSA:
        #   The function will read the set-strengths from LSA. These strengths correspond to a working point of:
        #   (qh,qv) = calc_q(kf, kd)
        #   This working point is the "target" for the correction (the strengths are used as a reference)
        # Option 2: initial strengths from measured tunes:
        #   The function calculates the "mean" measured vertical and horizontal tune and it calculates the kf, kd in
        #   which these correspond. These strengths are used as a reference
        #self.read_nominal_strengths()
        #print('Nominal strengths read from LSA:')
        #print(self.nominal_strengths['F'], self.nominal_strengths['D'])
        #initial_guess = [self.nominal_strengths['F'][0], self.nominal_strengths['D'][0]]  # used only as an initial guess
        initial_guess = [0.7641600195312502, -0.7667747656250004] # reference values at 4.40, 4.30 (hardcoded from LSA)
        # initial_guess = [0.7619227227, -0.7621893848] # reference values at 4.40, 4.24 (from MAD-X)
        qx, qy = calc_q(initial_guess[0], initial_guess[1])
        print('Based on the local makerule, these strengths correspond to a working point of:')
        print('(qx,qy) = (%1.5f, %1.5f)' % (qx, qy))

        if option == 1:
            print('Tune correction based on model working point (option 1).')
            print('Target working point: (%1.5f, %1.5f)' % (target_model_qh, target_model_qv))
            result = opt.root(tune_eq(target_model_qh, target_model_qv), x0=np.array(initial_guess))
            target_ks = [result['x'][0], result['x'][1]]
            print('Target KF and KD strengths (calculated from model):')
            print(target_ks)

        elif option == 2:
            print('Tune correction based mean measured working point (option 2).')
            mask = np.where((Qx_measured_list_ctimes>ctimeini) & (Qx_measured_list_ctimes<ctimefin))
            mean_qx = np.mean(Qx_measured_list[mask])
            mean_qy = np.mean(Qy_measured_list[mask])
            print('Target working point: (%1.5f, %1.5f)' % (mean_qx, mean_qy))
            print(4 + mean_qx - int(mean_qx), 4 + mean_qy - int(mean_qy))
            result = opt.root(tune_eq(4 + mean_qx - int(mean_qx), 4 + mean_qy - int(mean_qy)), x0=np.array(initial_guess))
            target_ks = [result['x'][0], result['x'][1]]
            print('Target KF and KD strengths (calculated from model):')
            print(target_ks)

        kfcorr = []
        kdcorr = []
        for qx,qy in zip(Qx_measured_list, Qy_measured_list):
            result = opt.root(tune_eq(4+qx-int(qx), 4+qy-int(qy)), x0=np.array(target_ks))
            k = result['x']
            kfcorr.append(target_ks[0]-k[0]) # delta k
            kdcorr.append(target_ks[1]-k[1]) # delta k

        return kfcorr, kdcorr
