# PSB-specific methods
from k_mod_app.parameters import *

def get_default_parameters():

	default_parameters = Parameters(machine_flag = 'PSB',

									quad_id_static='BR.QDE3', ctime_min_static=275, ctime_max_static=280, ctime_step_static=2.0,
									single_cycle_delta_k=-0.3e-2, deltak_min=-0.75e-2, deltak_max=1.0e-2, nr_of_cycles_static=1,

									quad_id_periodic='BR.QDE3', ctime_min_periodic=275, ctime_max_periodic=280, ctime_step_periodic=2.0,
									periodic_cycle_delta_k=-0.3e-2, deltak_offset=0.0e-2, modulation_freq=1.5, 
									periodic_modulation_function_flag='sine', Q_modulation_fitting_function_flag='double_sinusoidal',

                            		quad_list=['BR.QDE3', 'BR.QDE14'])
	
	default_simulation_parameters = SimulationParameters(QH_target=4.4, QV_target=4.45,
														 qde_length=0.87894, # extracted from acc-models-psb/psb.seq, needed only for the initial guess of the periodic fitting
                                                         betay_mean_model=16.249, # approximation; needed only for the initial guess of the periodic fitting
														 quad_to_position_dict = {'BR.QDE3': 2*9.8175+6.234797+0.8789/2,
																				  'BR.QDE14': 13*9.8175+6.234797+0.8789/2}) # from acc-models-psb/psb.seq
	return default_parameters, default_simulation_parameters



def get_MADX_lattice(simulation_parameters, mad):
	'''
	Loads the PSB lattice in MAD-X
	'''

	mad.chdir('/afs/cern.ch/eng/acc-models/psb/2021/operation/k_modulation/')

	# PSB lattice at (4.40, 4.45)
	# The tune correction strengths (kBRQFCORR, kBRQDCORR) and the beta-beating correction strengths
	# (kBRQD3CORR, kBRQD14CORR) are zero
	# The BSW/KSW are unasigned internally
	mad.call('psb_inj_lhc_kmod.madx')

	# Tune matching using TWISS (madx version >= 5.06.01)
	mad.input('QH_target=%f; QV_target=%f;' % (simulation_parameters.QH_target, simulation_parameters.QV_target))
	mad.input('exec, psb_match_tunes(QH_target, QV_target)')
	#mad.input('exec, psb_match_tunes_ptc_twiss_new(QH_target, QV_target);')
	mad.input('kBRQD_ref = kBRQD;')
	mad.input('kBRQF_ref = kBRQF;')


def assign_chicane_perturbations(simulation_parameters, mad, assign_chicane_tablerow, use_BSW_alignment):
	'''
	Assigns the injection chicane perturbations (only BSW)
	'''

	mad.input('readtable, file="BSW_collapse.tfs";')
	mad.input('SETVARS,TABLE=BSWTABLE,ROW=%d;'%assign_chicane_tablerow)
	mad.input('exec, assign_BSW_strength;')
	mad.input('use_BSW_alignment = %s;'%use_BSW_alignment)
	mad.input('if (use_BSW_alignment==1){exec, assign_BSW_alignment;};')

	mad.input('QH_target=%f; QV_target=%f;'%(simulation_parameters.QH_target, simulation_parameters.QV_target))
	#mad.input('exec, psb_match_tunes(QH_target, QV_target)')


def set_model_strengths(simulation_parameters, mad, ctime):

	mad.input('kBRQD = kBRQD_ref;')
	mad.input('kBRQF = kBRQF_ref;')
	mad.input('kBRQD3CORR = 0;')
	mad.input('kBRQD14CORR = 0;')
	mad.input('kBRQDCORR = 0;')
	mad.input('kBRQFCORR = 0;')

	if ((ctime>=275) & (ctime<=280)):
		row_in_BSW_table  = int((ctime-275)*10+1)
		use_BSW_alignment = 1
		assign_chicane_perturbations(simulation_parameters, mad, row_in_BSW_table, use_BSW_alignment)
	else: 
		pass


def set_deltak(mad, quad_id, deltak):

	# add the deltak_QDE3 and deltak_QDE14 (modulation strengths)
	mad.input('k%s_new := kBRQD + kBRQDCORR + kBR%sCORR + deltak_%s;'%(quad_id.partition('.')[2], quad_id.partition('.')[2], quad_id.partition('.')[2]))
	mad.input('%s, K1 := k%s_new;'%(quad_id, quad_id.partition('.')[2]))
	mad.input('deltak_%s = %f;'%(quad_id.partition('.')[2], deltak))


def get_twiss_table(mad, centre=False):
    
	#mad.input('exec, ptc_twiss_macro(2,0,0);')
	#twtable = mad.table.ptc_twiss
	# after madx version >= 5.06.01
	mad.input('twiss, centre=%s;'%centre)
	twtable = mad.table.twiss
	return twtable


def get_device_names(ring, bbq_device_number):

	# to be confirmed
	bbq_device = 'BR%s.BQ'%ring
	bct_device = 'BR%s.BCT-ST'%ring
	return bbq_device, bct_device