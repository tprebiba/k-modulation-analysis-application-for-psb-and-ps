from papc.fieldtype import FieldType, EquationFieldType
from papc.deviceproperty import Setting, Acquisition
from papc.device import Device
from papc.system import System



def system_factory():

    # fields
    bbq_setting_fields = (
        FieldType('acqState', 'str'),
        FieldType('acqPeriod', 'float'),
        FieldType('nbOfMeas', 'int'),
        FieldType('nbOfTurns', 'int'),

        FieldType('exAdvancedTimesH', 'int'),
        FieldType('exAdvancedTimesV', 'int'),
        FieldType('exLength', 'int'),
        FieldType('chirpStartFreqH', 'float'),
        FieldType('chirpStartFreqV', 'float'),
        FieldType('chirpStopFreqH', 'float'),
        FieldType('chirpStopFreqV', 'float'),
        FieldType('exAmplitudeH', 'float'),
        FieldType('exAmplitudeV', 'float'),
        FieldType('exEnabledH', 'bool'),
        FieldType('exEnabledV', 'bool'),
        FieldType('exMode', 'str'),
        FieldType('exPattern', 'str'),
        FieldType('minSearchWindowFreqH', 'float'),
        FieldType('minSearchWindowFreqV', 'float'),
        FieldType('maxSearchWindowFreqH', 'float'),
        FieldType('maxSearchWindowFreqV', 'float'),
    )

    bbq_acq_fields = (
        FieldType('estimatedTuneH', 'list'),
        FieldType('estimatedTuneV', 'list'),
        FieldType('measStamp', 'list'),
        FieldType('doneNbOfMeas', 'int'),
    )

    # properties
    settings = Setting('Setting', bbq_setting_fields)
    acquisition = Acquisition('Acquisition', bbq_acq_fields)

    # device
    bbq_device = Device('BBQDevice', (settings, acquisition))

    bct_setting_fields = (FieldType('param1', 'str'),)
    bct_acq_fields = (
        FieldType('firstSampleTime', 'float'),
        FieldType('samples', 'list'),
    )
    settings = Setting('Setting', bct_setting_fields)
    acquisition = Acquisition('Acquisition', bct_acq_fields)
    bct_device = Device('BCTDevice', (settings, acquisition))


    # system
    system = System([bbq_device, bct_device])

    return system

# test
# from papc.interfaces.pyjapc import SimulatedPyJapc
#
# PyJapc = SimulatedPyJapc.from_simulation_factory(system_factory)
# pyjapc = PyJapc(selector="")
#
# def get_tune_from_pyjapc(parameter, value):
#     print(parameter + ' value')
#     print(value)
#
# pyjapc.subscribeParam('BBQDevice/Acquisition#estimatedTuneV', get_tune_from_pyjapc)
# pyjapc.startSubscriptions()
# pyjapc.setParam('BBQDevice/Acquisition#estimatedTuneV', 10, checkDims=False)