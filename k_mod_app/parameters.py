from collections import namedtuple

Parameters = namedtuple('Parameters', 
						['machine_flag',
						
						 'quad_id_static', 'ctime_min_static', 'ctime_max_static', 'ctime_step_static',
						 'single_cycle_delta_k', 'deltak_min', 'deltak_max', 'nr_of_cycles_static', 

						 'quad_id_periodic', 'ctime_min_periodic', 'ctime_max_periodic', 'ctime_step_periodic',
						 'periodic_cycle_delta_k', 'deltak_offset', 'modulation_freq',
						 'periodic_modulation_function_flag', 'Q_modulation_fitting_function_flag',
						 
						 'quad_list'])

SimulationParameters = namedtuple('SimulationParameters', 
								  ['QH_target', 'QV_target', 'qde_length', 'betay_mean_model',
								   'quad_to_position_dict'])