from k_mod_app.analysis_tools import *
from scipy.interpolate import griddata

class Correction():

	def __init__(self, parameters, simulation_parameters, machine, output_path):

		self.parameters = parameters
		self.simulation_parameters = simulation_parameters
		self.machine = machine
		self.output_path = output_path



	def create_Madx_instance(self):
		
		from cpymad.madx import Madx
		mad = Madx()
		mad.options.echo = False
		mad.options.info = False
		mad.warn = False		

		return mad




	def get_error_response_matrix_from_madx(self, filepath):
		'''
		Input: correction matrix file path
		Function: loads the pre-defined error response matrix; separate file for the different QH (because of file size)
		Returns: MAD-X table
		'''

		print('Reading error response matrix...')
		mad = self.create_Madx_instance()
		correction_matrix_file = filepath

		myQHTunes = [4.45, 4.40, 4.35, 4.30, 4.25, 4.20, 4.15]  # as scanned in the correction_matrix_file
		index2correctionRow = myQHTunes.index(min(myQHTunes, key=lambda x: abs(x - self.simulation_parameters.QH_target)))
		myQHTunes_str = {4.45: '45', 4.40: '40', 4.35: '35', 4.30: '30', 4.25: '25', 4.20: '20', 4.15: '15'}

		# works, but to be reviewed
		mad.input('readtable, file="%s_qh%s.tfs";'%(correction_matrix_file[0:-4], myQHTunes_str[myQHTunes[index2correctionRow]]))
		correction_tbl = mad.table.correctionmatrix

		mad.input('exit;')

		return correction_tbl




	def interpolate_betas(self, correction_tbl, quad_id, data_analysis):
		'''
		Calculation of the optimal correction strenghts by interpolating the measured betas to the pre-defined error response matrix
		Output is saved at ../outputs/correction_output/
		'''

		kd_trim_correction_model  = correction_tbl.KBRQDCORR_TRM
		kd_delta_correction_model = correction_tbl.KBRQDCORR
		kf_delta_correction_model = correction_tbl.KBRQFCORR
		Qy_model                  = correction_tbl.QY
		#Qx_model                  = correction_tbl.QX # TBA
		betay_qde_model           = {}
		betay_qde_model['BR.QDE3']   = correction_tbl.BETAY_AVG_QDE3
		betay_qde_model['BR.QDE14']  = correction_tbl.BETAY_AVG_QDE14

		points = np.array([betay_qde_model[quad_id], Qy_model]).T

		kd_trim_correction_arr = []
		kd_delta_correction_arr = []
		kf_delta_correction_arr = []
		for i in range(len(data_analysis['ctimes'])):
			print('Calculating correction for C%1.1f'%data_analysis['ctimes'][i])
			kd_trim_correction = griddata(points, kd_trim_correction_model,
										  (data_analysis['betay_meas'][i], data_analysis['Qy_measfit'][i] + np.floor(self.simulation_parameters.QV_target)),
										  method='cubic')
			kd_delta_correction = griddata(points, kd_delta_correction_model,
										   (data_analysis['betay_meas'][i], data_analysis['Qy_measfit'][i] + np.floor(self.simulation_parameters.QV_target)),
										   method='cubic')
			kf_delta_correction = griddata(points, kf_delta_correction_model,
										   (data_analysis['betay_meas'][i], data_analysis['Qy_measfit'][i] + np.floor(self.simulation_parameters.QV_target)),
										   method='cubic')

			kd_trim_correction_arr.append(float(kd_trim_correction))
			kd_delta_correction_arr.append(float(kd_delta_correction))
			kf_delta_correction_arr.append(float(kf_delta_correction))

		# old way; to be reviewed
		'''
		myQHTunes           = [4.45, 4.40, 4.35, 4.30, 4.25, 4.20, 4.15] # as scanned in the correction_matrix_file
		index2correctionRow = myQHTunes.index(min(myQHTunes, key=lambda x:abs(x-self.simulation_parameters.QH_target)))
		points              = np.array([betay_qde_model[quad_id][(index2correctionRow*2652):((index2correctionRow+1)*2652)], Qy_model[(index2correctionRow*2652):((index2correctionRow+1)*2652)]]).T

		kd_trim_correction_arr  = []
		kd_delta_correction_arr = []
		kf_delta_correction_arr = []
		for i in range(len(data_analysis['ctimes'])):

			kd_trim_correction  = griddata(points, kd_trim_correction_model[(index2correctionRow*2652):((index2correctionRow+1)*2652)], 
								 		   (data_analysis['betay_meas'][i], data_analysis['Qy_measfit'][i]+np.floor(self.simulation_parameters.QV_target)),
										   method='cubic')
			kd_delta_correction = griddata(points, kd_delta_correction_model[(index2correctionRow*2652):((index2correctionRow+1)*2652)], 
										   (data_analysis['betay_meas'][i], data_analysis['Qy_measfit'][i]+np.floor(self.simulation_parameters.QV_target)),
										   method='cubic')
			kf_delta_correction = griddata(points, kf_delta_correction_model[(index2correctionRow*2652):((index2correctionRow+1)*2652)], 
										   (data_analysis['betay_meas'][i], data_analysis['Qy_measfit'][i]+np.floor(self.simulation_parameters.QV_target)),
										   method='cubic')

			kd_trim_correction_arr.append(float(kd_trim_correction))
			kd_delta_correction_arr.append(float(kd_delta_correction))
			kf_delta_correction_arr.append(float(kf_delta_correction))
		'''

		correction = {'correction': {'kd_trim_correction' : list(kd_trim_correction_arr),
									 'kd_delta_correction': list(kd_delta_correction_arr),
									 'kf_delta_correction': list(kf_delta_correction_arr)}}
		data_correction = {**data_analysis, **correction}

		if not os.path.exists('%soutputs/%s/static/correction_output/%s'%(self.output_path, self.parameters.machine_flag, datetime.now().strftime("%Y.%m.%d"))): 
			os.makedirs('%soutputs/%s/static/correction_output/%s'%(self.output_path, self.parameters.machine_flag, datetime.now().strftime("%Y.%m.%d")))
		output_path = '%soutputs/%s/static/correction_output/%s/'%(self.output_path, self.parameters.machine_flag, datetime.now().strftime("%Y.%m.%d"))
		output_file = '%s_correction_%s.json'%(quad_id, datetime.now().strftime("%Y.%m.%d-%H.%M.%S.%f"))
		save_data_to_json(data_correction,output_path,output_file)
		
		return data_correction