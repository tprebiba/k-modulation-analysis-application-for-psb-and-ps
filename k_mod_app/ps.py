# PSB-specific methods
from k_mod_app.parameters import *

def get_default_parameters():
	
	default_parameters = Parameters(machine_flag = 'PS',

									quad_id_static='PR.QDN00', ctime_min_static=275, ctime_max_static=280, ctime_step_static=2.0,
									single_cycle_delta_k=-0.3e-2, deltak_min=-0.75e-2, deltak_max=1.0e-2, nr_of_cycles_static=1,

									quad_id_periodic='PR.QDN00', ctime_min_periodic=275, ctime_max_periodic=280, ctime_step_periodic=2.0,
									periodic_cycle_delta_k=-0.3e-2, deltak_offset=0.0e-2, modulation_freq=1.5, 
									periodic_modulation_function_flag='sine', Q_modulation_fitting_function_flag='double_sinusoidal',
									
									quad_list=['PR.QDN00', 'PR.QDN10', 'PR.QDN36', 'PR.QDN40', 'PR.QDN46', 'PR.QDN50',
												'PR.QDN68', 'PR.QDN72', 'PR.QDN78', 'PR.QDN82', 'PR.QDN86', 'PR.QDN90',
												'PR.QDN96', 'PR.QDW06', 'PR.QDW18', 'PR.QDW22', 'PR.QDW28', 'PR.QDW32',
												'PR.QDW56', 'PR.QDW60', 'PR.QFN05', 'PR.QFN09', 'PR.QFN21', 'PR.QFN35',
												'PR.QFN39', 'PR.QFN45', 'PR.QFN49', 'PR.QFN55', 'PR.QFN67', 'PR.QFN71',
												'PR.QFN77', 'PR.QFN81', 'PR.QFN85', 'PR.QFN89', 'PR.QFN95', 'PR.QFN99',
												'PR.QFW17', 'PR.QFW27', 'PR.QFW31', 'PR.QFW59'])

	default_simulation_parameters = SimulationParameters(QH_target=4.4, QV_target=4.45,
														 qde_length=0.12, # extracted from acc-models-psb/psb_ss.seq, needed only for the initial guess of the periodic fitting
														 betay_mean_model=20, # approximation; needed only for the initial guess of the periodic fitting
														 quad_to_position_dict={'PR.QDN00': 623.801658, 'PR.QDN10': 58.315008, 'PR.QDN36': 222.797818, 'PR.QDN40': 246.810558,
																				'PR.QDN46': 285.629668, 'PR.QDN50': 309.642408, 'PR.QDN68': 423.299738, 'PR.QDN72': 448.712478,
																				'PR.QDN78': 486.131588, 'PR.QDN82': 511.544328, 'PR.QDN86': 536.957068, 'PR.QDN90': 560.969808,
																				'PR.QDN96': 599.788918, 'PR.QDW06': 34.302618, 'PR.QDW18': 109.140838, 'PR.QDW22': 134.553578,
																				'PR.QDW28': 171.972688, 'PR.QDW32': 197.385428, 'PR.QDW56': 348.461868, 'PR.QDW60': 372.474608,
																				'PR.QFN05': 26.899083, 'PR.QFN09': 52.311823, 'PR.QFN21': 128.550043, 'PR.QFN35': 215.394633,
																				'PR.QFN39': 240.807373, 'PR.QFN45': 278.226483, 'PR.QFN49': 303.639223, 'PR.QFN55': 341.058333,
																				'PR.QFN67': 417.296553, 'PR.QFN71': 442.709293, 'PR.QFN77': 480.128403, 'PR.QFN81': 505.541143,
																				'PR.QFN85': 529.553883, 'PR.QFN89': 554.966623, 'PR.QFN95': 592.385733, 'PR.QFN99': 617.798473,
																				'PR.QFW17': 103.137653, 'PR.QFW27': 165.969503, 'PR.QFW31': 191.382243, 'PR.QFW59': 366.471423})
	return default_parameters, default_simulation_parameters


def get_MADX_lattice(simulation_parameters, mad):
	'''
	Loads the PS lattice in MAD-X
	'''
	# From my public afs: runs locally...
	#mad.chdir('/afs/cern.ch/user/t/tprebiba/public/PS_stuff/ps_old/2021/scenarios/lhc_proton/0_injection/output')
	#mad.chdir('/afs/cern.ch/user/t/tprebiba/public/PS_stuff/ps_from_afseng/2021/scenarios/lhc/0_injection/')  # gives negative drift!
	#mad.chdir('/afs/cern.ch/user/t/tprebiba/public/PS_stuff/acc-models-ps/scenarios/lhc/0_injection/') # as copied from gitlab; permission errors on the CCC machines
	#mad.chdir('/afs/cern.ch/user/t/tprebiba/public/PS_stuff/k_modulation/lhc_injection_scenario/') # it cannot run through the CCC machines....
	mad.chdir('/user/psbop/ABP_studies/Tirsi/PS_lattice/k_modulation/lhc_injection_scenario/') # to give permission to the CCC machines (?)

	# From the official PS optics repo
	# mad.chdir('/afs/cern.ch/eng/acc-models/ps/2021/scenarios/lhc/0_injection/') # gives negative drift on the madx version of the CCC machines ?

	mad.call('ps_inj_lhc.madx')


def set_model_strengths(simulation_parameters, mad, ctime):

	pass


def set_deltak(mad, quad_id, deltak):
	
	mad.input('k%s_new := k%s + deltak_%s;'%(quad_id.partition('.')[2], quad_id.partition('.')[2], quad_id.partition('.')[2]))
	mad.input('%s, K1 := k%s_new;'%(quad_id, quad_id.partition('.')[2]))
	mad.input('deltak_%s = %f;'%(quad_id.partition('.')[2], deltak))


def get_twiss_table(mad):

    # TBA: twiss or ptc_twiss?
	#mad.input('exec, ptc_twiss_macro(2,0,0);') # for acc-models-ps
	#twtable = mad.table.ptc_twiss
	#mad.input('exec, twiss;')
	mad.input('twiss;')
	twtable = mad.table.twiss
	return twtable

def get_device_names(ring, bbq_device_number):

	# to be confirmed
	if bbq_device_number == 0:
		bbq_device = 'PR.BQS69'
	elif bbq_device_number == 1:
		bbq_device = 'PR.BQL72'
	bct_device = 'PR.BCT-ST'
	return bbq_device, bct_device
