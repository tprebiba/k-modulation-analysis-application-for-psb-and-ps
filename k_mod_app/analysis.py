from k_mod_app.analysis_tools import *
from cpymad.madx import Madx
#import threading


class Analysis():
	

	def load_datafiles(self, datafiles):
		'''
		Loads json files into dictionaries
		Output example:
		data_quad_dict = {'BR.QDE3':
							{'file_name1': {json content}
							 'file_name2': {json content}}
						  }
		'''
		
		quads = set([])
		for datafile in datafiles: 
			quads.add(datafile.split("/")[-1].split("_")[0])

		data_quad_dict = {}
		for quad in quads:

			data_quad_dict[quad] = {}

			datafiles_quad = [datafile for datafile in datafiles if datafile.split("/")[-1].split("_")[0]==quad]

			for datafile in datafiles_quad:
				with open(datafile, 'r') as fid:
					data_quad_dict[quad][datafile.split('/')[-1]] = json.load(fid)
		
		return data_quad_dict


	def update_master_table_static(self, master_table, data):

		df = pd.DataFrame.from_dict({
			**{'file': [data['metadata']['file']]*len(data['measurement']['ctime']),
			   'quad': [data['metadata']['quad']]*len(data['measurement']['ctime'])},
			**data['measurement']
		})

		master_table = master_table.append(df, ignore_index = True)

		return master_table


	def update_master_table_periodic(self, master_table, data):

		df = pd.DataFrame([{
			**data['metadata'],
			**data['measurement']
		}])

		master_table = master_table.append(df, ignore_index = True)

		return master_table


	def apply_beta_fit(self, master_table, parameters, simulation_parameters, output_path, plane, z_score_threshold,
					   minyval, maxyval, minxval, maxxval, logger):
		'''
		Input: reshaped dataframe of raw data
		Function: applies the non-linear fit to the strength-tune pairs of the reshaped dataframe for all ctimes. Expands the dataframe with all 
		the parameters of the fit.
		Output: expanded dataframe
		'''
		print('Entering Analysis().apply_beta_fit function.')
		logger.info('Entering Analysis().apply_beta_fit function.')
		quads = np.unique(master_table['quad'].values)
		data_analysis_quad_dict = {}
		output_files = {}
		for quad in quads:
			quad_mask = np.where(master_table['quad'].values == quad)

			betay_meas = []
			betay_meas_err = []
			Qy_measfit = []
			Qy_measfit_err = []
			x_fromfit = []
			y_fromfit = []
			x_deleted = []
			y_deleted = []
			datafiles_list = []

			ctimes = sorted(np.unique(master_table['ctime'].values[quad_mask]))
			ctimes = [float(ctime) for ctime in ctimes] # .json complains about int64?
			print('Applying beta fit (analysis_tools/fit_beta_function) for:')
			logger.info('Applying beta fit (analysis_tools/fit_beta_function) for:')
			for ctime in ctimes:
					print('C%1.1f'%ctime)
					#threading.Thread(target=lambda: logger.info('C%1.1f'%ctime), args=()).start()
					logger.info('C%1.1f' % ctime)


					ctime_mask = np.where((master_table['ctime'].values == ctime) & (master_table['quad'].values == quad))

					res = fit_beta_function(master_table['deltak'].values[ctime_mask],
											simulation_parameters.qde_length,
											master_table['Q%s_meas'%plane].values[ctime_mask],
											plane, z_score_threshold, minyval, maxyval, minxval, maxxval)

					betay_meas.append(res[0])
					betay_meas_err.append(res[2])
					Qy_measfit.append(res[1])
					Qy_measfit_err.append(res[3])
					x_fromfit.append(list(res[5]))
					y_fromfit.append(list(res[4]))
					x_deleted.append(list(res[6]))
					y_deleted.append(list(res[7]))
					datafiles_list.append(list(master_table['file'].values[ctime_mask]))

					print('Ok.')

			analysis = {'files': datafiles_list,
						'plane': plane,
						'ctimes': ctimes,
						'betay_meas': betay_meas,
						'betay_meas_err': list(betay_meas_err),
						'Qy_measfit': list(Qy_measfit),
						'Qy_measfit_err': list(Qy_measfit_err),
						'x_fromfit': list(x_fromfit),
						'y_fromfit': list(y_fromfit),
						'x_deleted': list(x_deleted),
						'y_deleted': list(y_deleted)}
			data_analysis_quad_dict[quad.split('_')[0]] = analysis

			print('Saving...')
			logger.info('Saving...')
			if not os.path.exists('%soutputs/%s/static/beta_output/%s' % (output_path, parameters.machine_flag, datetime.now().strftime("%Y.%m.%d"))):
				os.makedirs('%soutputs/%s/static/beta_output/%s' % (output_path, parameters.machine_flag, datetime.now().strftime("%Y.%m.%d")))

			output_path = '%soutputs/%s/static/beta_output/%s/' % (output_path, parameters.machine_flag, datetime.now().strftime("%Y.%m.%d"))
			output_file = '%s_beta_fit_%s.json' % (quad, datetime.now().strftime("%Y.%m.%d-%H.%M.%S.%f"))
			output_files[quad] = output_path + output_file
			save_data_to_json(analysis, output_path, output_file)
			print('Saved.')
			logger.info('Saved.')

		return data_analysis_quad_dict, output_files


	def apply_periodic_fit(self, data, parameters, simulation_parameters, plane, Q_target, z_score_threshold):
		'''
		Applies a sinusoidal fit to the tune modulation
		'''
		
		#ctime_arr_norm      = (np.array(data['measurement']['ctime']) - min(data['measurement']['ctime']))/(max(data['measurement']['ctime'])
		#																									-min(data['measurement']['ctime']))
		#ctime_arr_norm = (np.array(data['ctime']) - min(data['ctime'])) / (parameters.ctime_max_periodic-parameters.ctime_min_periodic)
		ctime_arr_norm      = (np.array(data['ctime']) - min(data['ctime']))/(float(data['tmax_set'])-min(data['ctime'])) # to fix the bug with the normalization of the ctime range
		ctime_arr_norm_freq = ctime_arr_norm*data['dk_freq']
		Qy_arr              = np.atleast_1d(data['Q%s_meas'%plane])

		(popt, perr, x_fromperiodicfit, y_fromperiodicfit) = fit_periodic_function(ctime_arr_norm_freq, Qy_arr, 
									   	     				 parameters.Q_modulation_fitting_function_flag,
										     				 Q_target,
										     				 simulation_parameters.betay_mean_model, 
										     				 simulation_parameters.qde_length, 
										     				 data['dk'], z_score_threshold)

		return (popt, perr, x_fromperiodicfit, y_fromperiodicfit)


	def get_quad_avg_betas_model(self, machine, simulation_parameters, ctimes, quadname, plane):
		'''
		Calculates the model mean beta function inside the specified quadrupole for the given ctimes
		'''

		print('Instantiating cpymad.')
		mad = Madx()
		mad.options.echo = False
		mad.options.info = False
		mad.warn = False
		mad.options.warn = False
		print('Loading lattice')
		machine.get_MADX_lattice(simulation_parameters, mad) # the tune matching is passed through the simulation_parameters

		quad_avg_betas_model = []
		print('Mean beta function for...')
		for ctime in ctimes:
			print('C%1.1f'%ctime)
			machine.set_model_strengths(simulation_parameters, mad, ctime)
			twtable = machine.get_twiss_table(mad)
			quad_avg_betas_model.append(get_meanbeta(quadname.lower(), twtable, plane))

		return quad_avg_betas_model


	def get_model_optics(self, machine, simulation_parameters, ctime, plane, centre=False):
		'''
		Calculates the model optics for a specific ctime
		'''

		mad = Madx()
		mad.options.echo = False
		mad.options.info = False
		mad.warn = False
		mad.options.warn = False
		machine.get_MADX_lattice(simulation_parameters,mad)
		machine.set_model_strengths(simulation_parameters, mad, ctime)
		twtable = machine.get_twiss_table(mad, centre=centre)

		return twtable['s'], twtable['bet%s'%plane], twtable['alf%s'%plane]
