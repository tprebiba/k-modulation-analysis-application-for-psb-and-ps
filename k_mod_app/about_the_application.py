from PyQt5.QtWidgets import *

def about_the_application():

	msg = QMessageBox()

	msg.setStyleSheet("QLabel{min-width: 650px; min-height: 60px; font-size: 13px;}")

	msg.setWindowTitle("About the application")
	msg.setText("How to use this application:")
	msg.setInformativeText("This application uses the k-modulation method to measure the mean-beta function inside the quadrupoles of the CERN PSB (BR.QDE3 and BR.QDE14) and PS (LEQs).\n"
							" \n"
							"There are two different measurement modes: the cycle-by-cycle modulation and the periodic modulation.\n"
							"    In the cycle-by-cycle modulation, the quadrupole strength is shifted by a user-specified value and the tune is measured at different Ctimes (Single Cycle Measurement) using the BBQ device. " 
						    "The option to automatically measure for multiple cycles and different dk settings (random values within a user-specified range) is also possible (Multi Cycle Measurement). "
							"A threshold in the error of the fitted beta can be specified to automatically stop the multi-cycle measurement when this threshold is reached.\n"
							"    In the periodic modulation the deltak is varied periodically. The frequency, aplitude and range of the modulation is specified by the user. The tune is acquired from the BBQ.\n"
						    "    Both cases monitor the intensity at the last measured Ctime. In case of losses, the user can drop the measurements that the intensity was below the specified threshold. " 
						    "All the measurements are saved externally in the output directory for offline analysis.\n"
							"\n"
							"The beta function and the nominal tune is reconstructed for all Ctimes by fitting the tune-kick points with the non-linear expected dependency (1st order approximation in dk and valid for a thick quadrupole). "
						    "The min/max cut-off tunes and the z-score drop the outliers. The optics are compared to the model values (calculated for the model working point).\n"
							" \n"
							"In the case of the PSB, the measured betas are used to calculate the optimal correction strengths for the Q-strips to correct the injection chicane beta-beating. "
						    "The tune correction tab identifies and corrects any tune perturbations (tune flattening).")

	msg.addButton(QPushButton('Got it!'), QMessageBox.YesRole)
	
	msg.exec_()
