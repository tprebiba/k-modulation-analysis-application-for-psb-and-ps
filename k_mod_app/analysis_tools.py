import glob
import os
import sys

import numpy as np
from scipy.optimize import curve_fit
from scipy import stats
from functools import partial
from scipy.interpolate import interp1d

from datetime import datetime
import pandas as pd
import json



def average_beta(K1,L,alfa0,beta0, plane):

    gamma0 = (1 + alfa0 ** 2) / beta0

    # for a defocusing quad (PSB specific): to be checked again
    if plane == 'y':
        u0 = 0.5*(1+np.sin(2*np.sqrt(K1)*L)/(2*np.sqrt(K1)*L))
        u1 = np.sin(np.sqrt(K1)*L)**2/(K1*L)
        u2 = 0.5/K1*(1-np.sin(2*np.sqrt(K1)*L)/(2*np.sqrt(K1)*L))
    elif plane == 'x':
        u0 = 0.5*(1 + np.sinh(2 * np.sqrt(K1) * L) / (2 * np.sqrt(K1) * L))
        u1 = np.sinh(np.sqrt(K1) * L) ** 2 / (K1 * L)
        u2 = -0.5 / K1 * (1 - np.sinh(2 * np.sqrt(K1) * L) / (2 * np.sqrt(K1) * L))

    # +u1 because twiss gives alfa0, beta0 at the end of the element (propagate backwards)
    avg_beta = beta0*u0 + alfa0*u1 + gamma0*u2
    return avg_beta


def Q_func(Delta_KL,beta,Q0, plane):
    # Tune shift induced from a quadrupole kick
	# Non-linear formula, valid for a thick quadrupole in first-order approximation

    Q0_2pi = 2*np.pi*Q0
    sgn = {'x': 1., 'y': -1.}
    Qint = np.floor(Q0)
    Q_func =  np.arccos(np.cos(Q0_2pi)-0.5*sgn[plane]*beta*Delta_KL*np.sin(Q0_2pi))/(2*np.pi) + Qint

    return Q_func


def get_meanbeta(quadname,twtable, plane):

    indx_quad = [i for i,n in enumerate(twtable['name']) if '%s:1'%quadname in n]
    alfa0 = twtable['alf%s'%plane][indx_quad[0]]
    beta0 = twtable['bet%s'%plane][indx_quad[0]]
    l_quad = twtable['l'][indx_quad[0]]
    kquad = twtable['k1l'][indx_quad[0]] / l_quad
    #beta_mean = average_beta(-kquad,l_quad,alfa0,beta0)
    beta_mean = average_beta(abs(kquad),l_quad,alfa0,beta0, plane) # modified 01/09/2020
    return beta_mean


def fit_beta_function(deltak_arr, l_quad, Q_arr, plane, z_score_threshold, minyval, maxyval, minxval, maxxval):
    #print('Entering analysis_tools/fit_beta_function.')
    #print('np.atleast_1d')
    x_measured = np.atleast_1d(deltak_arr)
    y_measured = np.atleast_1d(Q_arr)

    # 1st condition
    #print('Tune threshold condition.')
    ymask = sorted(list(list(np.where((y_measured < minyval)))[0]) + list(list(np.where((y_measured > maxyval)))[0]))
    x_deleted = x_measured[ymask]
    y_deleted = y_measured[ymask]
    x_measured = np.delete(x_measured, ymask)
    y_measured = np.delete(y_measured, ymask)

    # 2nd condition
    #print('Dk threshold condition.')
    xmask = sorted(list(list(np.where((x_measured < minxval)))[0]) + list(list(np.where((x_measured > maxxval)))[0]))
    x_deleted2 = x_measured[xmask]
    y_deleted2 = y_measured[xmask]
    x_deleted = np.concatenate([x_deleted, x_deleted2])
    y_deleted = np.concatenate([y_deleted, y_deleted2])
    x_measured = np.delete(x_measured, xmask)
    y_measured = np.delete(y_measured, xmask)

    # first fit
    #print('First fit (scipy.optimize.curve_fit(partial(Q_func,plane)) )')
    Q0_guess = np.mean(y_measured[np.argsort(np.abs(x_measured))][:3])
    popt, pcov = curve_fit(partial(Q_func, plane=plane), x_measured * l_quad, y_measured, p0=[14, Q0_guess])
    y_fitted = partial(Q_func, plane=plane)(x_measured * l_quad, *popt)

    # 3rd condition
    if z_score_threshold != 0:
        z_score_abs = np.abs(stats.zscore(y_fitted - y_measured))
        zscoremask = list(np.where(z_score_abs > z_score_threshold))[0]
        x_deleted3 = x_measured[zscoremask]
        y_deleted3 = y_measured[zscoremask]
        x_deleted = np.concatenate([x_deleted, x_deleted3])
        y_deleted = np.concatenate([y_deleted, y_deleted3])
        x_measured = np.delete(x_measured, zscoremask)
        y_measured = np.delete(y_measured, zscoremask)

        # second fit
        #print('Second fit ( curve_fit(partial(Q_func,plane)) )')
        popt, pcov = curve_fit(partial(Q_func, plane=plane), x_measured * l_quad, y_measured, p0=[14, Q0_guess])
    perr = np.sqrt(np.diag(pcov))

    beta_fitted = float(popt[0])
    beta_err = float(perr[0])
    Q0_fitted = float(popt[1])
    Q0_err = float(perr[1])
    x_fitted_sorted = np.sort(x_measured)
    y_fitted = partial(Q_func, plane=plane)(x_fitted_sorted * l_quad, *popt)

    return (beta_fitted, Q0_fitted,
            beta_err, Q0_err,
            y_fitted, x_fitted_sorted,
            x_deleted, y_deleted)


#----------------------------------------------------------------------------------------------------
# Functions for periodic k-mod
#----------------------------------------------------------------------------------------------------

def periodic_function_selector(periodic_modulation_function_flag, 
							   ctime_arr_norm,
							   deltak_ampl, modulation_freq, deltak_offset):

    if periodic_modulation_function_flag == 'sine':
        deltak_arr = deltak_ampl*np.sin(modulation_freq*2*np.pi*ctime_arr_norm) + deltak_offset

    elif periodic_modulation_function_flag == 'sawtooth':
        deltak_arr = deltak_ampl*2*(ctime_arr_norm*modulation_freq - np.floor(0.5+ctime_arr_norm*modulation_freq)) + deltak_offset

    return deltak_arr


def Qy_sinusoidal(ctime_range_freq_dk_norm, A, B):
    return A*np.sin(2*np.pi*ctime_range_freq_dk_norm) + B

def Qy_doubleSinusoidal(ctime_range_freq_dk_norm, A, B, C):
    return A*(1.0 + B*np.sin(2*np.pi*ctime_range_freq_dk_norm))*np.sin(2*np.pi*ctime_range_freq_dk_norm) + C

def Qy_full(ctime_range_freq_dk_norm, p1, p2):#, p4):
    # note that:
    # p1: changes the offset of the Q-modulation oscillation
    # p2: changes the amplitude of the Q-modulation oscillation
    return 0.5/np.pi*np.arccos(p1+p2*np.sin(2*np.pi*ctime_range_freq_dk_norm))# + p4)


def fit_periodic_function(ctime_range_freq_dk_norm, Qy_arr, 
                          Qfunc2fit,QV_target, betay_model, l_quad, dk_max, z_score_threshold):
    ctime_range_freq_dk_norm = np.atleast_1d(ctime_range_freq_dk_norm)
    Qy_arr = np.atleast_1d(Qy_arr)

    if Qfunc2fit == 'simple_sinusoidal':
        p0 = [np.max(Qy_arr)-np.mean(Qy_arr),np.mean(Qy_arr)]
        popt, pcov = curve_fit(Qy_sinusoidal, ctime_range_freq_dk_norm, Qy_arr, p0=p0)
        x_fromfit = np.sort(ctime_range_freq_dk_norm)
        y_fromfit = Qy_sinusoidal(x_fromfit,*popt)
        if z_score_threshold != 0:
            y_fromfit_unsorted = Qy_sinusoidal(ctime_range_freq_dk_norm,*popt)
            z_score_abs = np.abs(stats.zscore(y_fromfit_unsorted - Qy_arr))
            zscoremask = list(np.where(z_score_abs > z_score_threshold))[0]
            x_deleted = ctime_range_freq_dk_norm[zscoremask]
            y_deleted = Qy_arr[zscoremask]
            x_measured = np.delete(ctime_range_freq_dk_norm, zscoremask)
            y_measured = np.delete(Qy_arr, zscoremask)
            popt, pcov = curve_fit(Qy_sinusoidal, x_measured, y_measured, p0=p0)
            x_fromfit = np.sort(x_measured)
            y_fromfit = Qy_sinusoidal(x_fromfit, *popt)
    
    elif Qfunc2fit == 'double_sinusoidal':
        #p0 = [0.01, 0.01, QV_target - np.floor(QV_target)]
        p0 = [np.max(Qy_arr)-np.mean(Qy_arr), 0.0, np.mean(Qy_arr)]
        popt, pcov = curve_fit(Qy_doubleSinusoidal, ctime_range_freq_dk_norm, Qy_arr, p0=p0)
        x_fromfit = np.sort(ctime_range_freq_dk_norm)
        y_fromfit = Qy_doubleSinusoidal(x_fromfit,*popt)
        if z_score_threshold != 0:
            y_fromfit_unsorted = Qy_doubleSinusoidal(ctime_range_freq_dk_norm,*popt)
            z_score_abs = np.abs(stats.zscore(y_fromfit_unsorted - Qy_arr))
            zscoremask = list(np.where(z_score_abs > z_score_threshold))[0]
            x_deleted = ctime_range_freq_dk_norm[zscoremask]
            y_deleted = Qy_arr[zscoremask]
            x_measured = np.delete(ctime_range_freq_dk_norm, zscoremask)
            y_measured = np.delete(Qy_arr, zscoremask)
            popt, pcov = curve_fit(Qy_doubleSinusoidal, x_measured, y_measured, p0=p0)
            x_fromfit = np.sort(x_measured)
            y_fromfit = Qy_doubleSinusoidal(x_fromfit, *popt)
    
    elif Qfunc2fit == 'complete_formula': # in some Qy-dk_max regimes it gives NaNs
        p0 = [np.cos(2*np.pi*QV_target),0.5*betay_model*np.sin(2*np.pi*QV_target)*dk_max*l_quad]
        popt, pcov = curve_fit(Qy_full, ctime_range_freq_dk_norm, Qy_arr, p0=p0)
        x_fromfit = np.sort(ctime_range_freq_dk_norm)
        y_fromfit = Qy_full(x_fromfit,*popt)
        if z_score_threshold != 0:
            y_fromfit_unsorted = Qy_full(ctime_range_freq_dk_norm,*popt)
            z_score_abs = np.abs(stats.zscore(y_fromfit_unsorted - Qy_arr))
            zscoremask = list(np.where(z_score_abs > z_score_threshold))[0]
            x_deleted = ctime_range_freq_dk_norm[zscoremask]
            y_deleted = Qy_arr[zscoremask]
            x_measured = np.delete(ctime_range_freq_dk_norm, zscoremask)
            y_measured = np.delete(Qy_arr, zscoremask)
            popt, pcov = curve_fit(Qy_full, x_measured, y_measured, p0=p0)
            x_fromfit = np.sort(x_measured)
            y_fromfit = Qy_full(x_fromfit, *popt)
    
    perr = np.sqrt(np.diag(pcov))

    return popt, perr, x_fromfit, y_fromfit




#----------------------------------------------------------------------------------------------------
# Other
#----------------------------------------------------------------------------------------------------

def save_data_to_json(data, path, name):
    with open(path+name, 'w') as fid:
        json.dump(data, fid, indent=4)


def trapezoidal_function(x_range, ampl,
                         x_offset_left, x_offset_right,
                         x_rampup, x_rampdown,
                         cuttoff_x_limit):
    '''
    For LSA quadrupole strength settings
    Inputs:
        x_range         (list): the range of the step function
        ampl            (int) : the amplitute of the step function
        x_offset_left   (int) : the x-length of the left-extention of the step function
        x_offset_right  (int) : the x-length of the right-extention of the step function
        x_rampup        (int) : the x-length of the 0 to ampl ramp-up of the step function
        x_rampdown      (int) : the x-length of the 0 to ampl ramp-down of the step function
        cuttoff_x_limit (int) : the minimum value that x_range can take (275 ms for the PSB).
                                If the length of the left-extention or the ramp down exceed
                                this limit, then the trapezoidal is modified accordingly.
    '''

    if x_range[0] == cuttoff_x_limit:
        x_offset_left = 0
        x_rampup = 0

    x_final = list(x_range) + [x_range[0] - x_offset_left, x_range[0] - x_offset_left - x_rampup,
                               x_range[-1] + x_offset_right, x_range[-1] + x_offset_right + x_rampdown]
    x_final = sorted(list(set(x_final)))
    y_final = [ampl] * len(x_final)
    if x_rampup != 0.0:
        y_final[0] = 0.0
    if x_rampdown != 0.0:
        y_final[-1] = 0.0

    if (x_range[0] - x_offset_left - x_rampup) >= cuttoff_x_limit:
        return x_final, y_final

    elif (x_range[0] - x_offset_left) >= cuttoff_x_limit:
        x_final[0] = cuttoff_x_limit
        y_final[0] = ampl * (cuttoff_x_limit - (x_range[0] - x_offset_left - x_rampup)) / (x_rampup)
        return x_final, y_final
    else:
        x_final[1] = cuttoff_x_limit
        y_final[1] = ampl
        x_final.pop(0)
        y_final.pop(0)
        return x_final, y_final


def interpolate_value_to_array(x_value, x_list_ref, y_list_ref):
    '''
    Given a set of points x_list and y_list, and a value x_value, this function finds the elements
    of x_list that x_value lies in between and calculates the y_value as the linear interpolation
    between these two points. The x_value and y_value are added to the x_list and y_list respectively.
    '''

    # in order not to modify the original list
    x_list = x_list_ref[:]
    y_list = y_list_ref[:]

    # uncomment if not sorted
    # x_list, y_list = zip(*sorted(zip(x_list, y_list)))
    # x_list = list(x_list)
    # y_list = list(y_list)

    # if x_value<min(x_list):
    #    slope = (y_list[1]-y_list[0])/(x_list[1]-x_list[0])
    #    y_value = slope*(x_value-x_list[0])+y_list[0]
    #    if y_value<0.0:
    #        y_value = 0.0
    #
    #    x_list.insert(0, x_value)
    #    y_list.insert(0, y_value)

    if len(x_list) > 1:
        if x_value < max(x_list):
            if x_value > min(x_list):
                for i in range(len(x_list)):
                    if x_value < x_list[i]:
                        slope = (y_list[i] - y_list[i - 1]) / (x_list[i] - x_list[i - 1])
                        y_value = slope * (x_value - x_list[i - 1]) + y_list[i - 1]

                        x_list.insert(i, x_value)
                        y_list.insert(i, y_value)
                        break
            elif x_value < min(x_list):
                x_list.insert(0, x_value)
                y_list.insert(0, 0)


        elif x_value > max(x_list):
            slope = (y_list[-1] - y_list[-2]) / (x_list[-1] - x_list[-2])
            y_value = slope * (x_value - x_list[-2]) + y_list[-2]
            if (((slope < 0) and (y_value < 0.0)) or ((slope > 0) and (y_value > 0.0))):
                y_value = 0.0

            x_list.append(x_value)
            y_list.append(y_value)

    elif len(x_list) == 1:
        x_list.append(x_value)
        y_list.append(0.0)
        x_list, y_list = (list(t) for t in zip(*sorted(zip(x_list, y_list))))

    return x_list, y_list


def add_lsa_functions(ctimes_old, deltak_old,
                      ctimes_new, deltak_new):
    '''
    Function to add two LSA functions
    '''

    ctimes_total = sorted(list(set(ctimes_old + ctimes_new)))

    if len(ctimes_old) == 1:

        if ctimes_old[0] not in ctimes_new:
            ctimes_total, deltak_total = interpolate_value_to_array(ctimes_old[0], ctimes_new, deltak_new)
        else:
            ctimes_total = ctimes_new
            deltak_total = deltak_new
        deltak_total[ctimes_total.index(ctimes_old[0])] += deltak_old[0]

    elif len(ctimes_new) == 1:

        if ctimes_new[0] not in ctimes_old:
            ctimes_total, deltak_total = interpolate_value_to_array(ctimes_new[0], ctimes_old, deltak_old)
        else:
            ctimes_total = ctimes_old
            deltak_total = deltak_old
        deltak_total[ctimes_total.index(ctimes_new[0])] += deltak_new[0]

    else:

        for ctime in ctimes_total:
            if ctime not in ctimes_old:
                ctimes_old, deltak_old = interpolate_value_to_array(ctime, ctimes_old, deltak_old)
            if ctime not in ctimes_new:
                ctimes_new, deltak_new = interpolate_value_to_array(ctime, ctimes_new, deltak_new)
        deltak_total = list(np.array(deltak_old) + np.array(deltak_new))

    return (ctimes_old, deltak_old,
            ctimes_new, deltak_new,
            ctimes_total, deltak_total)


def insert_additional_points_using_interp1d(x,y,nr_total_points):
    # makes a linear interpolation for the (x,y) data
    # interpolates nr_total_points to the data
    xnew = np.linspace(x[0], x[-1], int(nr_total_points))
    ynew = interp1d(x,y,kind='linear')(xnew)
    return xnew, ynew