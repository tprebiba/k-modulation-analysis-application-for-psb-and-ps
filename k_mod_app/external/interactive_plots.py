from PyQt5.QtCore import QTimer
import os
os.environ["ACCWIDGETS_OVERRIDE_DEPENDENCIES"] = "1" # this is to ignore pyqtgraph==0.10.0.post0 requirement from accwidgets
from accwidgets import graph as accgraph

class DataSource(accgraph.UpdateSource):

    def __init__(self, x_data, y_data):
        super().__init__()
        data = accgraph.CurveData(x_data, y_data)
        self._timer = QTimer()
        #self._timer.singleShot(0, lambda: self.new_data(data)) # changed by Tirsi on 03/03/2022
        self._timer.singleShot(0, lambda: self.send_data(data)) # changed by Tirsi on 03/03/2022

    def handle_data_model_edit(self, data: accgraph.CurveData):
        print("~~~~~~~~~~~ Received Update ~~~~~~~~~~~\n"
              f"X-values of the received curve \n{data.x}\n"
              f"Y-values of the received curve \n{data.y}")