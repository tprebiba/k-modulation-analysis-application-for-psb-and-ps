from k_mod_app.analysis_tools import *
from k_mod_app.BBQ_device import system_factory
from papc.interfaces.pyjapc import SimulatedPyJapc

# Initialize PyJapc
PyJapc = SimulatedPyJapc.from_simulation_factory(system_factory)
pyjapc = PyJapc(selector="")
BBQdevice = 'BBQDevice'

class Measurement():

	def __init__(self, parameters, simulation_parameters, machine, output_path, ring):

		self.parameters = parameters
		self.simulation_parameters = simulation_parameters
		self.machine = machine
		self.output_path = output_path
		self.ring = ring


	def set_deltak(self, ctime_list, deltak_list):
		pass


	def get_deltak(self):
		pass

	def start_subscription(self):
		print('Subscription started.')


	def stop_subscription(self):
		print('Subscription stopped.')


	def subscribe_to_acquisition_fields(self):

		pyjapc.subscribeParam('%s/Acquisition#estimatedTuneV'%BBQdevice, self.estimatedTuneV_callback)
		pyjapc.subscribeParam('%s/Acquisition#estimatedTuneH'%BBQdevice, self.estimatedTuneH_callback)
		pyjapc.subscribeParam('%s/Acquisition#measStamp'%BBQdevice, self.measStamp_callback)
		pyjapc.subscribeParam('%s/Acquisition#doneNbOfMeas'%BBQdevice, self.doneNbOfMeas_callback)

		# Ignore for now
		#pyjapc.subscribeParam('%s/Samples#firstSampleTime'%BCTdevice, self.firstSampleTime)
		#pyjapc.subscribeParam('%s/Samples#samples'%BCTdevice, self.intensitySamples)

		pyjapc.startSubscriptions()


	def estimatedTuneV_callback(self, parameter, value):
		self.estimatedTuneV = value


	def estimatedTuneH_callback(self, parameter, value):
		self.estimatedTuneH = value


	def measStamp_callback(self, parameter, value):
		self.measStamp = value


	def doneNbOfMeas_callback(self, parameter, value):
		self.doneNbOfMeas_from_pyjapc = value


	def cycle_static_measurement(self, deltak):

		# Send strengths to LSA
		ctime_list = np.arange(self.parameters.ctime_min_static, self.parameters.ctime_max_static,
							   float(self.parameters.ctime_step_static))
		deltak_list = [deltak] * len(ctime_list)
		self.set_deltak(ctime_list, deltak_list)

		# Read LSA
		deltak_list = deltak_list #self.get_deltak()

		# SUbscribe to BBQ
		self.subscribe_to_acquisition_fields()
		# wait until the measurement
		self.madx_cycle_static_measurement(deltak)

		# Intensities
		# ignore for now

		# Reset strengths
		self.set_deltak(ctime_list, [0.0] * len(ctime_list))

		# Save model to compare
		betaymodel = self.model_betay()

		# Save measurement
		output_file = '%s_%s.json' % (self.parameters.quad_id_static, datetime.now().strftime("%Y.%m.%d-%H.%M.%S.%f"))

		metadata = {'file': output_file,
					'machine': self.parameters.machine_flag,
					'quad': self.parameters.quad_id_static,
					'mode': 'static'}
		measurement = {'ctime': self.measStamp,
					   'deltak': deltak_list,
					   'Qx_meas': self.estimatedTuneH,
					   'Qy_meas': self.estimatedTuneV}
		model = {'betaymodel': list(betaymodel)}
		data = {'metadata': metadata,
				'measurement': measurement,
				'model': model}

		if not os.path.exists('%soutputs/%s/static/raw_data/%s'%(self.output_path, self.parameters.machine_flag, datetime.now().strftime("%Y.%m.%d"))):
			os.makedirs('%soutputs/%s/static/raw_data/%s'%(self.output_path, self.parameters.machine_flag, datetime.now().strftime("%Y.%m.%d")))
		output_path = '%soutputs/%s/static/raw_data/%s/'%(self.output_path, self.parameters.machine_flag, datetime.now().strftime("%Y.%m.%d"))
		save_data_to_json(data, output_path, output_file)

		return data, (output_path+output_file)


	def model_betay(self):

		# Model values from MAD-X
		# to test; not the actual values
		ctimes_model = list(np.linspace(275, 280, 51))
		betay_model  = np.array([12.050353840657088, 12.037941401491796, 12.047257819417025, 12.078527857704728,
					   12.13204789623594, 12.208174010937945, 12.307111746425582, 12.428779958039376,
					   12.573583081140537, 12.746853966996907, 12.918554908365051, 13.088622261782575,
					   13.256668216835935, 13.422735366900392, 13.586441834344718, 13.747826245719521,
					   13.906514946524696, 14.062444801743485, 14.215547647095102, 14.365466338686431,
					   14.512231882557858, 14.65550078207094, 14.79530175998865, 14.931306935362915,
					   15.063460161759844, 15.191699553574374, 15.315723924398606, 15.43555746188265,
					   15.55091874791464, 15.66183039218615, 15.768032108311543, 15.869481902573177,
					   15.966130933821573, 16.057752797292526, 16.14436608606021, 16.225768211405104,
					   16.301975992811634, 16.37281146608278, 16.43824961382186, 16.498257529718508,
					   16.552705270094627, 16.60156618951496, 16.644785095293507, 16.630014942933247,
					   16.602939959298894, 16.566178307417985, 16.52206279380309, 16.472667728389695,
					   16.4197643017352, 16.364857784719582, 16.309152036566])

		mask = []
		for ctime in self.measStamp:
			if ctime in ctimes_model:
				mask.append(ctimes_model.index(ctime))

		return betay_model[mask]



	#------------------------------------------------------------------------------------------------------------------#
	# MAD-X specific functions
	# ------------------------------------------------------------------------------------------------------------------#

	def create_Madx_instance(self):
		from cpymad.madx import Madx
		mad = Madx()
		mad.options.echo = False
		mad.options.info = False
		mad.warn = False
		return mad		


	def madx_cycle_static_measurement(self, deltak):
		'''
		This function is not needed in the real_measurement.py
		'''
		
		mad = self.create_Madx_instance()
		self.machine.get_MADX_lattice(self.simulation_parameters, mad)

		ctime_arr= np.arange(self.parameters.ctime_min_static, self.parameters.ctime_max_static, float(self.parameters.ctime_step_static))
		
		Qy_meas    = []
		Qx_meas    = []
		betaymodel = []
		
		for ctime in ctime_arr:

			self.machine.set_model_strenghts(self.simulation_parameters, mad, ctime)

			# model
			twtable = self.machine.get_twiss_table(mad)
			indx_quad = [(i-1) for i,n in enumerate(twtable['name']) if '%s:1'%self.parameters.quad_id_static.lower() in n] 
			#l_quad = twtable['l'][indx_quad]
			betaymodel.append(get_meanbetay(self.parameters.quad_id_static.lower(), twtable))

			self.machine.set_deltak(mad, self.parameters.quad_id_static, deltak)
			Qx, Qy = self.get_tune_from_madx(mad)
			Qx_meas.append(Qx)
			Qy_meas.append(Qy)
			self.machine.set_deltak(mad, self.parameters.quad_id_static, 0)

		pyjapc.system.set("BBQDevice/Acquisition", {"estimatedTuneH": Qx_meas,
													"estimatedTuneV": Qy_meas,
													"measStamp": list(ctime_arr),
													"doneNbOfMeas": len(ctime_arr)})
		mad.input('exit;')


	def get_tune_from_madx(self, mad):
		twtable = self.machine.get_twiss_table(mad)
		Qx_meas = twtable.summary['q1']
		Qy_meas = twtable.summary['q2']  # +np.random.normal(scale=0.5e-3)
		return Qx_meas, Qy_meas


	def cycle_periodic_measurement(self):
		'''
		Input: 
		Function: tune measurements along one cycle for a periodic deltak variation
		Output: datafile of the measurement in the format: cpm_YYYMMDD-HHMMSS.csv
		'''

		mad = self.create_Madx_instance()
		self.machine.get_MADX_lattice(self.simulation_parameters, mad)
		
		ctime_arr      = np.arange(self.parameters.ctime_min_periodic, self.parameters.ctime_max_periodic, self.parameters.ctime_step_periodic)
		ctime_arr_norm = (ctime_arr-self.parameters.ctime_min_periodic)/(self.parameters.ctime_max_periodic-self.parameters.ctime_min_periodic)
		deltak_arr     = periodic_function_selector(self.parameters.periodic_modulation_function_flag, 
																  ctime_arr_norm,
																  self.parameters.periodic_cycle_delta_k, self.parameters.modulation_freq, self.parameters.deltak_offset)

		Qx_meas = []
		Qy_meas = []
		for deltak,i in zip(deltak_arr, range(len(deltak_arr))):

			#self.machine.set_model_strenghts(self.simulation_parameters, mad, ctime_arr[i])

			self.machine.set_deltak(mad, self.parameters.quad_id_periodic, deltak)
			Qx, Qy = self.get_tune(mad)
			Qx_meas.append(Qx)
			Qy_meas.append(Qy)
		self.machine.set_deltak(mad, self.parameters.quad_id_periodic, 0)

		output_file = '%s_%s.json'%(self.parameters.quad_id_static, datetime.now().strftime("%Y.%m.%d-%H.%M.%S.%f"))

		metadata    = {'file'     : output_file,
		  			   'machine'  : self.parameters.machine_flag,
		  			   'quad'     : self.parameters.quad_id_static,
		  			   'mode'     : 'static',
		  			   'dk'       : self.parameters.periodic_cycle_delta_k,
		  			   'dk_freq'  : self.parameters.modulation_freq,
		  			   'dk_offset': self.parameters.deltak_offset}
		measurement = {'ctime'    : list(ctime_arr),
				       'deltak'   : list(deltak_arr),
					   'Qx_meas'  : list(Qx_meas),
				       'Qy_meas'  : list(Qy_meas)}
		data        = {'metadata' : metadata, 'measurement': measurement}
		

		if not os.path.exists('%soutputs/%s/periodic/raw_data/%s'%(self.output_path, self.parameters.machine_flag, datetime.now().strftime("%Y.%m.%d"))): 
			os.makedirs('%soutputs/%s/periodic/raw_data/%s'%(self.output_path, self.parameters.machine_flag, datetime.now().strftime("%Y.%m.%d")))
		output_path = '%soutputs/%s/periodic/raw_data/%s/'%(self.output_path, self.parameters.machine_flag,datetime.now().strftime("%Y.%m.%d"))
		save_data_to_json(data,output_path,output_file)

		mad.input('exit;')

		return data, (output_path+output_file)