# App modules
from k_mod_app.analysis import Analysis
from k_mod_app.correction import Correction
from k_mod_app.machine_selection import machine_selection
from k_mod_app.about_the_application import about_the_application
from k_mod_app.analysis_tools import *

from k_mod_app.external.interactive_plots import DataSource
from k_mod_app.external.pg_time_axis import TimeAxisItem
from k_mod_app.external.worker import Worker

# from PyQt5 import uic
# with open('k_mod_app/windows/mainWindow1.py', 'w') as file:
# 	uic.compileUi("k_mod_app/windows/mainWindow1.ui", file)
from k_mod_app.windows.mainWindow1 import Ui_mainWindow1

# GUI libraries
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import qtawesome

from pyqtgraph import PlotWidget
import pyqtgraph as pg

# Other
from accwidgets import graph as accgraph
# from accwidgets import log_console
from accwidgets.log_console import LogConsoleModel, LogConsole, LogConsoleDock
from accwidgets.lsa_selector import LsaSelector, LsaSelectorModel, LsaSelectorAccelerator, AbstractLsaSelectorContext
import jpype
import logging


# Settings
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')
logging.basicConfig(level='DEBUG')
enable_lsa = False  # If false the LSA reading/writting will not be performed by the PSB Correction tab
show_lsa = True  # If false the LSA context selector will not be instantiated


class MainWindow(QtWidgets.QMainWindow):

	def __init__(self):

		super(MainWindow, self).__init__()

		# Select machine, initialize window and parameters, setup buttons
		self.version = '0.9.72' # how to access app's version?
		self.machine, self.ring, self.measurement_mode, self.output_path = machine_selection()
		self.ui = Ui_mainWindow1()
		self.ui.setupUi(self)
		self.parameters, self.simulation_parameters = self.machine.get_default_parameters()
		self.setup_buttons()
		self.update_parameters()
		# self.setWindowIcon(QIcon("/afs/cern.ch/user/t/tprebiba/Desktop/k-modulation-analysis-application-for-psb-and-ps/k_mod_app/windows/k-mod-app-logo.png")) # relative path

		# Add log_console
		self.logger = logging.getLogger('root')
		self.logger.setLevel(level='DEBUG')
		model = LogConsoleModel(loggers=[self.logger])#, level_changes_modify_loggers=True)
		dock = LogConsoleDock(console=LogConsole(model=model))
		dock.setFeatures(QDockWidget.NoDockWidgetFeatures)  # Disable floating and collapsing (dock-level)
		dock.console.expanded = False  # Make collapsed by default
		self.addDockWidget(Qt.BottomDockWidgetArea, dock)
		self.logger.info('K-modulation analysis application.')
		#print = self.logger.info

		# Structure output directories
		if not os.path.exists('%soutputs/%s' % (self.output_path, self.parameters.machine_flag)):
			os.makedirs('%soutputs/%s' % (self.output_path, self.parameters.machine_flag))
		for mode in ['static', 'periodic']:
			if not os.path.exists('%soutputs/%s/%s' % (self.output_path, self.parameters.machine_flag, mode)):
				os.makedirs('%soutputs/%s/%s' % (self.output_path, self.parameters.machine_flag, mode))
			if not os.path.exists('%soutputs/%s/%s/raw_data' % (self.output_path, self.parameters.machine_flag, mode)):
				os.makedirs('%soutputs/%s/%s/raw_data' % (self.output_path, self.parameters.machine_flag, mode))
			if not os.path.exists('%soutputs/%s/%s/beta_output' % (self.output_path, self.parameters.machine_flag, mode)):
				os.makedirs('%soutputs/%s/%s/beta_output' % (self.output_path, self.parameters.machine_flag, mode))
			if not os.path.exists('%soutputs/%s/%s/correction_output' % (self.output_path, self.parameters.machine_flag, mode)):
				os.makedirs('%soutputs/%s/%s/correction_output' % (self.output_path, self.parameters.machine_flag, mode))
		if not os.path.exists('%soutputs/%s/tune_evolution' % (self.output_path, self.parameters.machine_flag)):
			os.makedirs('%soutputs/%s/tune_evolution' % (self.output_path, self.parameters.machine_flag))

		# GUI layout modifications
		self.ui.actionExit_Ctrl_Q.triggered.connect(qApp.quit)
		self.ui.actionExit_Ctrl_Q.setShortcut('Ctrl+Q')
		self.ui.actionAbout_the_Application.triggered.connect(self.get_help)
		self.ui.actionAbout_the_Application.setShortcut('Ctrl+H')

		self.statusBar().showMessage('Ready.')
		self.ui.OutputDirectoryLabel.setText("Output directory: %s" % self.output_path)

		self.ui.OneCycleInterruptButton.setEnabled(False)
		self.ui.MultipleCycleInterruptButton.setEnabled(False)
		self.ui.PeriodicInterruptButton.setEnabled(False)

		self.ui.exModeComboBox.setCurrentIndex(0)
		self.show_hide_BBQ_ex_settings()
		self.ui.exModeComboBox.currentIndexChanged.connect(self.show_hide_BBQ_ex_settings)
		self.ui.exPatternComboBox.currentIndexChanged.connect(self.show_hide_BBQ_ex_settings)

		# Mode specific GUI layout modifications
		if str(self.measurement_mode).split('/')[-1][:-2] == 'measurement.py':
			self.measurement_mode_flag = 'expert'
			self.logger.info('%s%s is selected in %s mode.' % (self.parameters.machine_flag, self.ring, self.measurement_mode_flag))
			self.logger.info('Path to output: %s' % self.output_path)
			self.japc = None
			self.lsa = None
			self.lsa_context = None
			mode_title_flag = " (MAD-X simulation)"
			self.ui.BBQSettingsGroupBox.deleteLater()
			self.ui.TuneEvolutionTab.deleteLater()

		else:
			self.measurement_mode_flag = 'standard'
			self.logger.info('%s%s is selected in %s mode.' % (self.parameters.machine_flag, self.ring, self.measurement_mode_flag))
			self.logger.info('Path to output: %s' % self.output_path)
			import pjlsa
			import pyjapc
			#from pyjapcscout import PyJapcScout
			# server = 'next'
			server = self.parameters.machine_flag.lower()
			self.lsa = pjlsa.LSAClient(server=server)
			print('LSA instantiated on "%s" server.' % server)
			self.logger.info('LSA instantiated on "%s" server.' % server)
			self.japc = pyjapc.PyJapc(noSet=False)
			#self.japc = PyJapcScout(incaAcceleratorName=server.upper(), noSet=False)
			self.japc.rbacLogin() # Get an RBAC token (required for the OASIS data)
			mode_title_flag = ""
			# user = "PSB.USER.MD1" # old
			# self.japc.setSelector(user)
			# self.japc.rbacLogin(username="tprebiba")
			# print('JAPC initialized on %s'%user)
			self.lsa_context = None
			# print('LSA context (hardcoded): %s'%self.lsa_context)

			# Set LSA selector
			if show_lsa:
				container = QWidget(self.ui.CycleSelectorGroupBox)
				lsaSelectorModel = None
				if self.parameters.machine_flag.lower() == 'psb':
					lsaSelectorModel = LsaSelectorModel(accelerator=LsaSelectorAccelerator.PSB, lsa=self.lsa, categories={AbstractLsaSelectorContext.Category.OPERATIONAL,
					                                                                                                      AbstractLsaSelectorContext.Category.MD})
				elif self.parameters.machine_flag.lower() == 'ps':
					lsaSelectorModel = LsaSelectorModel(accelerator=LsaSelectorAccelerator.PS, lsa=self.lsa, categories={AbstractLsaSelectorContext.Category.OPERATIONAL,
					                                                                                                      AbstractLsaSelectorContext.Category.MD})
				lsaSelector = LsaSelector(parent=container, model=lsaSelectorModel)
				layout = QVBoxLayout()
				layout.addWidget(lsaSelector)
				layout.setContentsMargins(0, 0, 0, 0)
				lsaSelector.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
				container.setLayout(layout)

				lsaSelector.contextSelectionChanged.connect(lambda ctx: self.update_context_user(ctx))

		# Machine specific GUI layout modifications
		if self.parameters.machine_flag == 'PSB':
			self.setWindowTitle("K-Modulation Application for the CERN %s Ring %s%s (v.%s)" % (self.parameters.machine_flag, self.ring, mode_title_flag, self.version))
			self.ui.MachineLabel.setText('%s Ring %s' % (self.parameters.machine_flag, self.ring))
			self.ui.TwoModesTab.setCurrentIndex(1)
			self.ui.BBQDeviceComboBox.deleteLater()
			self.bbq_device_number = 0
		elif self.parameters.machine_flag == 'PS':
			self.setWindowTitle("K-Modulation Application for the CERN %s%s (v.%s)" % (self.parameters.machine_flag, mode_title_flag, self.version))
			self.ui.MachineLabel.setText(self.parameters.machine_flag)
			self.ui.TwoModesTab.setCurrentIndex(0)
			self.bbq_device_number = 0
			self.ui.CorrectionTab.deleteLater()
			self.ui.TuneEvolutionTab.deleteLater()

			self.ui.PeriodicHorizontalTuneValue.setValue(6.210)
			self.ui.PeriodicVerticalTuneValue.setValue(6.247)
			self.ui.HorizontalTuneValue.setValue(6.210)
			self.ui.VerticalTuneValue.setValue(6.247)
			for i, item in enumerate(['none', 'all']):
				self.ui.StaticQuadrupoleQListWidget.addItem(item)
				self.ui.StaticQuadrupoleQListWidget.item(i).setCheckState(QtCore.Qt.Unchecked)
				self.ui.PeriodicQuadrupoleQListWidget.addItem(item)
				self.ui.PeriodicQuadrupoleQListWidget.item(i).setCheckState(QtCore.Qt.Unchecked)

		self.beta_tabs = {}  # tab for each quad
		self.beta_tabs_periodic = {}
		self.beta_graphs = {}  # deltak-Q plot for each quad
		self.beta_graphs_periodic = {}
		self.model_meas_beta_graphs = {}  # model-measured beta function for each quad
		self.betas_from_fit_lists = {}  # betas from fit list for each quad
		self.q_modulation_periodic = {}  # Q-ctime plot
		self.dk_modulation_periodic = {}  # dk-ctime plot
		self.first_quad_indx = {'PSB': 0, 'PS': 2}
		for quad_indx, quad in enumerate(self.parameters.quad_list):
			self.ui.StaticQuadrupoleQListWidget.addItem(quad)
			self.ui.StaticQuadrupoleQListWidget.item(quad_indx + self.first_quad_indx[self.parameters.machine_flag]).setCheckState(QtCore.Qt.Unchecked)
			self.ui.PeriodicQuadrupoleQListWidget.addItem(quad)
			self.ui.PeriodicQuadrupoleQListWidget.item(quad_indx + self.first_quad_indx[self.parameters.machine_flag]).setCheckState(QtCore.Qt.Unchecked)

			self.beta_tabs[quad] = QtWidgets.QWidget()
			self.beta_tabs[quad].setObjectName(quad)
			self.beta_tabs_periodic[quad] = QtWidgets.QWidget()
			self.beta_tabs_periodic[quad].setObjectName(quad + "_periodic")

			self.beta_graphs[quad] = self.custom_plot_editor(self.beta_tabs[quad], None, QtCore.QRect(10, 10, 581, 311), 'Q',r'&delta;k (10<sup>-2</sup> m<sup>-2</sup>)', 'black', True, True)
			self.beta_graphs_periodic[quad] = self.custom_plot_editor(self.beta_tabs_periodic[quad], None, QtCore.QRect(10, 290, 771, 351), '&beta; (m)', '', 'black', True, True)
			self.beta_graphs_periodic[quad].getPlotItem().hideAxis('bottom')
			self.model_meas_beta_graphs[quad] = self.custom_plot_editor(self.beta_tabs[quad], None, QtCore.QRect(10, 330, 581, 311), '&beta; (m)', 'CTime (ms)', 'black', True,True)

			self.betas_from_fit_lists[quad] = QtWidgets.QListView(self.beta_tabs[quad])
			self.betas_from_fit_lists[quad].setGeometry(QtCore.QRect(600, 10, 181, 631))
			self.betas_from_fit_lists[quad].setModelColumn(0)
			self.betas_from_fit_lists[quad].setWordWrap(True)

			self.q_modulation_periodic[quad] = self.custom_plot_editor(self.beta_tabs_periodic[quad], None, QtCore.QRect(400, 10, 381, 271), 'Q', 'CTime (ms)', 'black', True, True)
			self.dk_modulation_periodic[quad] = self.custom_plot_editor(self.beta_tabs_periodic[quad], None, QtCore.QRect(10, 10, 381, 271),
																		r'&delta;k (10<sup>-2</sup> m<sup>-2</sup>)',
																		'CTime (ms)', 'black', True, True)

			self.ui.BetaFitTabs.addTab(self.beta_tabs[quad], quad)
			self.ui.PeriodicQuadTabs.addTab(self.beta_tabs_periodic[quad], quad)

		# Set first quad checked
		self.ui.StaticQuadrupoleQListWidget.item(self.first_quad_indx[self.parameters.machine_flag]).setCheckState(QtCore.Qt.Checked)
		self.ui.PeriodicQuadrupoleQListWidget.item(self.first_quad_indx[self.parameters.machine_flag]).setCheckState(QtCore.Qt.Checked)

		for i in range(2):
			self.ui.BetaFitTabs.removeTab(0)
			self.ui.PeriodicQuadTabs.removeTab(0)
		self.ui.BetaFitTabs.setCurrentIndex(3)
		self.ui.PeriodicQuadTabs.setCurrentIndex(2)

		self.ui.CorrectionQDE3CheckBox.stateChanged.connect(self.plot_correction)
		self.ui.CorrectionQDE14CheckBox.stateChanged.connect(self.plot_correction)
		self.ui.CorrectionAverageCheckBox.stateChanged.connect(self.plot_correction)
		self.CorrectionCheckBox = {self.parameters.quad_list[0]: self.ui.CorrectionQDE3CheckBox,
								   self.parameters.quad_list[1]: self.ui.CorrectionQDE14CheckBox}

		# Various additional fields
		self.static_measurement_file_list = []
		self.static_measurement_model_list = QStandardItemModel(self.ui.LoadedSingleCycleDataList)
		self.periodic_measurement_file_list = []
		self.periodic_measurement_model_list = QStandardItemModel(self.ui.LoadedPeriodicCycleDataList)
		self.QDE3_beta_fitted_file_list = []
		self.QDE3_beta_fitted_model_list = QStandardItemModel(self.ui.QDE3LoadedCorrectionDataList)
		self.QDE14_beta_fitted_file_list = []
		self.QDE14_beta_fitted_model_list = QStandardItemModel(self.ui.QDE14LoadedCorrectionDataList)
		self.current_session_ctimes_static = set([])
		self.master_table = pd.DataFrame(columns=[])
		self.master_table_periodic = pd.DataFrame(columns=[])
		self.executor = QThreadPool()  # for multithreading
		self.executor.setMaxThreadCount(1)  # !!! important !!!
		# self.correction_matrix_file = '/afs/cern.ch/eng/acc-models/psb/2021/operation/k_modulation/error_response_matrix/correction_matrix_reference_new_lattice.tfs'
		self.correction_matrix_file = '/afs/cern.ch/user/t/tprebiba/public/KModAppPSB_correction_matrices/correction_matrix_extended.tfs'
		model = QStandardItemModel(self.ui.CorrectionMatrixFileList)
		model.appendRow(QStandardItem(self.correction_matrix_file.split("/")[-1]))
		self.ui.CorrectionMatrixFileList.setModel(model)
		self.tune_evolution_file_list = []
		self.tune_evolution_model_list = QStandardItemModel(self.ui.LoadedTuneEvolutionFileList)

		# Set static plots
		self.custom_plot_editor(None, self.ui.BetaSummaryTwissGraph, None, r'&beta; (m)', 's (m)', 'black', True, True)
		self.custom_plot_editor(None, self.ui.PeriodicBetaSummaryTwissGraph, None, r'&beta; (m)', 's (m)', 'black', True, True)
		self.custom_plot_editor(None, self.ui.BRBCTIntensityGraph, None, 'Intensity at last tune acquisition (10<sup>10</sup> ppb)', "", 'black', True, True)
		self.ui.BRBCTIntensityGraph.getPlotItem().setAxisItems({'bottom': TimeAxisItem(orientation='bottom')})
		self.custom_plot_editor(None, self.ui.QReconstructedGraph, None, 'Tune (reconstructed)', "CTime (ms)", 'black', True, True)
		self.custom_plot_editor(None, self.ui.PeriodicBRBCTIntensityGraph, None, 'Intensity (10<sup>10</sup> ppb)', "", 'black', True, True)
		self.ui.PeriodicBRBCTIntensityGraph.getPlotItem().setAxisItems({'bottom': TimeAxisItem(orientation='bottom')})
		self.custom_plot_editor(None, self.ui.QDE314TrimCorrectionGraph, None, r'&delta;k<sub>QD314CORR</sub> (10<sup>-2</sup> m<sup>-2</sup>)', "CTime (ms)", 'black', True, True)
		self.custom_plot_editor(None, self.ui.QDE314KDDeltaCorrectionGraph, None, r'&delta;k<sub>QDCORR</sub>', 'CTime (ms)', 'black', True, True)
		self.custom_plot_editor(None, self.ui.QDE314KFDeltaCorrectionGraph, None, r'&delta;k<sub>QFCORR</sub>', 'CTime (ms)', 'black', True, True)

		# Set interactive plots
		self.QDE314TrimCorrectionFinalGraph = accgraph.EditablePlotWidget()
		self.custom_plot_editor(None, self.QDE314TrimCorrectionFinalGraph, None, 'k<sub>QD314CORR</sub> (10<sup>-2</sup> m<sup>-2</sup>)', 'CTime (ms)', 'black',
		                        False, False) # showGrid offsets the cursor point; to be understood; switched to False for all interactive plots (for now...)
		container = QWidget(self.ui.FinalCorrectionGroupBox)
		layout = QVBoxLayout()
		layout.addWidget(self.QDE314TrimCorrectionFinalGraph)
		container.setLayout(layout)
		container.setGeometry(QtCore.QRect(10, 30, 371, 281))

		self.QDE314KDDeltaCorrectionFinalGraph = accgraph.EditablePlotWidget()
		self.custom_plot_editor(None, self.QDE314KDDeltaCorrectionFinalGraph, None, 'k<sub>QDCORR</sub> (10<sup>-2</sup> m<sup>-2</sup>)', 'CTime (ms)', 'black',
		                        False, False) # showGrid offsets the cursor point; to be understood; switched to False for all interactive plots (for now...)
		container = QWidget(self.ui.FinalCorrectionGroupBox)
		layout = QVBoxLayout()
		layout.addWidget(self.QDE314KDDeltaCorrectionFinalGraph)
		container.setLayout(layout)
		container.setGeometry(QtCore.QRect(400, 30, 371, 281))

		self.QDE314KFDeltaCorrectionFinalGraph = accgraph.EditablePlotWidget()
		self.custom_plot_editor(None, self.QDE314KFDeltaCorrectionFinalGraph, None, 'k<sub>QFCORR</sub> (10<sup>-2</sup> m<sup>-2</sup>)', 'CTime (ms)', 'black',
		                        False, False) # showGrid offsets the cursor point; to be understood; switched to False for all interactive plots (for now...)
		container = QWidget(self.ui.FinalCorrectionGroupBox)
		layout = QVBoxLayout()
		layout.addWidget(self.QDE314KFDeltaCorrectionFinalGraph)
		container.setLayout(layout)
		container.setGeometry(QtCore.QRect(790, 30, 371, 281))

		self.TuneCorrectionDeltaKFPlot = accgraph.EditablePlotWidget()
		self.custom_plot_editor(None, self.TuneCorrectionDeltaKFPlot, None, r'&delta;k<sub>QFCORR</sub> (m<sup>-2</sup>)', 'CTime (ms)', 'black',
		                        False, False) # showGrid offsets the cursor point; to be understood; switched to False for all interactive plots (for now...)
		container = QWidget(self.ui.TuneEvolutionTab)
		layout = QVBoxLayout()
		layout.addWidget(self.TuneCorrectionDeltaKFPlot)
		container.setLayout(layout)
		container.setGeometry(QtCore.QRect(230, 370, 471, 281))

		self.TuneCorrectionDeltaKDPlot = accgraph.EditablePlotWidget()
		self.custom_plot_editor(None, self.TuneCorrectionDeltaKDPlot, None, r'&delta;k<sub>QDCORR</sub> (m<sup>-2</sup>)', 'CTime (ms)', 'black',
		                        False, False) # showGrid offsets the cursor point; to be understood; switched to False for all interactive plots (for now...)
		container = QWidget(self.ui.TuneEvolutionTab)
		layout = QVBoxLayout()
		layout.addWidget(self.TuneCorrectionDeltaKDPlot)
		container.setLayout(layout)
		container.setGeometry(QtCore.QRect(720, 370, 471, 281))

		# Set function editor for PSB beta-beating correction tab
		insert_points_action = QtWidgets.QAction(qtawesome.icon('mdi.plus'), 'Add points', self)
		self.bar = accgraph.EditingToolBar()
		self.bar.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
		self.bar.add_transformation(insert_points_action, self.insert_points_transformation, min_points_needed=2)
		container = QWidget(self.ui.EditorGroupBox)
		layout = QVBoxLayout()
		layout.addWidget(self.bar)
		container.setLayout(layout)
		#container.setGeometry(QtCore.QRect(QtCore.QRect(0, 20, 811, 71)))
		container.setGeometry(QtCore.QRect(0, 20, 891, 71))
		self.bar.connect(self.QDE314TrimCorrectionFinalGraph)
		self.bar.connect(self.QDE314KDDeltaCorrectionFinalGraph)
		self.bar.connect(self.QDE314KFDeltaCorrectionFinalGraph)
		# Set function editor for PSB tune correction tab
		self.bar2 = accgraph.EditingToolBar()
		self.bar2.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
		self.bar2.add_transformation(insert_points_action, self.insert_points_transformation, min_points_needed=2)
		container = QWidget(self.ui.EditorGroupBox_2)
		layout = QVBoxLayout()
		layout.addWidget(self.bar2)
		container.setLayout(layout)
		container.setGeometry(QtCore.QRect(QtCore.QRect(0, 20, 901, 71)))
		self.bar2.connect(self.TuneCorrectionDeltaKFPlot)
		self.bar2.connect(self.TuneCorrectionDeltaKDPlot)

		# Set tune evolution plots
		self.custom_plot_editor(None, self.ui.HorizontalTuneEvolutionPlot, None, r'Q<sub>H</sub>', 'CTime (ms)', 'black', True, True)
		self.custom_plot_editor(None, self.ui.VerticalTuneEvolutionPlot, None, r'Q<sub>V</sub>', 'CTime (ms)', 'black', True, True)

		# Connect signals
		self.ui.LoadedSingleCycleDataList.setModel(self.static_measurement_model_list)
		self.ui.LoadedSingleCycleDataList.selectionModel().selectionChanged.connect(self.highlight_plot_points)
		self.ui.LoadedPeriodicCycleDataList.setModel(self.periodic_measurement_model_list)
		self.ui.LoadedPeriodicCycleDataList.selectionModel().selectionChanged.connect(self.highlight_plot_points_periodic)
		self.ui.LoadedTuneEvolutionFileList.setModel(self.tune_evolution_model_list)
		self.ui.LoadedTuneEvolutionFileList.selectionModel().selectionChanged.connect(self.highlight_tune_evolution_points)
		self.ui.StaticCtimesToPlotCheckableList.itemClicked.connect(self.plot_raw_data_static)
		self.ui.PlaneComboBox.currentIndexChanged.connect(self.plot_raw_data_static)
		self.ui.PeriodicPlaneComboBox.currentIndexChanged.connect(lambda x: [self.plot_raw_data_periodic(), self.clear_beta_graphs_periodic()])
		#self.ui.BetaSummaryCtimeComboBox.currentIndexChanged.connect(self.plot_beta_twiss_summary_static) # automatically updates every-time update_static_gui_fields is called, to be fixed

		# For "none" and "all" combobox states
		self.ui.StaticCtimesToPlotCheckableList.itemClicked.connect(lambda item: self.combobox_select_all_none(item, self.ui.StaticCtimesToPlotCheckableList,
																											   len(self.current_session_ctimes_static), self.plot_raw_data_static))
		self.ui.StaticQuadrupoleQListWidget.itemClicked.connect(lambda item: self.combobox_select_all_none(item, self.ui.StaticQuadrupoleQListWidget,
																										   len(self.parameters.quad_list), self.update_parameters))
		self.ui.PeriodicQuadrupoleQListWidget.itemClicked.connect(lambda item: self.combobox_select_all_none(item, self.ui.PeriodicQuadrupoleQListWidget,
																											 len(self.parameters.quad_list), self.update_parameters))


	def update_context_user(self, ctx):
		self.lsa_context = str(ctx.name)
		print('LSA context set to %s' % self.lsa_context)
		self.logger.info('LSA context set to %s' % self.lsa_context)

		self.japc.setSelector(ctx.user)
		print('PyJAPC selector set to %s' % self.japc.getSelector())
		self.logger.info('PyJAPC selector set to %s' % self.japc.getSelector())
		#self.japc.setDefaultSelector(ctx.user)
		#print('PyJAPC selector set to %s' % ctx.user) # pyjapcscout does not have method getSelector or similar
		#self.logger.info('PyJAPC selector set to %s' % ctx.user)


	def custom_plot_editor(self, parent, plot, geometry, leftlabel, bottomlabel, color, showgridx, showgridy):
		if parent is not None:
			plot_widget = PlotWidget(parent)
		else:
			plot_widget = plot
		if geometry is not None:
			plot_widget.setGeometry(geometry)
		# plot_widget.setObjectName(name)
		plot_widget.setLabel('left', leftlabel, color=color)  # , size=size)
		plot_widget.setLabel('bottom', bottomlabel, color=color)  # , size=size)
		plot_widget.showGrid(x=showgridx, y=showgridy)
		return plot_widget


	def insert_points_transformation(self, curve, value=None):
		# Additional transformation for the function editor bars
		# curve represents the selected points only!
		if value is None:
			spin_box = pg.SpinBox()
			dialog = QDialog()
			dialog.setWindowTitle("Insert points")
			dialog.setLayout(QGridLayout())
			button_bar = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
			explanation = "Insert additional points between selected data:"
			dialog.layout().addWidget(QLabel(explanation), 1, 1, 1, 2)
			dialog.layout().addWidget(QLabel("Number of total points: "), 2, 1)
			dialog.layout().addWidget(spin_box, 2, 2)
			dialog.layout().addWidget(button_bar, 3, 1, 1, 2)
			def accept():
				nonlocal value
				value = spin_box.value()
				dialog.close()
			button_bar.accepted.connect(accept)
			button_bar.rejected.connect(dialog.close)
			dialog.exec_()
		if value is not None:
			curve.x, curve.y = insert_additional_points_using_interp1d(curve.x[:], curve.y[:], value)
		return curve


	def update_parameters(self):
		'''
		updates the parameter list of the Parameters instance from the interface
		'''

		self.parameters = self.parameters._replace(ctime_min_static=self.ui.StaticCTimeMinValue.value())
		self.parameters = self.parameters._replace(ctime_max_static=self.ui.StaticCTimeMaxValue.value())
		self.parameters = self.parameters._replace(ctime_step_static=self.ui.StaticStepMsValue.value())

		self.parameters = self.parameters._replace(ctime_min_periodic=self.ui.PeriodicCTimeMinValue.value())
		self.parameters = self.parameters._replace(ctime_max_periodic=self.ui.PeriodicCTimeMaxValue.value())
		self.parameters = self.parameters._replace(ctime_step_periodic=self.ui.PeriodicStepMsValue.value())

		self.parameters = self.parameters._replace(deltak_min=self.ui.DeltaKMinValue.value() * 1e-2)
		self.parameters = self.parameters._replace(deltak_max=self.ui.DeltaKMaxValue.value() * 1e-2)
		self.parameters = self.parameters._replace(single_cycle_delta_k=self.ui.SingleCycleDeltaKValue.value() * 1e-2)
		self.parameters = self.parameters._replace(periodic_cycle_delta_k=self.ui.PeriodicCycleDeltaKValue.value() * 1e-2)
		self.parameters = self.parameters._replace(modulation_freq=self.ui.ModulationFrequencyValue.value())
		self.parameters = self.parameters._replace(deltak_offset=self.ui.DeltaKOffsetValue.value() * 1e-2)

		self.parameters = self.parameters._replace(nr_of_cycles_static=self.ui.NrOfCyclesValue.value())

		self.simulation_parameters = self.simulation_parameters._replace(QH_target=self.ui.HorizontalTuneValue.value())
		self.simulation_parameters = self.simulation_parameters._replace(QV_target=self.ui.VerticalTuneValue.value())

		if self.ui.SinusoidalRadioButton.isChecked():
			self.parameters = self.parameters._replace(periodic_modulation_function_flag='sine')
		elif self.ui.SawtoothRadioButton.isChecked():
			self.parameters = self.parameters._replace(periodic_modulation_function_flag='sawtooth')

		if self.ui.QSinusoidalRadioButton.isChecked():
			self.parameters = self.parameters._replace(Q_modulation_fitting_function_flag='simple_sinusoidal')
		elif self.ui.QDoubleSinusoidalRadioButton.isChecked():
			self.parameters = self.parameters._replace(Q_modulation_fitting_function_flag='double_sinusoidal')
		elif self.ui.QExactNonLinearRadioButton.isChecked():
			self.parameters = self.parameters._replace(Q_modulation_fitting_function_flag='complete_formula')

		if self.parameters.machine_flag.lower() == 'psb':
			self.bbq_device_number = 0
		elif self.parameters.machine_flag.lower() == 'ps':
			try:
				if self.ui.BBQDeviceComboBox.currentText() == 'PR.BQS69':
					self.bbq_device_number = 0
				elif self.ui.BBQDeviceComboBox.currentText() == 'PR.BQL72':
					self.bbq_device_number = 1
			except:
				pass

	def setup_buttons(self):
		'''
		Connects the window buttons to the corresponding functions
		'''

		# Static
		self.ui.OneCycleMeasurementButton.clicked.connect(self.single_cycle_measurement)
		self.ui.MultipleCycleMeasurementButton.clicked.connect(self.multi_cycle_measurement)
		self.ui.GetBetaFromFitButton.clicked.connect(self.get_beta_from_fit)
		self.ui.OneCycleInterruptButton.clicked.connect(self.interrupt_all_processes)
		self.ui.MultipleCycleInterruptButton.clicked.connect(self.interrupt_all_processes)

		# Periodic
		self.ui.PeriodicMeasurementButton.clicked.connect(self.periodic_measurement)
		self.ui.PeriodicTuneFitButton.clicked.connect(self.periodic_tune_fit)
		self.ui.PeriodicInterruptButton.clicked.connect(self.interrupt_all_processes)
		self.ui.GetModelPeriodicButton.clicked.connect(self.plot_beta_twiss_summary_periodic)

		# Load and delete data buttons
		self.ui.LoadDataForAnalysisButton.clicked.connect(self.load_raw_data)
		self.ui.LoadDataForCorrectionButton.clicked.connect(self.load_beta_fitted_data)
		self.ui.LoadDataForPeriodicButton.clicked.connect(self.load_periodic_data)
		self.ui.DeleteRawDataButton.clicked.connect(self.delete_raw_data)
		self.ui.DeleteBetaFittedDataButton.clicked.connect(self.delete_beta_fitted_data)
		self.ui.DeletePeriodicDataButton.clicked.connect(self.delete_periodic_data)
		self.ui.LoadNewCorrectionMatrixButton.clicked.connect(self.load_correction_matrix_file)
		self.ui.DeleteAllStaticDataButton.clicked.connect(self.delete_all_static_data)
		self.ui.DeleteAllPeriodicDataButton.clicked.connect(self.delete_all_periodic_data)

		# Other
		self.ui.SendToLSAButton.clicked.connect(self.send_to_lsa)
		self.ui.SetDefaultsButton.clicked.connect(self.set_default_correction)
		self.ui.BBQSettingsSendToFESAButton.clicked.connect(self.update_bbq_settings)
		self.ui.CalculateCorrectionButton.clicked.connect(self.interpolate_betas)
		self.ui.DropIntensityPointsButton.clicked.connect(self.drop_intensity_points)

		# Tune evolution
		self.ui.RecordTuneButton.clicked.connect(self.record_tune)
		self.ui.StopTuneRecordButton.clicked.connect(self.stop_tune_record)
		self.ui.LoadTuneEvolutionDataButton.clicked.connect(self.load_tune_evolution_data)
		self.ui.DeleteTuneEvolutionDataButton.clicked.connect(self.delete_tune_evolution_data)
		self.ui.FindMeanButton.clicked.connect(self.calculate_plot_mean_tune)
		self.ui.CalculateTuneCorrectionStrengthsButton.clicked.connect(self.calculate_tune_correction_deltas)
		self.ui.SendTuneCorrectionToLSAButton.clicked.connect(self.send_tune_corrections_to_lsa)

	# ---------------------------------------------------------------------------------------------
	# Signals
	# ---------------------------------------------------------------------------------------------

	# -----------------------------
	# Measurement

	def single_cycle_measurement(self):

		if ((self.measurement_mode_flag != 'expert') and (self.lsa_context == None)):
			self.custom_popup_window('Warning', 'Please select a cycle.', QMessageBox.Warning, QMessageBox.Ok)
		else:

			self.update_parameters()
			if self.measurement_mode_flag != 'expert':
				self.update_bbq_settings('static')
			self.lock_measurement_buttons(False)

			# Get quads to modulate:
			quads_to_modulate = []
			for quad_list_indx in range(self.first_quad_indx[self.parameters.machine_flag], self.ui.StaticQuadrupoleQListWidget.count()):
				if self.ui.StaticQuadrupoleQListWidget.item(quad_list_indx).checkState() == Qt.Checked:
					quads_to_modulate.append(self.ui.StaticQuadrupoleQListWidget.item(quad_list_indx).text())
			self.statusBar().showMessage('Static k-setting for %s...' %quads_to_modulate[0])

			# Instansiate Measurement class
			# (is an instance field: is used from the interrupt button)
			self.meas = self.measurement_mode.Measurement(self.parameters, self.simulation_parameters, self.machine,
														  self.output_path, self.ring, self.japc, self.lsa, self.lsa_context,
														  self.bbq_device_number, self.logger)

			# Setup measurement parameters (counters, read LSA, etc)
			ctimes = list(np.arange(self.parameters.ctime_min_static, self.parameters.ctime_max_static, float(self.parameters.ctime_step_static)))
			if (ctimes[-1] + float(self.parameters.ctime_step_static)) <= self.parameters.ctime_max_static:
				ctimes.append(ctimes[-1] + float(self.parameters.ctime_step_static))
			self.meas.setup_measurement('static', ctimes, [self.parameters.single_cycle_delta_k], self.ui.SkipCycleValue.value(), quads_to_modulate)

			# Subscribe to BCT and BBQ
			self.meas.start_subscriptions()

			# Wait until the measurement is done and stop subscription
			for i in range(len(quads_to_modulate)):
				worker = Worker(lambda: self.meas.get_measurement_when_done())
				worker.signals.result.connect(self.update_static_gui_fields)
				if i < (len(quads_to_modulate)-1):
					worker.signals.finished.connect(lambda: [self.lock_measurement_buttons(True),
					                                         self.statusBar().showMessage('Static k-setting for %s'%(quads_to_modulate[self.quad_id_counter]))])
				else: # to handle the status bar in the last quadrupole (self.quad_id_counter > len(quads_to_modulate))
					worker.signals.finished.connect(lambda: [self.lock_measurement_buttons(True),
					                                         self.statusBar().showMessage('Static k-setting for %s' % (quads_to_modulate[-1]))])
				self.executor.start(worker)

			# Subscription is finished after last job is finished
			worker = Worker(lambda: self.meas.stop_subscriptions())
			worker.signals.finished.connect(lambda: [self.statusBar().showMessage('Ready.'), self.lock_measurement_buttons(True)])
			self.executor.start(worker)

	def multi_cycle_measurement(self):

		if ((self.measurement_mode_flag != 'expert') and (self.lsa_context == None)):
			self.custom_popup_window('Warning', 'Please select a cycle.', QMessageBox.Warning, QMessageBox.Ok)
		else:
			# automatically apply the beta fit and check if the fitted beta error is below the desired threshold
			i_to_apply_fit = []
			if ((self.ui.StaticFittingOptionsGroupBox.isChecked()) and (self.ui.NrOfCyclesValue.value() > self.ui.FitNrStartValue.value())):
				i_to_apply_fit = list(np.arange(self.ui.FitNrStartValue.value(), self.ui.NrOfCyclesValue.value(), self.ui.FitNrStepValue.value()))

			self.update_parameters()

			# Get quads to modulate:
			quads_to_modulate = []
			for quad_list_indx in range(self.first_quad_indx[self.parameters.machine_flag], self.ui.StaticQuadrupoleQListWidget.count()):
				if self.ui.StaticQuadrupoleQListWidget.item(quad_list_indx).checkState() == Qt.Checked:
					quads_to_modulate.append(self.ui.StaticQuadrupoleQListWidget.item(quad_list_indx).text())

			if self.measurement_mode_flag != 'expert':
				self.update_bbq_settings('static')
			self.lock_measurement_buttons(False)
			self.statusBar().showMessage('Static k-setting for %s (#%s/%s)...' % (quads_to_modulate[0],1,str(self.parameters.nr_of_cycles_static)))

			# Instantiate Measurement class
			self.meas = self.measurement_mode.Measurement(self.parameters, self.simulation_parameters, self.machine,
														  self.output_path, self.ring, self.japc, self.lsa, self.lsa_context,
														  self.bbq_device_number, self.logger)

			# Setup measurement parameters (counters, read LSA, etc)
			deltak_values_list = []
			for i in range(self.parameters.nr_of_cycles_static):
				deltak_values_list.append(np.random.uniform(self.parameters.deltak_min, self.parameters.deltak_max))
			ctimes = list(np.arange(self.parameters.ctime_min_static, self.parameters.ctime_max_static, float(self.parameters.ctime_step_static)))
			if (ctimes[-1] + float(self.parameters.ctime_step_static)) <= self.parameters.ctime_max_static:
				ctimes.append(ctimes[-1] + float(self.parameters.ctime_step_static))
			self.meas.setup_measurement('static', ctimes, deltak_values_list, self.ui.SkipCycleValue.value(), quads_to_modulate)

			# Subscribe to BCT and BBQ
			self.meas.start_subscriptions()

			# Wait until the measurement is done and stop subscription
			# total number of measurements: nr_of_cycles * nr_of_quads
			for i in range(len(quads_to_modulate) * self.parameters.nr_of_cycles_static):

				# for automatic measurement control from beta fit
				if (i + 1) in i_to_apply_fit:
					if self.ui.ZScoreGroupBox.isChecked():
						z_score_threshold = self.ui.ZScoreValue.value()
					else:
						z_score_threshold = 0
					worker = Worker(lambda: Analysis().apply_beta_fit(self.master_table,
																	  self.parameters, self.simulation_parameters,
																	  self.output_path, 'y', z_score_threshold,
																	  self.ui.MinYValueValue.value(), self.ui.MaxYValueValue.value(),
																	  self.ui.MinXValueValue.value() * 1e-2, self.ui.MaxXValueValue.value() * 1e-2), self.logger)
					worker.signals.result.connect(self.check_beta_from_automatic_fit)
					self.executor.start(worker)

				worker = Worker(lambda: self.meas.get_measurement_when_done())
				worker.signals.result.connect(self.update_static_gui_fields)
				if i<(len(quads_to_modulate)*self.parameters.nr_of_cycles_static-1):
					worker.signals.finished.connect(lambda: [self.statusBar().showMessage('Static k-setting for %s (#%s/%s)...' % (quads_to_modulate[self.quad_id_counter],
					                                                                                                               str(self.measurement_counter+1),
																														           str(self.parameters.nr_of_cycles_static))),
														    self.lock_measurement_buttons(False)])
				else: # to handle when self.quad_id_counter >= len(quads_to_modulate)
					worker.signals.finished.connect(lambda: [self.statusBar().showMessage('Static k-setting for %s (#%s/%s)...' % (quads_to_modulate[-1],
					                                                                                                               str(self.measurement_counter+1),
					                                                                                                               str(self.parameters.nr_of_cycles_static))),
					                                         self.lock_measurement_buttons(False)])
				self.executor.start(worker)
			# Subscription is finished after last job is finished
			worker = Worker(lambda: self.meas.stop_subscriptions())
			worker.signals.finished.connect(lambda: [self.statusBar().showMessage('Ready.'), self.lock_measurement_buttons(True)])
			self.executor.start(worker)

	def periodic_measurement(self):

		if ((self.measurement_mode_flag != 'expert') and (self.lsa_context == None)):
			self.custom_popup_window('Warning', 'Please select a cycle.', QMessageBox.Warning, QMessageBox.Ok)
		else:

			self.update_parameters()
			if self.measurement_mode_flag != 'expert':
				self.update_bbq_settings('periodic')
			self.lock_measurement_buttons(False)

			# Get quads to modulate:
			quads_to_modulate = []
			for quad_list_indx in range(self.first_quad_indx[self.parameters.machine_flag], self.ui.PeriodicQuadrupoleQListWidget.count()):
				if self.ui.PeriodicQuadrupoleQListWidget.item(quad_list_indx).checkState() == Qt.Checked:
					quads_to_modulate.append(self.ui.PeriodicQuadrupoleQListWidget.item(quad_list_indx).text())
			self.statusBar().showMessage('Periodic modulation for %s...' % quads_to_modulate[0])

			# Prepare strength variation
			ctime_arr = list(np.arange(self.parameters.ctime_min_periodic, self.parameters.ctime_max_periodic, self.parameters.ctime_step_periodic))
			if (ctime_arr[-1] + self.parameters.ctime_step_periodic) <= self.parameters.ctime_max_periodic:
				ctime_arr.append(ctime_arr[-1] + self.parameters.ctime_step_periodic)
			ctime_arr_norm = (np.array(ctime_arr) - self.parameters.ctime_min_periodic) / (self.parameters.ctime_max_periodic - self.parameters.ctime_min_periodic)
			deltak_arr = periodic_function_selector(self.parameters.periodic_modulation_function_flag,
													ctime_arr_norm,
													self.parameters.periodic_cycle_delta_k, self.parameters.modulation_freq,
													self.parameters.deltak_offset)

			# Instansiate Measurement class
			# (is an instance field: is used from the interrupt button)
			self.meas = self.measurement_mode.Measurement(self.parameters, self.simulation_parameters, self.machine,
														  self.output_path, self.ring, self.japc, self.lsa, self.lsa_context,
														  self.bbq_device_number, self.logger)

			# Setup measurement parameters (counters, read LSA, etc)
			self.meas.setup_measurement('periodic', list(ctime_arr), list(deltak_arr),
										self.ui.SkipCyclePeriodicValue.value(), quads_to_modulate)

			# Subscribe to BCT and BBQ
			self.meas.start_subscriptions()

			# Wait until the measurement is done and stop subscription
			for i in range(len(quads_to_modulate)):
				worker = Worker(lambda: self.meas.get_measurement_when_done())
				worker.signals.result.connect(self.update_periodic_gui_fields)
				if i < (len(quads_to_modulate)-1):
					worker.signals.finished.connect(lambda: [self.lock_measurement_buttons(True),
					                                         self.statusBar().showMessage('Periodic modulation for %s...'%(quads_to_modulate[self.quad_id_counter]))])
				else:
					worker.signals.finished.connect(lambda: [self.lock_measurement_buttons(True),
					                                         self.statusBar().showMessage('Periodic modulation for %s...' % (quads_to_modulate[-1]))])
				self.executor.start(worker)

			# Subscription is finished after last job is finished
			worker = Worker(lambda: self.meas.stop_subscriptions())
			worker.signals.finished.connect(lambda: [self.statusBar().showMessage('Ready.'), self.lock_measurement_buttons(True)])
			self.executor.start(worker)

	# -----------------------------
	# Analysis

	def get_beta_from_fit(self):
		'''
		Applies the non-linear fit to the deltak-Qy points to get mean betas in the quads
		updates the plots
		'''
		worker = Worker(lambda: self.statusBar().showMessage('Applying fits and loading model...'))  # to be done in a background thread
		self.executor.start(worker)
		plane = self.ui.PlaneComboBox.currentText()
		plane_to_plot = {'H': 'x', 'V': 'y'}
		if self.ui.ZScoreGroupBox.isChecked():
			z_score_threshold = self.ui.ZScoreValue.value()
		else:
			z_score_threshold = 0

		print('Applying beta fits (Analysis().apply_beta_fit)...')
		self.logger.info('Applying beta fits (Analysis().apply_beta_fit)...')
		(data_analysis_quad_dict, output_files) = Analysis().apply_beta_fit(self.master_table,
																			self.parameters, self.simulation_parameters,
																			self.output_path,
																			plane_to_plot[plane], z_score_threshold,
																			self.ui.MinYValueValue.value(), self.ui.MaxYValueValue.value(),
																			self.ui.MinXValueValue.value() * 1e-2, self.ui.MaxXValueValue.value() * 1e-2, self.logger)
		print('Done.')
		self.logger.info('Done.')
		qde_colors = {self.parameters.quad_list[0]: 'b', self.parameters.quad_list[1]: 'r'}  # for tune reconstruction colors
		self.ui.QReconstructedGraph.clear()
		print('Plotting...')
		self.logger.info('Plotting...')
		for quad in data_analysis_quad_dict:

			# update file lists
			model = QStandardItemModel(self.betas_from_fit_lists[quad])
			model.appendRow(QStandardItem('Fit summary'))
			model.appendRow(QStandardItem('============'))
			self.betas_from_fit_lists[quad].setModel(model)
			# update file list of PSB correction tab
			if self.parameters.machine_flag == 'PSB':
				if quad == self.parameters.quad_list[0]:
					self.QDE3_beta_fitted_file_list.append(output_files[quad])
					self.QDE3_beta_fitted_model_list.appendRow(QStandardItem(output_files[quad].split("/")[-1]))
				elif quad == self.parameters.quad_list[1]:
					self.QDE14_beta_fitted_file_list.append(output_files[quad])
					self.QDE14_beta_fitted_model_list.appendRow(QStandardItem(output_files[quad].split("/")[-1]))

			# calculate and plot model betas
			self.update_parameters()
			print('Loading model optics (Analysis().get_quad_avg_betas_model)...')
			self.logger.info('Loading model optics (Analysis().get_quad_avg_betas_model)...')
			modelbetas = Analysis().get_quad_avg_betas_model(self.machine, self.simulation_parameters,
															 data_analysis_quad_dict[quad]['ctimes'], quad, plane_to_plot[plane])
			self.model_meas_beta_graphs[quad].clear()
			self.model_meas_beta_graphs[quad].addLegend(offset=(250, 80), labelTextColor=0.0, brush=1.0, pen=pg.mkPen(color=0.0, width=1.0))
			self.model_meas_beta_graphs[quad].plot(data_analysis_quad_dict[quad]['ctimes'], modelbetas,
												   symbol='s', symbolSize=6, symbolBrush=0.0, symbolPen=pg.mkPen(color=0.0),
												   pen=pg.mkPen(color=0.0, width=1.5, style=QtCore.Qt.DashLine), name='model')
			print('Model optics calculated for all ctimes. Plotting measured data...')
			self.logger.info('Model optics calculated for all ctimes. Plotting measured data...')

			for i, ctime in enumerate(data_analysis_quad_dict[quad]['ctimes']):
				if self.ui.StaticCtimesToPlotCheckableList.item(i + 2).checkState() == Qt.Checked:
					# plot Q-dk fits (only the ones that the ctime is checked)
					pen = pg.mkPen(color=(sorted(list(self.current_session_ctimes_static)).index(ctime), len(self.current_session_ctimes_static)), width=2)
					self.beta_graphs[quad].plot(np.array(data_analysis_quad_dict[quad]['x_fromfit'][i]) * 1e2,
												np.array(data_analysis_quad_dict[quad]['y_fromfit'][i]),
												pen=pen, name=None)

					# plot deleted points as x
					self.beta_graphs[quad].plot(np.array(data_analysis_quad_dict[quad]['x_deleted'][i]) * 1e2,
												np.array(data_analysis_quad_dict[quad]['y_deleted'][i]),
												symbol='x', symbolSize=12, pen=None,
												symbolBrush=(sorted(list(self.current_session_ctimes_static)).index(ctime), len(self.current_session_ctimes_static)))

				# update file lists (for all ctimes)
				model.appendRow(QStandardItem('%1.1f ms' % data_analysis_quad_dict[quad]['ctimes'][i]))
				model.appendRow(QStandardItem('Beta = %1.4f (%1.4f)' % (data_analysis_quad_dict[quad]['betay_meas'][i], data_analysis_quad_dict[quad]['betay_meas_err'][i])))
				model.appendRow(QStandardItem('Qy = %1.4f (%1.4f)' % (data_analysis_quad_dict[quad]['Qy_measfit'][i], data_analysis_quad_dict[quad]['Qy_measfit_err'][i])))
				model.appendRow(QStandardItem('----'))

				# plot measured betas (for all ctimes)
				err = pg.ErrorBarItem(x=np.array([data_analysis_quad_dict[quad]['ctimes'][i]]), y=np.array([data_analysis_quad_dict[quad]['betay_meas'][i]]),
									  top=np.array([data_analysis_quad_dict[quad]['betay_meas_err'][i]]), bottom=np.array([data_analysis_quad_dict[quad]['betay_meas_err'][i]]),
									  beam=0.05, pen={'color': (sorted(list(self.current_session_ctimes_static)).index(ctime),
																len(self.current_session_ctimes_static)), 'width': 1})
				self.model_meas_beta_graphs[quad].getPlotItem().addItem(err)
				self.model_meas_beta_graphs[quad].plot([data_analysis_quad_dict[quad]['ctimes'][i]], [data_analysis_quad_dict[quad]['betay_meas'][i]],
													   symbol='o', symbolSize=6, symbolBrush=(sorted(list(self.current_session_ctimes_static)).index(ctime),
																							  len(self.current_session_ctimes_static)))

			# plot reconstructed tunes with errorbars (for all ctimes)
			try:
				err = pg.ErrorBarItem(x=np.array(data_analysis_quad_dict[quad]['ctimes']), y=np.array(data_analysis_quad_dict[quad]['Qy_measfit']),
									  top=np.array(data_analysis_quad_dict[quad]['Qy_measfit_err']), bottom=np.array(data_analysis_quad_dict[quad]['Qy_measfit_err'])*5,
									  left=np.array([(data_analysis_quad_dict[quad]['ctimes'][1] - data_analysis_quad_dict[quad]['ctimes'][0]) * 0.5] * len(
										  data_analysis_quad_dict[quad]['ctimes']))*0, # remove "error" from the ctime shift
									  right=np.array([(data_analysis_quad_dict[quad]['ctimes'][1] - data_analysis_quad_dict[quad]['ctimes'][0]) * 0.5] * len(
										  data_analysis_quad_dict[quad]['ctimes']))*0,  # remove "error" from the ctime shift
									  beam=0.00, pen={'color': 'b', 'width': 1})
			except:
				err = pg.ErrorBarItem(x=np.array(data_analysis_quad_dict[quad]['ctimes']), y=np.array(data_analysis_quad_dict[quad]['Qy_measfit']),
									  top=np.array(data_analysis_quad_dict[quad]['Qy_measfit_err']), bottom=np.array(data_analysis_quad_dict[quad]['Qy_measfit_err']),
									  beam=0.00, pen={'color': 'b', 'width': 1})
			self.ui.QReconstructedGraph.getPlotItem().addItem(err)
			self.ui.QReconstructedGraph.addLegend(labelTextColor=0.0, brush=1.0, pen=pg.mkPen(color=0.0, width=1.0))
			try:
				self.ui.QReconstructedGraph.plot(data_analysis_quad_dict[quad]['ctimes'], data_analysis_quad_dict[quad]['Qy_measfit'],
												 symbol='o', symbolSize=8, symbolBrush=(qde_colors[quad]), name='from %s measurement' % quad)
			except:
				print('Tune reconstruction from PS to be worked out...')  # TBA
				self.ui.QReconstructedGraph.plot(data_analysis_quad_dict[quad]['ctimes'], data_analysis_quad_dict[quad]['Qy_measfit'],
												 symbol='o', symbolSize=8, symbolBrush=0.5)

		# automatically calculate and plot correction strengths
		if ((self.parameters.machine_flag == 'PSB') and (self.ui.PlaneComboBox.currentText() == 'V')):
			self.ui.QDE3LoadedCorrectionDataList.setModel(self.QDE3_beta_fitted_model_list)
			last_index = self.QDE3_beta_fitted_model_list.index(self.QDE3_beta_fitted_model_list.rowCount() - 1, 0)
			self.ui.QDE3LoadedCorrectionDataList.setCurrentIndex(last_index)

			self.ui.QDE14LoadedCorrectionDataList.setModel(self.QDE14_beta_fitted_model_list)
			last_index = self.QDE14_beta_fitted_model_list.index(self.QDE14_beta_fitted_model_list.rowCount() - 1, 0)
			self.ui.QDE14LoadedCorrectionDataList.setCurrentIndex(last_index)

			self.update_parameters()
		# self.interpolate_betas() # uncomment to automatically calculate the correction (slower)

		self.data_analysis_quad_dict = data_analysis_quad_dict
		self.plot_beta_twiss_summary_static()
		self.statusBar().showMessage('Ready.')


	def periodic_tune_fit(self):
		'''
		Applies a sinusoidal fit to the tune modulation
		The points of the sinusoidal fit are used to create the deltak-Qy plot from which the beta is calculated
		'''

		try:
			self.update_parameters()
			plane = self.ui.PeriodicPlaneComboBox.currentText()
			plane_to_plot = {'H': 'x', 'V': 'y'}
			Q_target = {'H': self.ui.PeriodicHorizontalTuneValue.value(), 'V': self.ui.PeriodicVerticalTuneValue.value()}

			y_fromperiodicfit_list = []
			betay_meas = []
			betay_meas_err = []
			Qy_meas_fit = []
			Qy_meas_fit_err = []
			shot_nr         = []
			#x_fromfit = []
			#y_fromfit = []
			for i in range(len(self.master_table_periodic.values)):
				print('Applying periodic fit for file # %i.'%(i+1))
				self.logger.info('Applying periodic fit for file # %i.'%(i+1))
				data = self.master_table_periodic.iloc[i]

				# periodic fit
				# TBA: not working properly? deactivated the widget
				if self.ui.ZScorePeriodicGroupBox.isChecked():
					z_score_threshold = self.ui.ZScorePeriodicValue.value()
				else:
					z_score_threshold = 0
				(popt, perr, x_fromperiodicfit, y_fromperiodicfit) = Analysis().apply_periodic_fit(data,self.parameters,self.simulation_parameters,plane_to_plot[plane],Q_target[plane],z_score_threshold)

				# Get beta from the tunes of the periodic fit (reduces the tune noise)
				res = fit_beta_function(data['deltak'], self.simulation_parameters.qde_length, np.array(y_fromperiodicfit),
										plane=plane_to_plot[plane], z_score_threshold=0, minyval=0.0, maxyval=1.0, minxval=-10.0, maxxval=10.0)

				y_fromperiodicfit_list.append(y_fromperiodicfit)
				betay_meas.append(res[0])
				betay_meas_err.append(res[2])
				Qy_meas_fit.append(res[1])
				Qy_meas_fit_err.append(res[3])
				shot_nr.append(i+1)
				#x_fromfit.append(res[5])
				#y_fromfit.append(res[4])

			self.master_table_periodic['y_fromperiodicfit'] = y_fromperiodicfit_list
			self.master_table_periodic['betay_meas'] = betay_meas
			self.master_table_periodic['betay_meas_err'] = betay_meas_err
			self.master_table_periodic['Qy_measfit'] = Qy_meas_fit
			self.master_table_periodic['Qy_measfit_err'] = Qy_meas_fit_err
			self.master_table_periodic['shot_nr'] = shot_nr
			#self.master_table_periodic['x_fromfit'] = x_fromfit
			#self.master_table_periodic['y_fromfit'] = y_fromfit

			# plot mean
			print('Done.')
			print('Plotting mean values.')
			self.logger.info('Done.')
			self.logger.info('Plotting mean values.')
			self.plot_mean_beta_periodic()

		except Exception as ex:
				print('An error occurred on the periodic_tune_fit. Here is the exception:')
				print(ex)
				self.logger.error('An error occurred on the periodic_tune_fit. Here is the exception:')
				self.logger.error(str(ex))

	# -----------------------------
	# Correction

	def interpolate_betas(self):
		'''
		Calculation of the optimal correction strenghts by interpolating the measured betas to the pre-defined error response matrix
		Output is saved at ../outputs/correction_output/
		'''

		print('Calculating optimal beta-beating correction...')
		self.logger.info('Calculating optimal beta-beating correction...')

		# Correction matrix
		self.update_parameters()
		correction = Correction(self.parameters, self.simulation_parameters, self.machine, self.output_path)
		correction_tbl = correction.get_error_response_matrix_from_madx(self.correction_matrix_file)
		model = QStandardItemModel(self.ui.CorrectionMatrixFileList)
		model.appendRow(QStandardItem(self.correction_matrix_file.split("/")[-1]))
		self.ui.CorrectionMatrixFileList.setModel(model)

		clicked_correction_files = []
		try:
			clicked_correction_files.append(self.QDE3_beta_fitted_file_list[self.ui.QDE3LoadedCorrectionDataList.selectionModel().selection().indexes()[0].row()])
		except:
			pass
		try:
			clicked_correction_files.append(self.QDE14_beta_fitted_file_list[self.ui.QDE14LoadedCorrectionDataList.selectionModel().selection().indexes()[0].row()])
		except:
			pass
		data_analysis_quad_dict = Analysis().load_datafiles(clicked_correction_files)

		ctimes = []
		kd_trims = {}
		kd_deltas = {}
		kf_deltas = {}
		data_correction = {}
		for quad in data_analysis_quad_dict:

			data_correction[quad] = {}
			label = quad
			for key in data_analysis_quad_dict[quad]:
				data_correction[quad][key] = correction.interpolate_betas(correction_tbl, quad, data_analysis_quad_dict[quad][key])

				# Calculate average correction for each quad (slow TBA)
				for i, ctime in enumerate(data_correction[quad][key]['ctimes']):

					if ctime in ctimes:
						kd_trims[ctime].append(data_correction[quad][key]['correction']['kd_trim_correction'][i])
						kd_deltas[ctime].append(data_correction[quad][key]['correction']['kd_delta_correction'][i])
						kf_deltas[ctime].append(data_correction[quad][key]['correction']['kf_delta_correction'][i])

					else:
						kd_trims[ctime] = []
						kd_deltas[ctime] = []
						kf_deltas[ctime] = []
						kd_trims[ctime].append(data_correction[quad][key]['correction']['kd_trim_correction'][i])
						kd_deltas[ctime].append(data_correction[quad][key]['correction']['kd_delta_correction'][i])
						kf_deltas[ctime].append(data_correction[quad][key]['correction']['kf_delta_correction'][i])
						ctimes.append(ctime)

		avg_kd_trims = [np.average(kd_trims[ctime]) for ctime in ctimes]
		avg_kd_deltas = [np.average(kd_deltas[ctime]) for ctime in ctimes]
		avg_kf_deltas = [np.average(kf_deltas[ctime]) for ctime in ctimes]
		# Final correction values (average) in units of 1/m^2 (sorted)
		ctimes, avg_kd_trims, avg_kd_deltas, avg_kf_deltas = (list(t) for t in zip(*sorted(zip(ctimes, avg_kd_trims, avg_kd_deltas, avg_kf_deltas))))

		# plot correction and average
		self.data_for_plot_correction = [data_correction, np.copy(ctimes), np.copy(avg_kd_trims), np.copy(avg_kd_deltas), np.copy(avg_kf_deltas)]
		self.plot_correction()

		# Add an offset to the ctimes (measured betas over 1000 turns) not to be hardcoded TBA
		ctimes = list(np.array(ctimes) + float(self.ui.DeltaTShiftValue.value()))
		# Add ramp up and ramp down
		ramp_up = 0.5
		ramp_down = 1
		if ((ctimes[0] >= 275.5) and (avg_kd_trims[0] != 0)):
			ctimes.insert(0, ctimes[0] - ramp_up)
			avg_kd_trims.insert(0, 0.0)
			avg_kd_deltas.insert(0, 0.0)
			avg_kf_deltas.insert(0, 0.0)
		if ((ctimes[-1] <= 804) and (avg_kd_trims[-1] != 0)):
			ctimes.append(ctimes[-1] + ramp_down)
			avg_kd_trims.append(0.0)
			avg_kd_deltas.append(0.0)
			avg_kf_deltas.append(0.0)

		# Read old strengths from LSA
		if self.measurement_mode_flag == 'expert':
			ctime_kd_trm = [275, 805]
			kd_trm = [0.0, 0.0]
			ctime_kd_delta = [275, 805]
			kd_delta = [0.0, 0.0]
			ctime_kf_delta = [275, 805]
			kf_delta = [0.0, 0.0]
		else:
			(ctime_kd_trm, kd_trm,
			 ctime_kd_delta, kd_delta,
			 ctime_kf_delta, kf_delta) = self.read_lsa()

		# Add old strength to avg values
		(a, b, c, d,
		 self.ctimes_kd_trm_final, self.kd_trm_final) = add_lsa_functions(ctime_kd_trm, kd_trm, ctimes, avg_kd_trims)
		(a, b, c, d,
		 self.ctimes_kd_delta_final, self.kd_delta_final) = add_lsa_functions(ctime_kd_delta, kd_delta, ctimes, avg_kd_deltas)
		(a, b, c, d,
		 self.ctimes_kf_delta_final, self.kf_delta_final) = add_lsa_functions(ctime_kf_delta, kf_delta, ctimes, avg_kf_deltas)

		# Final (interactive) plots
		self.set_default_correction()


	def send_to_lsa(self):
		'''
		Sends the final correction plots (after the edit) to LSA
		'''

		try:
			self.kd_trim_ctimes_after_edit = list(self.trim_curve.xData)
			self.avg_kd_trims_after_edit = list(np.array(self.trim_curve.yData) * 1e-2)
			self.kd_delta_ctimes_after_edit = list(self.deltakd_curve.xData)
			self.avg_kd_deltas_after_edit = list(np.array(self.deltakd_curve.yData) * 1e-2)
			self.kf_delta_ctimes_after_edit = list(self.deltakf_curve.xData)
			self.avg_kf_deltas_after_edit = list(np.array(self.deltakf_curve.yData) * 1e-2)

			print('c-times:')
			print(self.kd_trim_ctimes_after_edit)
			print('kQDE314CORR:')
			print(self.avg_kd_trims_after_edit)
			print('kQDCORR:')
			print(self.avg_kd_deltas_after_edit)
			print('kQFCORR:')
			print(self.avg_kf_deltas_after_edit)

			# Send trims to LSA
			if enable_lsa:
				print('Sending trims to LSA')
				self.logger.info('Sending trims to LSA')
				meas = self.measurement_mode.Measurement(self.parameters, self.simulation_parameters,
														 self.machine, self.output_path, self.ring,
														 self.japc, self.lsa, self.lsa_context, self.bbq_device_number, self.logger)
				meas.set_deltak(self.kd_trim_ctimes_after_edit, self.avg_kd_trims_after_edit,
								'BR%s.QDE3_CORR' % self.ring, 'Correction from k-mod app.')
				meas.set_deltak(self.kd_trim_ctimes_after_edit, self.avg_kd_trims_after_edit,
								'BR%s.QDE14_CORR' % self.ring, 'Correction from k-mod app.')
				meas.set_deltak(self.kd_delta_ctimes_after_edit, self.avg_kd_deltas_after_edit,
								'BR%s.QDE_CORR' % self.ring, 'Correction from k-mod app.')
				meas.set_deltak(self.kf_delta_ctimes_after_edit, self.avg_kf_deltas_after_edit,
								'BR%s.QFO_CORR' % self.ring, 'Correction from k-mod app.')
				print('Trims sent.')
				self.logger.info('Trims sent.')
			else:
				print('Doing nothing.')

		except Exception as ex:
			print('Empty correction plot?')
			print('Sending trims to LSA failed. Here is the exception:')
			print(ex)
			self.logger.warning('Sending trims to LSA failed. Here is the exception:')
			self.logger.warning(str(ex))


	def read_lsa(self):
		'''
		Read all the relevant strengths from LSA for the final correction plots
		'''
		if enable_lsa:
			meas = self.measurement_mode.Measurement(self.parameters, self.simulation_parameters,
													 self.machine, self.output_path, self.ring,
													 self.japc, self.lsa, self.lsa_context, self.bbq_device_number, self.logger)
			ctime_kd_trm, kd_trm = meas.get_deltak('BR%s.QDE3_CORR' % self.ring)  # should be the same as QDE14
			ctime_kd_delta, kd_delta = meas.get_deltak('BR%s.QDE_CORR' % self.ring)
			ctime_kf_delta, kf_delta = meas.get_deltak('BR%s.QFO_CORR' % self.ring)

		else:
			ctime_kd_trm = [275, 805]
			kd_trm = [0.0, 0.0]
			ctime_kd_delta = [275, 805]
			kd_delta = [0.0, 0.0]
			ctime_kf_delta = [275, 805]
			kf_delta = [0.0, 0.0]

		return (ctime_kd_trm, kd_trm,
				ctime_kd_delta, kd_delta,
				ctime_kf_delta, kf_delta)


	def set_default_correction(self):

		self.QDE314TrimCorrectionFinalGraph.clear()
		source_trim_data = DataSource(x_data=np.array(self.ctimes_kd_trm_final), y_data=np.array(self.kd_trm_final) * 1e2)
		self.trim_curve: accgraph.EditablePlotCurve = self.QDE314TrimCorrectionFinalGraph.addCurve(data_source=source_trim_data,
																								   symbol='s', symbolSize=6, symbolBrush=0.0,
																								   pen=pg.mkPen(color=0.0, width=1.5, style=QtCore.Qt.DashLine))
		self.trim_curve.selection.points_labeled = True
		#source_trim_data2 = DataSource(x_data=np.array(self.ctimes_kd_trm_final), y_data=np.array(self.kd_trm_final) * 1e2*0.5)
		#self.trim_curve2: accgraph.EditablePlotCurve = self.QDE314TrimCorrectionFinalGraph.addCurve(data_source=source_trim_data2,
		#                                                                                           symbol='s', symbolSize=6, symbolBrush=0.5,
		#                                                                                           pen=pg.mkPen(color=0.5, width=1.5, style=QtCore.Qt.DashLine))
		#self.trim_curve2.selection.points_labeled = True

		self.QDE314KDDeltaCorrectionFinalGraph.clear()
		source_deltakd_data = DataSource(x_data=np.array(self.ctimes_kd_delta_final), y_data=np.array(self.kd_delta_final) * 1e2)
		self.deltakd_curve: accgraph.EditablePlotCurve = self.QDE314KDDeltaCorrectionFinalGraph.addCurve(data_source=source_deltakd_data,
																										 symbol='s', symbolSize=6, symbolBrush=0.0,
																										 pen=pg.mkPen(color=0.0, width=1.5, style=QtCore.Qt.DashLine))
		self.deltakd_curve.selection.points_labeled = True

		self.QDE314KFDeltaCorrectionFinalGraph.clear()
		source_deltakf_data = DataSource(x_data=np.array(self.ctimes_kf_delta_final), y_data=np.array(self.kf_delta_final) * 1e2)
		self.deltakf_curve: accgraph.EditablePlotCurve = self.QDE314KFDeltaCorrectionFinalGraph.addCurve(data_source=source_deltakf_data,
																										 symbol='s', symbolSize=6, symbolBrush=0.0,
																										 pen=pg.mkPen(color=0.0, width=1.5, style=QtCore.Qt.DashLine))
		self.deltakf_curve.selection.points_labeled = True

	# -----------------------------
	# Load and Delete functions

	def load_raw_data(self):

		datafiles = QFileDialog.getOpenFileNames(self, "Load raw data",
												 "%soutputs/%s/static/raw_data/%s/" % (self.output_path, self.parameters.machine_flag, datetime.now().strftime("%Y.%m.%d")),
												 "json(*.json)", options=QFileDialog.Options())
		if datafiles:
			for datafile in datafiles[0]:
				if not datafile in self.static_measurement_file_list:
					(quad, datafile_name), = Analysis().load_datafiles([datafile]).items()
					(datafile_name, data), = datafile_name.items()  # to unfold the data
					self.master_table = Analysis().update_master_table_static(self.master_table, data)
					self.current_session_ctimes_static = sorted(np.unique(self.master_table['ctime'].values))
					self.static_measurement_file_list.append(datafile)
					self.static_measurement_model_list.appendRow(QStandardItem(datafile.split("/")[-1]))

		# update data list, plots etc.
		self.ui.LoadedSingleCycleDataList.setModel(self.static_measurement_model_list)
		self.refresh_ctime_checkable_list()
		self.plot_raw_data_static()
		self.plot_intensity()
		self.update_parameters()


	def load_periodic_data(self):

		datafiles = QFileDialog.getOpenFileNames(self, "Load periodic data",
											"%soutputs/%s/periodic/raw_data/%s/" % (self.output_path, self.parameters.machine_flag, datetime.now().strftime("%Y.%m.%d")),
											"json(*.json)", options=QFileDialog.Options())
		if datafiles:
			for datafile in datafiles[0]:
				if not datafile in self.periodic_measurement_file_list:
					(quad, datafile_name), = Analysis().load_datafiles([datafile]).items()
					(datafile_name, data), = datafile_name.items()  # to unfold the data
					self.master_table_periodic = Analysis().update_master_table_periodic(self.master_table_periodic, data)
					self.periodic_measurement_file_list.append(datafile)
					self.periodic_measurement_model_list.appendRow(QStandardItem(datafile.split("/")[-1]))

		# update data list, plots etc.
		self.ui.LoadedPeriodicCycleDataList.setModel(self.periodic_measurement_model_list)
		self.plot_raw_data_periodic()
		self.update_parameters()

	def delete_raw_data(self):

		indices_to_remove = []
		for indx in self.ui.LoadedSingleCycleDataList.selectedIndexes():
			indices_to_remove.append(indx.row())
			# remove row from master table
			self.master_table = self.master_table.drop(self.master_table[self.master_table['file'] == self.static_measurement_file_list[indx.row()].split('/')[-1]].index)
		# remove file from file_list
		self.static_measurement_file_list = [i for j, i in enumerate(self.static_measurement_file_list) if j not in indices_to_remove]

		# Re-write the loaded data list
		self.static_measurement_model_list = QStandardItemModel(self.ui.LoadedSingleCycleDataList)
		for output_file in self.static_measurement_file_list:
			self.static_measurement_model_list.appendRow(QStandardItem(output_file.split("/")[-1]))
		self.ui.LoadedSingleCycleDataList.setModel(self.static_measurement_model_list)
		# the connection has to be made again because self.static_measurement_model_list is redefined (to be reviewed)
		self.ui.LoadedSingleCycleDataList.selectionModel().selectionChanged.connect(self.highlight_plot_points)

		# Clear all plots and re-make them
		for quad in self.parameters.quad_list:
			self.model_meas_beta_graphs[quad].clear()
			self.betas_from_fit_lists[quad].setModel(QStandardItemModel(self.betas_from_fit_lists[quad]))
		self.plot_raw_data_static()
		self.plot_intensity()
		self.update_parameters()

	def delete_periodic_data(self):

		indices_to_remove = []
		for indx in self.ui.LoadedPeriodicCycleDataList.selectedIndexes():
			indices_to_remove.append(indx.row())
			# remove row from master table
			self.master_table_periodic = self.master_table_periodic.drop(self.master_table_periodic[self.master_table_periodic['file'] == self.periodic_measurement_file_list[indx.row()].split('/')[-1]].index)
		# remove file from file_list
		self.periodic_measurement_file_list = [i for j, i in enumerate(self.periodic_measurement_file_list) if j not in indices_to_remove]

		# Re-write the loaded data list
		self.periodic_measurement_model_list = QStandardItemModel(self.ui.LoadedPeriodicCycleDataList)
		for output_file in self.periodic_measurement_file_list:
			self.periodic_measurement_model_list.appendRow(QStandardItem(output_file.split("/")[-1]))
		self.ui.LoadedPeriodicCycleDataList.setModel(self.periodic_measurement_model_list)
		# the connection has to be made again because self.periodic_measurement_model_list is redefined (to be reviewed)
		self.ui.LoadedPeriodicCycleDataList.selectionModel().selectionChanged.connect(self.highlight_plot_points_periodic)

		# Clear all plots and re-make them
		for quad in self.parameters.quad_list:
			self.dk_modulation_periodic[quad].clear()
			self.q_modulation_periodic[quad].clear()
		self.plot_raw_data_periodic()
		try:
			self.plot_mean_beta_periodic()
		except:
			pass

		self.update_parameters()

	def delete_all_static_data(self):
		# clear fields and plots
		self.static_measurement_file_list = []
		self.static_measurement_model_list = QStandardItemModel(self.ui.LoadedSingleCycleDataList)
		self.ui.LoadedSingleCycleDataList.setModel(self.static_measurement_model_list)
		self.master_table = pd.DataFrame(columns=[])
		# the connection has to be made again because self.static_measurement_model_list is redefined (to be reviewed)
		self.ui.LoadedSingleCycleDataList.selectionModel().selectionChanged.connect(self.highlight_plot_points)
		for quad in self.parameters.quad_list:
			self.beta_graphs[quad].clear()
			self.model_meas_beta_graphs[quad].clear()
			try:
				self.betas_from_fit_lists[quad].model().removeRows(0, self.betas_from_fit_lists[quad].model().rowCount())
			except:  # to avoid error in case of empty lists
				pass
		self.ui.BetaSummaryTwissGraph.clear()
		self.ui.QReconstructedGraph.clear()
		self.ui.BRBCTIntensityGraph.clear()

	def delete_all_periodic_data(self):
		# clear fields and pltos
		self.periodic_measurement_file_list = []
		self.periodic_measurement_model_list = QStandardItemModel(self.ui.LoadedPeriodicCycleDataList)
		self.ui.LoadedPeriodicCycleDataList.setModel(self.periodic_measurement_model_list)
		self.master_table_periodic = pd.DataFrame(columns=[])
		# the connection has to be made again because self.periodic_measurement_model_list is redefined (to be reviewed)
		self.ui.LoadedPeriodicCycleDataList.selectionModel().selectionChanged.connect(self.highlight_plot_points_periodic)
		for quad in self.parameters.quad_list:
			self.beta_graphs_periodic[quad].clear()
			self.q_modulation_periodic[quad].clear()
			self.dk_modulation_periodic[quad].clear()
		self.ui.PeriodicBRBCTIntensityGraph.clear()
		self.ui.PeriodicBetaSummaryTwissGraph.clear()

	def load_beta_fitted_data(self):

		options = QFileDialog.Options()
		data = QFileDialog.getOpenFileNames(self, "Load beta-fitted data",
											"%soutputs/%s/static/beta_output/%s/" % (self.output_path, self.parameters.machine_flag, datetime.now().strftime("%Y.%m.%d")),
											"json(*.json)", options=options)
		if data:
			for datafile in data[0]:
				if datafile.split("/")[-1].split('_')[0] == 'BR.QDE3':
					self.QDE3_beta_fitted_file_list.append(datafile)
					self.QDE3_beta_fitted_model_list.appendRow(QStandardItem(datafile.split("/")[-1]))
				if datafile.split("/")[-1].split('_')[0] == 'BR.QDE14':
					self.QDE14_beta_fitted_file_list.append(datafile)
					self.QDE14_beta_fitted_model_list.appendRow(QStandardItem(datafile.split("/")[-1]))

			self.ui.QDE3LoadedCorrectionDataList.setModel(self.QDE3_beta_fitted_model_list)
			last_index = self.QDE3_beta_fitted_model_list.index(self.QDE3_beta_fitted_model_list.rowCount() - 1, 0)
			self.ui.QDE3LoadedCorrectionDataList.setCurrentIndex(last_index)

			self.ui.QDE14LoadedCorrectionDataList.setModel(self.QDE14_beta_fitted_model_list)
			last_index = self.QDE14_beta_fitted_model_list.index(self.QDE14_beta_fitted_model_list.rowCount() - 1, 0)
			self.ui.QDE14LoadedCorrectionDataList.setCurrentIndex(last_index)

	# self.interpolate_betas() # do not automatically calculate correction

	def delete_beta_fitted_data(self):

		# QDE3
		try:
			indices_to_remove = []
			for index in self.ui.QDE3LoadedCorrectionDataList.selectedIndexes():
				indices_to_remove.append(index.row())
			self.QDE3_beta_fitted_file_list = [i for j, i in enumerate(self.QDE3_beta_fitted_file_list) if j not in indices_to_remove]
			self.QDE3_beta_fitted_model_list = QStandardItemModel(self.ui.QDE3LoadedCorrectionDataList)
			for output_file in self.QDE3_beta_fitted_file_list:
				self.QDE3_beta_fitted_model_list.appendRow(QStandardItem(output_file.split("/")[-1]))
			self.ui.QDE3LoadedCorrectionDataList.setModel(self.QDE3_beta_fitted_model_list)
			last_index = self.QDE3_beta_fitted_model_list.index(self.QDE3_beta_fitted_model_list.rowCount() - 1, 0)
			self.ui.QDE3LoadedCorrectionDataList.setCurrentIndex(last_index)
		except:
			pass

		# QDE14
		try:
			indices_to_remove = []
			for index in self.ui.QDE14LoadedCorrectionDataList.selectedIndexes():
				indices_to_remove.append(index.row())
			self.QDE14_beta_fitted_file_list = [i for j, i in enumerate(self.QDE14_beta_fitted_file_list) if j not in indices_to_remove]
			self.QDE14_beta_fitted_model_list = QStandardItemModel(self.ui.QDE14LoadedCorrectionDataList)
			for output_file in self.QDE14_beta_fitted_file_list:
				self.QDE14_beta_fitted_model_list.appendRow(QStandardItem(output_file.split("/")[-1]))
			self.ui.QDE14LoadedCorrectionDataList.setModel(self.QDE14_beta_fitted_model_list)
			last_index = self.QDE14_beta_fitted_model_list.index(self.QDE14_beta_fitted_model_list.rowCount() - 1, 0)
			self.ui.QDE14LoadedCorrectionDataList.setCurrentIndex(last_index)
		except:
			pass

		# Clear all plots and re-make them
		try:
			self.ui.QDE314TrimCorrectionGraph.clear()
			self.ui.QDE314KDDeltaCorrectionGraph.clear()
			self.ui.QDE314KFDeltaCorrectionGraph.clear()
			self.interpolate_betas()
		except Exception as ex:
			print(ex)

	def load_correction_matrix_file(self):

		data = QFileDialog.getOpenFileNames(self, "Load pre-calculated correction matrix", 'k_mod_app/acc-models-psb/operation/k_modulation/error_response_matrix/',
											"tfs(*.tfs)", options=QFileDialog.Options())
		if data:
			for datafile in data[0]:
				self.correction_matrix_file = datafile
		model = QStandardItemModel(self.ui.CorrectionMatrixFileList)
		model.appendRow(QStandardItem(self.correction_matrix_file.split("/")[-1]))
		self.ui.CorrectionMatrixFileList.setModel(model)

		# update correction
		self.interpolate_betas()

	# -----------------------------
	# GUI-related (plots etc.)

	def update_static_gui_fields(self, result):
		try:
			# the measurement_counter and the quad_id_label is for the statusBar
			data, output_file, self.quad_id_counter, self.measurement_counter = result
			if output_file != None:
				self.master_table = Analysis().update_master_table_static(self.master_table, data)
				self.current_session_ctimes_static = sorted(np.unique(self.master_table['ctime'].values))
				self.static_measurement_file_list.append(output_file)
				self.static_measurement_model_list.appendRow(QStandardItem(output_file.split("/")[-1]))
				self.ui.LoadedSingleCycleDataList.setModel(self.static_measurement_model_list)
				self.refresh_ctime_checkable_list()
				self.plot_raw_data_static()
				self.plot_intensity()
			else:
				print('GUI fields were not updated: output_file = None')
		except Exception as ex:
			print("An exception occurred in the update_static_gui_fields. Here is the exception:")
			print(ex)
			print('Ignoring...')
		#self.statusBar().showMessage('Ready.')
		self.update_parameters()

	def update_periodic_gui_fields(self, result):
		try:
			data, output_file, self.quad_id_counter, self.measurement_counter = result
			if output_file != None:
				self.master_table_periodic = Analysis().update_master_table_periodic(self.master_table_periodic, data)
				self.periodic_measurement_file_list.append(output_file)
				self.periodic_measurement_model_list.appendRow(QStandardItem(output_file.split("/")[-1]))
				self.ui.LoadedPeriodicCycleDataList.setModel(self.periodic_measurement_model_list)
				self.update_parameters()
				#last_item = self.periodic_measurement_model_list.item(self.periodic_measurement_model_list.rowCount() - 1, 0)
				#self.ui.LoadedPeriodicCycleDataList.setCurrentIndex(self.periodic_measurement_model_list.indexFromItem(last_item))
				self.plot_raw_data_periodic()
			else:
				print('GUI fields were not updated')
		except Exception as ex:
			print("An exception occurred in the update_periodic_gui_fields. Here is the exception:")
			print(ex)
			print('Ignoring...')
		self.statusBar().showMessage('Ready.')

	def lock_measurement_buttons(self, boolean):
		self.ui.OneCycleMeasurementButton.setEnabled(boolean)
		self.ui.MultipleCycleMeasurementButton.setEnabled(boolean)
		self.ui.PeriodicMeasurementButton.setEnabled(boolean)
		nboolean = not boolean
		self.ui.OneCycleInterruptButton.setEnabled(nboolean)
		self.ui.MultipleCycleInterruptButton.setEnabled(nboolean)
		self.ui.PeriodicInterruptButton.setEnabled(nboolean)

	def refresh_ctime_checkable_list(self):

		# adds a ctime in the qlistview static and in the beta summary ctime combo box
		# everytime a new measurment is done
		self.ui.StaticCtimesToPlotCheckableList.clear()
		self.ui.BetaSummaryCtimeComboBox.clear()
		self.ui.StaticCtimesToPlotCheckableList.addItem('none')
		self.ui.StaticCtimesToPlotCheckableList.item(0).setCheckState(QtCore.Qt.Unchecked)
		self.ui.StaticCtimesToPlotCheckableList.addItem('all')
		self.ui.StaticCtimesToPlotCheckableList.item(1).setCheckState(QtCore.Qt.Unchecked)
		for i, ctime in enumerate(sorted(list(self.current_session_ctimes_static))):
			self.ui.StaticCtimesToPlotCheckableList.addItem(str(ctime))
			if i < 8:  # check only first few ctimes
				self.ui.StaticCtimesToPlotCheckableList.item(i + 2).setCheckState(QtCore.Qt.Checked)
			else:
				self.ui.StaticCtimesToPlotCheckableList.item(i + 2).setCheckState(QtCore.Qt.Unchecked)
			self.ui.BetaSummaryCtimeComboBox.addItem(str(ctime))

	def combobox_select_all_none(self, item, parent_widget, nr_of_items, callback):
		# Selects "all" or "none" of the parent_widget combobox elements and calls a callback function
		checkState = None
		if ((item.text() == 'all') and (item.checkState() == Qt.Checked)):
			parent_widget.item(0).setCheckState(QtCore.Qt.Unchecked)
			checkState = QtCore.Qt.Checked
		elif ((item.text() == 'none') and (item.checkState() == Qt.Checked)):
			parent_widget.item(1).setCheckState(QtCore.Qt.Unchecked)
			checkState = QtCore.Qt.Unchecked
		if checkState != None:
			for i in range(nr_of_items):
				parent_widget.item(i + 2).setCheckState(checkState)
			callback()

	def custom_popup_window(self, title, message, icon, button):
		msg = QMessageBox()
		msg.setWindowTitle(title)
		msg.setText(message)
		msg.setIcon(icon)
		msg.setStandardButtons(button)
		msg.exec_()

	# -----------------------------
	# Plotting stuff

	def plot_raw_data_static(self):

		# if all datafiles are removed from the list
		if not self.static_measurement_file_list:

			for quad in self.parameters.quad_list:
				self.beta_graphs[quad].clear()
				self.model_meas_beta_graphs[quad].clear()
				self.betas_from_fit_lists[quad].setModel(QStandardItemModel(self.betas_from_fit_lists[quad]))
			self.current_session_ctimes_static = set([])
			self.refresh_ctime_checkable_list()

		else:

			self.ui.BetaSummaryTwissGraph.clear()
			plane = self.ui.PlaneComboBox.currentText()
			plane_to_plot = {'H': 'x', 'V': 'y'}
			quads = np.unique(self.master_table['quad'].values)
			for quad in quads:
				self.beta_graphs[quad].clear()
				self.model_meas_beta_graphs[quad].clear()
				self.beta_graphs[quad].addLegend(offset=(350, 10), labelTextColor=0.0, brush=1.0, pen=pg.mkPen(color=0.0, width=1.0))
				quad_mask = np.where(self.master_table['quad'].values == quad)
				for ctime_indx, ctime in enumerate(sorted(self.current_session_ctimes_static)):
					if ((ctime in np.unique(self.master_table['ctime'].values[quad_mask])) and (
							self.ui.StaticCtimesToPlotCheckableList.item(ctime_indx + 2).checkState() == Qt.Checked)):

						ctime_mask = np.where((self.master_table['ctime'].values == ctime) & (self.master_table['quad'].values == quad))
						try:
							self.beta_graphs[quad].plot(list(self.master_table['deltak'].values[ctime_mask] * 1e2),
														list(self.master_table['Q%s_meas' % plane_to_plot[plane]].values[ctime_mask]),
														symbol='o', symbolSize=6, pen=None,
														symbolBrush=(ctime_indx, len(self.current_session_ctimes_static)),
														name="%s ms" % ctime)
						except Exception as ex:
							print('Error in the plot_raw_data_static. Here is the exception:')
							print(ex)

	def highlight_plot_points(self, selected, deselected):
		try:
			self.plot_raw_data_static()
			self.plot_intensity()

			clicked_output_file = selected.indexes()[0].data()
			clicked_output_file_mask = np.where(self.master_table['file'] == clicked_output_file)

			plane = self.ui.PlaneComboBox.currentText()
			plane_to_plot = {'H': 'x', 'V': 'y'}

			self.beta_graphs[clicked_output_file.split('_')[0]].plot(list(self.master_table['deltak'].values[clicked_output_file_mask] * 1e2),
																	 list(self.master_table['Q%s_meas' % plane_to_plot[plane]].values[clicked_output_file_mask]),
																	 symbol='+', symbolSize=15, pen=None, symbolBrush=0.0)

			self.ui.BRBCTIntensityGraph.plot([self.master_table['timestamp'].values[clicked_output_file_mask][0]],
											 [self.master_table['intensity_at_last_ctime'].values[clicked_output_file_mask][0]],
											 symbol='+', symbolSize=15, pen=None, symbolBrush=0.0)
		except:
			print('self.highlight_plot_points failed')

	def plot_raw_data_periodic(self):

		try:
			plane = self.ui.PeriodicPlaneComboBox.currentText()
			plane_to_plot = {'H': 'x', 'V': 'y'}
			self.ui.PeriodicBRBCTIntensityGraph.clear()
			quads = np.unique(self.master_table_periodic['quad'].values)
			for quad in quads:

				self.dk_modulation_periodic[quad].clear()
				self.q_modulation_periodic[quad].clear()

				quad_mask = np.where(self.master_table_periodic['quad']==quad)
				(ar,) = quad_mask
				for i in range(len(ar)):

					self.dk_modulation_periodic[quad].plot(self.master_table_periodic['ctime'].values[quad_mask][i], np.array(self.master_table_periodic['deltak'].values[quad_mask][i]) * 1e2,
														   symbol='o', symbolSize=5, symbolBrush=(i,len(ar)), pen=None, name=None)
					self.q_modulation_periodic[quad].plot(self.master_table_periodic['ctime'].values[quad_mask][i],self.master_table_periodic['Q%s_meas' % plane_to_plot[plane]].values[quad_mask][i],
														   symbol='o', symbolSize=5, symbolBrush=(i,len(ar)), pen=None, name=None)
					# intensity plot
					self.ui.PeriodicBRBCTIntensityGraph.plot([self.master_table_periodic['timestamp'].values[quad_mask][i]],
					                                         [self.master_table_periodic['intensity_at_last_ctime'].values[quad_mask][i]],
															 symbol='s', symbolSize=6, pen=None, symbolBrush=0.0)

		except Exception as ex:
			print('Exception in the plot_raw_data_periodic. Here is the exception:')
			print(ex)


	def highlight_plot_points_periodic(self, selected, deselected):
		# it way faster than the highlight_plot_points: almost independent of the amount of data !
		# but it always plots on top of previous points (this is why "+" is not used...)

		try:
			plane = self.ui.PeriodicPlaneComboBox.currentText()
			plane_to_plot = {'H': 'x', 'V': 'y'}

			try:
				# replot the deselected file with the right color ()
				deselected_output_file = deselected.indexes()[0].data()
				deselected_output_file_mask = np.where(self.master_table_periodic['file']==deselected_output_file)
				quad = deselected_output_file.split('_')[0]
				quad_mask = np.where(self.master_table_periodic['quad'] == quad)
				(ar,) = quad_mask
				i = list(self.master_table_periodic['file'].values[quad_mask]).index(self.master_table_periodic['file'].values[deselected_output_file_mask][0])
				self.dk_modulation_periodic[quad].plot(self.master_table_periodic['ctime'].values[deselected_output_file_mask][0],
													   np.array(self.master_table_periodic['deltak'].values[deselected_output_file_mask][0]) * 1e2,
													   symbol='o', symbolSize=5, symbolBrush=(i, len(ar)), pen=None, name=None)
				self.q_modulation_periodic[quad].plot(self.master_table_periodic['ctime'].values[deselected_output_file_mask][0],
													  self.master_table_periodic['Q%s_meas' % plane_to_plot[plane]].values[deselected_output_file_mask][0],
													  symbol='o', symbolSize=5, symbolBrush=(i, len(ar)), pen=None, name=None)
				self.ui.PeriodicBRBCTIntensityGraph.plot([self.master_table_periodic['timestamp'].values[deselected_output_file_mask][0]],
			                                         [self.master_table_periodic['intensity_at_last_ctime'].values[deselected_output_file_mask][0]],
			                                         symbol='s', symbolSize=6, pen=None, symbolBrush=0.0)
				try:
					self.beta_graphs_periodic[quad].plot([self.master_table_periodic['shot_nr'].values[deselected_output_file_mask][0]],
					                                     [self.master_table_periodic['betay_meas'].values[deselected_output_file_mask][0]],
					                                     symbol='o', symbolSize=8, symbolBrush=(i,len(ar)), pen=None)
				except:
					pass
			except:
				pass

			# highlight the selected file
			clicked_output_file = selected.indexes()[0].data()
			clicked_output_file_mask = np.where(self.master_table_periodic['file'] == clicked_output_file)
			quad = clicked_output_file.split('_')[0]
			self.dk_modulation_periodic[quad].plot(self.master_table_periodic['ctime'].values[clicked_output_file_mask][0],
												   np.array(self.master_table_periodic['deltak'].values[clicked_output_file_mask][0])*1e2,
												   symbol='o', symbolSize=5, pen=None, symbolBrush=0.0)
			self.q_modulation_periodic[quad].plot(self.master_table_periodic['ctime'].values[clicked_output_file_mask][0],
												  self.master_table_periodic['Q%s_meas' % plane_to_plot[plane]].values[clicked_output_file_mask][0],
												  symbol='o', symbolSize=5, pen=None, symbolBrush=0.0)
			try:
				self.ui.PeriodicBRBCTIntensityGraph.plot([self.master_table_periodic['timestamp'].values[clicked_output_file_mask][0]],
			                                         [self.master_table_periodic['intensity_at_last_ctime'].values[clicked_output_file_mask][0]],
			                                         symbol='s', symbolSize=6, pen=None, symbolBrush=0.5)
			except Exception as ex:
				print(ex)
			try:
				self.beta_graphs_periodic[quad].plot([self.master_table_periodic['shot_nr'].values[clicked_output_file_mask][0]],
				                                     [self.master_table_periodic['betay_meas'].values[clicked_output_file_mask][0]],
				                                     symbol='o', symbolSize=8, symbolBrush=0.0, pen=None)
			except:
				pass
		except:
			pass


	def clear_beta_graphs_periodic(self):
		quads = np.unique(self.master_table_periodic['quad'].values)
		for quad in quads:
			self.beta_graphs_periodic[quad].clear()

	def plot_mean_beta_periodic(self):

		self.clear_beta_graphs_periodic()

		# Plot betas from fit
		for i in range(len(self.master_table_periodic.values)):
			quad = self.master_table_periodic['quad'].values[i]
			quad_mask = np.where(self.master_table_periodic['quad'] == quad)
			(ar,) = quad_mask
			j = list(self.master_table_periodic['file'].values[quad_mask]).index(self.master_table_periodic['file'].values[i])
			self.q_modulation_periodic[quad].plot(self.master_table_periodic['ctime'].values[i], self.master_table_periodic['y_fromperiodicfit'].values[i], pen=pg.mkPen(color=(j, len(ar))))
			self.beta_graphs_periodic[quad].plot([i + 1], [self.master_table_periodic['betay_meas'].values[i]], symbol='o', symbolSize=8, symbolBrush=(j, len(ar)), pen=None)

		# Plot mean value
		quads = np.unique(self.master_table_periodic['quad'].values)
		for quad in quads:
			quad_mask = np.where(self.master_table_periodic['quad'] == quad)

			mean = float(np.mean(self.master_table_periodic['betay_meas'].values[quad_mask]))
			std = float(np.std(self.master_table_periodic['betay_meas'].values[quad_mask]))
			mean_line = pg.InfiniteLine(mean, angle=0, pen=pg.mkPen(color=0.0, width=2.0,style=QtCore.Qt.DashLine), label='Mean %1.4f (%1.4f)'%(mean,std),
			                            labelOpts={'color': (0,0,0), 'fill': (255,255,255,255), 'movable': True})
			self.beta_graphs_periodic[quad].addItem(mean_line)


	def plot_intensity(self):

		# if all datafiles are removed from the list
		if not self.static_measurement_file_list:
			self.ui.BRBCTIntensityGraph.clear()

		else:
			self.ui.BRBCTIntensityGraph.clear()
			self.ui.BRBCTIntensityGraph.plot(list(self.master_table['timestamp'].values),
											 list(self.master_table['intensity_at_last_ctime'].values),
											 symbol='s', symbolSize=6, pen=None, symbolBrush=0.0)

	def plot_beta_twiss_summary_static(self):
		'''
		plots the measured and model betas in the summary tab
		'''

		# load model
		try:
			selected_ctime = self.ui.BetaSummaryCtimeComboBox.currentText()
			plane = self.ui.PlaneComboBox.currentText()
			plane_to_plot = {'H': 'x', 'V': 'y'}
			s, model_beta, model_alpha = Analysis().get_model_optics(self.machine, self.simulation_parameters,
																	 float(selected_ctime), plane_to_plot[plane], centre=True)
			self.ui.BetaSummaryTwissGraph.clear()
			self.ui.BetaSummaryTwissGraph.plot(s, model_beta, pen=pg.mkPen(color=0.0, width=2.0))
			for quad in self.data_analysis_quad_dict:
				try:
					i = (self.data_analysis_quad_dict[quad]['ctimes']).index(float(selected_ctime))
					err = pg.ErrorBarItem(x=np.array([self.simulation_parameters.quad_to_position_dict[quad] - self.simulation_parameters.qde_length / 2]),
										  y=np.array([self.data_analysis_quad_dict[quad]['betay_meas'][i]]),
										  top=np.array([self.data_analysis_quad_dict[quad]['betay_meas_err'][i]]),
										  bottom=np.array([self.data_analysis_quad_dict[quad]['betay_meas_err'][i]]),beam=0.5,pen={'color': 'r', 'width': 2})
					self.ui.BetaSummaryTwissGraph.getPlotItem().addItem(err)
					self.ui.BetaSummaryTwissGraph.plot([self.simulation_parameters.quad_to_position_dict[quad] - self.simulation_parameters.qde_length / 2],
													   [self.data_analysis_quad_dict[quad]['betay_meas'][i]],symbol='o', symbolSize=8,symbolBrush='r')
				except:
					print('%s is not in %s' % (selected_ctime, quad))
		except:
			pass

	def plot_beta_twiss_summary_periodic(self):

		self.update_parameters()
		selected_ctime = '300'
		plane = self.ui.PeriodicPlaneComboBox.currentText()
		plane_to_plot = {'H': 'x', 'V': 'y'}

		# Plot model
		s, model_beta, model_alpha = Analysis().get_model_optics(self.machine, self.simulation_parameters,float(selected_ctime), plane_to_plot[plane])
		self.ui.PeriodicBetaSummaryTwissGraph.clear()
		self.ui.PeriodicBetaSummaryTwissGraph.plot(s, model_beta, pen=pg.mkPen(color=0.0, width=2.0))

		# Plot mean values
		try:
			mean_betas = []
			mean_betas_err = []
			mean_quad_positions = []
			quads = np.unique(self.master_table_periodic['quad'].values)
			for quad in quads:
				quad_mask = np.where(self.master_table_periodic['quad'] == quad)
				mean_betas.append(float(np.mean(self.master_table_periodic['betay_meas'].values[quad_mask])))
				mean_betas_err.append(float(np.std(self.master_table_periodic['betay_meas'].values[quad_mask])))
				mean_quad_positions.append(self.simulation_parameters.quad_to_position_dict[quad])
			err = pg.ErrorBarItem(x=np.array(mean_quad_positions),y=np.array(mean_betas),top=np.array(mean_betas_err),bottom=np.array(mean_betas_err),beam=0.5,pen={'color': 'r', 'width': 2})
			self.ui.PeriodicBetaSummaryTwissGraph.getPlotItem().addItem(err)
			self.ui.PeriodicBetaSummaryTwissGraph.plot(mean_quad_positions, mean_betas,symbol='o', symbolsize=8, symbolBrush='r', pen=None)
		except:
			pass



	def plot_correction(self):

		print('Plotting correction.')
		self.ui.QDE314TrimCorrectionGraph.clear()
		self.ui.QDE314TrimCorrectionGraph.addLegend(labelTextColor=0.0, brush=1.0, pen=pg.mkPen(color=0.0, width=1.0))
		self.ui.QDE314KDDeltaCorrectionGraph.clear()
		self.ui.QDE314KFDeltaCorrectionGraph.clear()

		qde_colors = {self.parameters.quad_list[0]: 'b', self.parameters.quad_list[1]: 'r'}

		data_correction = self.data_for_plot_correction[0]
		for quad in data_correction:
			if self.CorrectionCheckBox[quad].isChecked():
				label = quad
				for key in data_correction[quad]:
					self.ui.QDE314TrimCorrectionGraph.plot(np.array(data_correction[quad][key]['ctimes']),
														   np.array(data_correction[quad][key]['correction']['kd_trim_correction']) * 1e2,
														   symbol='o', symbolSize=8, symbolBrush=(qde_colors[quad]), name='from %s measurement' % label)
					self.ui.QDE314KDDeltaCorrectionGraph.plot(np.array(data_correction[quad][key]['ctimes']),
															  np.array(data_correction[quad][key]['correction']['kd_delta_correction']) * 1e2,
															  symbol='o', symbolSize=4, symbolBrush=(qde_colors[quad]), name=None)
					self.ui.QDE314KFDeltaCorrectionGraph.plot(np.array(data_correction[quad][key]['ctimes']),
															  np.array(data_correction[quad][key]['correction']['kf_delta_correction']) * 1e2,
															  symbol='o', symbolSize=4, symbolBrush=(qde_colors[quad]), name=None)
					label = None  # to show the label only once

		# Plot average
		if self.ui.CorrectionAverageCheckBox.isChecked():
			# ctimes, avg_kd_trims, avg_kd_trims, avg_kf_deltas
			pen = pg.mkPen(color=0.0, width=1, style=QtCore.Qt.DashLine)
			self.ui.QDE314TrimCorrectionGraph.plot(np.array(self.data_for_plot_correction[1]), np.array(self.data_for_plot_correction[2]) * 1e2, name='Avg', pen=pen)
			self.ui.QDE314KDDeltaCorrectionGraph.plot(np.array(self.data_for_plot_correction[1]), np.array(self.data_for_plot_correction[3]) * 1e2, name=None, pen=pen)
			self.ui.QDE314KFDeltaCorrectionGraph.plot(np.array(self.data_for_plot_correction[1]), np.array(self.data_for_plot_correction[4]) * 1e2, name=None, pen=pen)

	# -----------------------------
	# Other

	def get_help(self):
		print("Help")
		about_the_application()

	def update_bbq_settings(self, mode):

		global acqOffset
		bbq_device, bct_device = self.machine.get_device_names(self.ring, self.bbq_device_number)

		try:
			# Acquisition Settings

			# to set the BBQ according to the widget values of the different measurement modes (static/periodic)
			# default (mode == 'static')
			if self.parameters.machine_flag.lower() == 'psb':
				acqOffset = int(self.ui.StaticCTimeMinValue.value() - self.ui.StaticCTimeMinValue.minimum())
			elif self.parameters.machine_flag.lower() == 'ps':
				acqOffset = int(self.ui.StaticCTimeMinValue.value() - 170)  # Hardcoded TBA
			acqPeriod = int(self.ui.StaticStepMsValue.value())
			nbOfMeas = int(round((self.ui.StaticCTimeMaxValue.value() - self.ui.StaticCTimeMinValue.value()) / (self.ui.StaticStepMsValue.value())) + 1)
			if mode == 'periodic':
				if self.parameters.machine_flag.lower() == 'psb':
					acqOffset = int(self.ui.PeriodicCTimeMinValue.value() - self.ui.PeriodicCTimeMinValue.minimum())
				elif self.parameters.machine_flag.lower() == 'ps':
					acqOffset = int(self.ui.PeriodicCTimeMinValue.value() - 170)  # Hardcoded TBA
				acqPeriod = int(self.ui.PeriodicStepMsValue.value())
				nbOfMeas = int(round((self.ui.PeriodicCTimeMaxValue.value() - self.ui.PeriodicCTimeMinValue.value()) / (self.ui.PeriodicStepMsValue.value())) + 1)
			self.ui.acqOffsetValue.setValue(acqOffset)
			self.ui.acqPeriodValue.setValue(acqPeriod)
			self.ui.nbOfMeasValue.setValue(nbOfMeas)

			acq_settings = {'nbOfTurns': self.ui.nbOfTurnsValue.value(), 'acqOffset': self.ui.acqOffsetValue.value(),
							'acqPeriod': int(self.ui.acqPeriodValue.value()), 'nbOfMeas': self.ui.nbOfMeasValue.value()}
			self.japc.setParam('%s/Setting' % bbq_device, acq_settings)

			# Excitation Settings
			if self.ui.BBQExcitationSettingsGroupBox.isChecked():
				# frequency window
				max_ctime_freq_window_set = float(self.ui.StaticCTimeMaxValue.value())  # default (mode == 'static')
				if mode == 'periodic':
					max_ctime_freq_window_set = float(self.ui.PeriodicCTimeMaxValue.value())

				minSearchWindowFreqH_value = self.ui.minSearchWindowFreqHValue.value()
				maxSearchWindowFreqH_value = self.ui.maxSearchWindowFreqHValue.value()
				minSearchWindowFreqH_array = np.array([[0.00e+00, max_ctime_freq_window_set],
													   [minSearchWindowFreqH_value, minSearchWindowFreqH_value]])
				maxSearchWindowFreqH_array = np.array([[0.00e+00, max_ctime_freq_window_set],
													   [maxSearchWindowFreqH_value, maxSearchWindowFreqH_value]])
				minSearchWindowFreqV_value = self.ui.minSearchWindowFreqVValue.value()
				maxSearchWindowFreqV_value = self.ui.maxSearchWindowFreqVValue.value()
				minSearchWindowFreqV_array = np.array([[0.00e+00, max_ctime_freq_window_set],
													   [minSearchWindowFreqV_value, minSearchWindowFreqV_value]])
				maxSearchWindowFreqV_array = np.array([[0.00e+00, max_ctime_freq_window_set],
													   [maxSearchWindowFreqV_value, maxSearchWindowFreqV_value]])

				freq_window_settings = {'minSearchWindowFreqH': minSearchWindowFreqH_array, 'maxSearchWindowFreqH': maxSearchWindowFreqH_array,
										'minSearchWindowFreqV': minSearchWindowFreqV_array, 'maxSearchWindowFreqV': maxSearchWindowFreqV_array}
				self.japc.setParam('%s/Setting' % bbq_device, freq_window_settings)

				# Excitation mode
				exMode = {'NONE': 0, 'CHIRP': 1, 'PULSE': 2}
				self.japc.setParam('%s/Setting#exMode' % bbq_device, exMode[self.ui.exModeComboBox.currentText()])
				if self.ui.exModeComboBox.currentText() != 'NONE':

					exMaskTune = {'TRUE': 1, 'FALSE': 0}
					excitation_settings_1 = {'exEnableH': self.ui.exEnabledVGroupBox.isChecked(), 'exEnableV': self.ui.exEnabledVGroupBox.isChecked(),
											 'exAmplitudeH': self.ui.exAmplitudeHValue.value(), 'exAmplitudeV': self.ui.exAmplitudeVValue.value(),
											 'exMaskTune': exMaskTune[self.ui.exMaskTuneComboBox.currentText()]}
					self.japc.setParam('%s/Setting' % bbq_device, excitation_settings_1)

					# if nbOfMeas <= 1:
					#	self.ui.exPatternComboBox.setCurrentIndex(1)
					exPattern = {'ALTERNATE': 0, 'CONCURRENT': 1, 'ADVANCED': 2}
					self.japc.setParam('%s/Setting#exPattern' % bbq_device, exPattern[self.ui.exPatternComboBox.currentText()])
					if self.ui.exPatternComboBox.currentText() == 'ADVANCED':
						exAdvancedTimesH = np.arange(self.ui.exAdvancedTimesHMinValue.value(),
													 self.ui.exAdvancedTimesHMaxValue.value(),
													 self.ui.exAdvancedTimesHStepValue.value())
						exAdvancedTimesV = np.arange(self.ui.exAdvancedTimesVMinValue.value(),
													 self.ui.exAdvancedTimesVMaxValue.value(),
													 self.ui.exAdvancedTimesVStepValue.value())
						excitation_settings_2 = {'exAdvancedTimesH': exAdvancedTimesH, 'exAdvancedTimesV': exAdvancedTimesV}
						self.japc.setParam('%s/Setting' % bbq_device, excitation_settings_2, checkDims=False)

					if self.ui.exModeComboBox.currentText() == 'CHIRP':
						excitation_settings_3 = {'chirpStartFreqH': self.ui.chirpStartFreqHVaue.value(), 'chirpStopFreqH': self.ui.chirpStopFreqHVaue.value(),
												 'chirpStartFreqV': self.ui.chirpStartFreqVVaue.value(), 'chirpStopFreqV': self.ui.chirpStopFreqVVaue.value(),
												 'exLength': self.ui.exLengthValue.value()}
						self.japc.setParam('%s/Setting' % bbq_device, excitation_settings_3)

				print('BBQ settings updated.')
				self.logger.info('BBQ settings updated.')

		except jpype.java.lang.Exception as ex:
			print('BBQ setting failed. Here is the exception:')
			print(ex.printStackTrace())
			self.logger.warning('BBQ setting failed. Here is the exception:')
			self.logger.warning(str(ex.printStackTrace()))

	def interrupt_all_processes(self):
		# to be checked again (?) # TBA
		# self.executor.cancel()
		# self.executor.signalsBlocked()
		self.executor.clear()
		print('All active jobs were interrupted.')
		self.logger.warning('All active jobs were interrupted.')
		self.lock_measurement_buttons(True)
		self.meas.interrupt_measurement()
		self.statusBar().showMessage('Ready.')

	def check_beta_from_automatic_fit(self, result):
		data_analysis_quad_dict, output_files = result

		beta_error = []
		for quad in data_analysis_quad_dict:
			beta_error.extend(data_analysis_quad_dict[quad]['betay_meas_err'])

		if max(beta_error) < self.ui.BetaFittedErrorThresholdValue.value():
			print('Reached specified tolerance in the error of the beta-from-fit.')
			print('Cancelling the rest of the measurements')
			self.interrupt_all_processes()

	def show_hide_BBQ_ex_settings(self):

		if self.ui.exModeComboBox.currentText() == 'NONE':
			self.ui.exEnabledHGroupBox.hide()
			self.ui.exEnabledVGroupBox.hide()
			self.ui.exPatternLabel.hide()
			self.ui.exPatternComboBox.hide()
			self.ui.exMaskTuneLabel.hide()
			self.ui.exMaskTuneComboBox.hide()
			self.ui.ExAdvancedGroupBox.hide()
			self.ui.ChirpFreqGroupBox.hide()
		elif self.ui.exModeComboBox.currentText() == 'PULSE':
			self.ui.exEnabledHGroupBox.show()
			self.ui.exEnabledVGroupBox.show()
			self.ui.exPatternLabel.show()
			self.ui.exPatternComboBox.show()
			self.ui.exMaskTuneLabel.show()
			self.ui.exMaskTuneComboBox.show()
			if self.ui.exPatternComboBox.currentText() == 'ADVANCED':
				self.ui.ExAdvancedGroupBox.show()
			else:
				self.ui.ExAdvancedGroupBox.hide()
			self.ui.ChirpFreqGroupBox.hide()
		elif self.ui.exModeComboBox.currentText() == 'CHIRP':
			self.ui.exEnabledHGroupBox.show()
			self.ui.exEnabledVGroupBox.show()
			self.ui.exPatternLabel.show()
			self.ui.exPatternComboBox.show()
			self.ui.exMaskTuneLabel.show()
			self.ui.exMaskTuneComboBox.show()
			if self.ui.exPatternComboBox.currentText() == 'ADVANCED':
				self.ui.ExAdvancedGroupBox.show()
			else:
				self.ui.ExAdvancedGroupBox.hide()
			self.ui.ChirpFreqGroupBox.show()

	def drop_intensity_points(self):

		print('Dropping points with intensities lower than: %1.2f 1E10 ppb...' % self.ui.IntensityThresholdValue.value())
		intensity_mask = np.where(self.master_table['intensity_at_last_ctime'].values < self.ui.IntensityThresholdValue.value())
		files_with_low_intensity = set(np.unique(self.master_table['file'].values[intensity_mask]))
		indices_to_drop = [i for i in range(len(self.static_measurement_file_list)) if self.static_measurement_file_list[i].split("/")[-1] in files_with_low_intensity]

		# update master table
		print('Updating master table...')
		self.master_table = self.master_table.drop(list(list(intensity_mask)[0]))

		# Deleting files...
		if self.ui.AlsoDeleteDatafilesCheckBox.isChecked():
			print('Also (permanently) deleting the saved datafiles...')
			import os
			files_to_delete = [i for j, i in enumerate(self.static_measurement_file_list) if j in indices_to_drop]
			for file in files_to_delete:
				os.remove(file)

		# Drop file from file_list
		self.static_measurement_file_list = [i for j, i in enumerate(self.static_measurement_file_list) if j not in indices_to_drop]

		# Re-write the loaded data list
		self.static_measurement_model_list = QStandardItemModel(self.ui.LoadedSingleCycleDataList)
		for output_file in self.static_measurement_file_list:
			self.static_measurement_model_list.appendRow(QStandardItem(output_file.split("/")[-1]))
		self.ui.LoadedSingleCycleDataList.setModel(self.static_measurement_model_list)

		# Clear all plots and re-make them
		print('Re-doing plots...')
		for quad in self.parameters.quad_list:
			self.model_meas_beta_graphs[quad].clear()
			self.betas_from_fit_lists[quad].setModel(QStandardItemModel(self.betas_from_fit_lists[quad]))
		self.plot_raw_data_static()
		self.plot_intensity()
		self.update_parameters()
		print('Done!')

	# -----------------------------
	# Tune evolution tab

	def record_tune(self):
		from k_mod_app.tune_record_correction import TuneRecord
		self.tune_record = TuneRecord(self.parameters, self.simulation_parameters, self.machine,
									  self.output_path, self.ring, self.japc, self.lsa,
									  self.lsa_context, self.bbq_device_number, self.ui.RecordRawDataHV.isChecked())
		self.tune_record.start_tune_recording()
		self.statusBar().showMessage('Recording BBQ...')

	def stop_tune_record(self):
		self.tune_record.stop_tune_recording()
		self.statusBar().showMessage('Subscriptions stopped and cleared.')

	def load_tune_evolution_data(self):

		options = QFileDialog.Options()
		data = QFileDialog.getOpenFileNames(self, "Load periodic data", "%soutputs/%s/tune_evolution" % (self.output_path, self.parameters.machine_flag),
											"json(*.json)", options=options)

		if data:
			for datafile in data[0]:
				self.tune_evolution_file_list.append(datafile)
				self.tune_evolution_model_list.appendRow(QStandardItem(datafile.split("/")[-1]))
			self.ui.LoadedTuneEvolutionFileList.setModel(self.tune_evolution_model_list)

		self.plot_tune_evolution()
		self.update_parameters()

	def delete_tune_evolution_data(self):
		indices_to_remove = []
		for index in self.ui.LoadedTuneEvolutionFileList.selectedIndexes():
			indices_to_remove.append(index.row())

		self.tune_evolution_file_list = [i for j, i in enumerate(self.tune_evolution_file_list) if j not in indices_to_remove]
		self.tune_evolution_model_list = QStandardItemModel(self.ui.LoadedTuneEvolutionFileList)
		for output_file in self.tune_evolution_file_list:
			self.tune_evolution_model_list.appendRow(QStandardItem(output_file.split("/")[-1]))

		self.ui.LoadedTuneEvolutionFileList.setModel(self.tune_evolution_model_list)

		self.ui.HorizontalTuneEvolutionPlot.clear()
		self.ui.VerticalTuneEvolutionPlot.clear()
		self.plot_tune_evolution()
		self.update_parameters()
		# the connection has to be made again because self.periodic_measurement_model_list is redefined (to be reviewed)
		self.ui.LoadedTuneEvolutionFileList.selectionModel().selectionChanged.connect(self.highlight_tune_evolution_points)

	def plot_tune_evolution(self):

		self.ui.HorizontalTuneEvolutionPlot.clear()
		self.ui.VerticalTuneEvolutionPlot.clear()

		for datafile in self.tune_evolution_file_list:
			with open(datafile) as f:
				data = json.load(f)  # very slow TBA

			err = pg.ErrorBarItem(x=np.array(data['ctime']), #+ (data['ctime'][1] - data['ctime'][0]) * 0.5, # removed ctime-shift
								  y=np.array(data['estimatedTuneH']),
								  left=np.array([(data['ctime'][1] - data['ctime'][0]) * 0.5] * len(data['ctime']))*0,
								  right=np.array([(data['ctime'][1] - data['ctime'][0]) * 0.5] * len(data['ctime']))*0,
								  beam=0.0001)
			self.ui.HorizontalTuneEvolutionPlot.getPlotItem().addItem(err)
			self.ui.HorizontalTuneEvolutionPlot.plot([np.array(data['ctime'])],# + (data['ctime'][1] - data['ctime'][0]) * 0.5], # removed ctime-shift
													 data['estimatedTuneH'],
													 symbol='o', symbolSize=6, pen=None)
			err = pg.ErrorBarItem(x=np.array(data['ctime']), #+ (data['ctime'][1] - data['ctime'][0]) * 0.5, # removed ctime-shift
								  y=np.array(data['estimatedTuneV']),
								  left=np.array([(data['ctime'][1] - data['ctime'][0]) * 0.5] * len(data['ctime']))*0,
								  right=np.array([(data['ctime'][1] - data['ctime'][0]) * 0.5] * len(data['ctime']))*0,
								  beam=0.0001)
			self.ui.VerticalTuneEvolutionPlot.getPlotItem().addItem(err)
			self.ui.VerticalTuneEvolutionPlot.plot([np.array(data['ctime'])], # + (data['ctime'][1] - data['ctime'][0]) * 0.5], # removed ctime-shift
												   data['estimatedTuneV'],
												   symbol='o', symbolSize=6, pen=None)

	def highlight_tune_evolution_points(self):

		self.ui.HorizontalTuneEvolutionPlot.clear()
		self.ui.VerticalTuneEvolutionPlot.clear()
		self.TuneCorrectionDeltaKFPlot.clear()
		self.TuneCorrectionDeltaKDPlot.clear()
		self.plot_tune_evolution()

		clicked_index = self.ui.LoadedTuneEvolutionFileList.selectionModel().selection().indexes()[0].row()
		with open(self.tune_evolution_file_list[clicked_index]) as f:
			data = json.load(f)

		self.ui.HorizontalTuneEvolutionPlot.plot(np.array(data['ctime']) + (data['ctime'][1] - data['ctime'][0]) * 0.5,
												 np.array(data['estimatedTuneH']), pen=pg.mkPen(color='g', width=1.0))
		self.ui.VerticalTuneEvolutionPlot.plot(np.array(data['ctime']) + (data['ctime'][1] - data['ctime'][0]) * 0.5,
											   np.array(data['estimatedTuneV']), pen=pg.mkPen(color='g', width=1.0))

	def calculate_plot_mean_tune(self):
		# data must have the same ctimes... to be reviewed...
		self.Qs = {'H': {},
				   'V': {}}
		arx = []
		ary = []
		for datafile in self.tune_evolution_file_list:
			with open(datafile) as f:
				data = json.load(f)  # very slow TBA
			arx.append(np.array(data['estimatedTuneH']))
			ary.append(np.array(data['estimatedTuneV']))
		self.Qs['H']['mean'] = np.mean(arx, axis=0)
		self.Qs['V']['mean'] = np.mean(ary, axis=0)
		self.Qs['H']['err'] = np.std(ary, axis=0)
		self.Qs['V']['err'] = np.std(ary, axis=0)
		self.Qs['H']['ctimes'] = np.array(data['ctime']) #+ (data['ctime'][1] - data['ctime'][0]) * 0.5 # removed ctime-shift
		self.Qs['V']['ctimes'] = np.array(data['ctime']) #+ (data['ctime'][1] - data['ctime'][0]) * 0.5 # removed ctime-shift

		err = pg.ErrorBarItem(x=self.Qs['H']['ctimes'], y=self.Qs['H']['mean'],
							  top=self.Qs['H']['err'], bottom=self.Qs['H']['err'],
							  beam=0.1, pen={'color': 'r', 'width': 1})
		self.ui.HorizontalTuneEvolutionPlot.getPlotItem().addItem(err)
		self.ui.HorizontalTuneEvolutionPlot.plot(self.Qs['H']['ctimes'], self.Qs['H']['mean'],
												 pen=pg.mkPen(color='r', width=2.0), symbol='o', symbolSize=0.1, symbolBrush='r')
		err = pg.ErrorBarItem(x=self.Qs['V']['ctimes'], y=self.Qs['V']['mean'],
							  top=self.Qs['V']['err'], bottom=self.Qs['V']['err'],
							  beam=0.1, pen={'color': 'r', 'width': 1})
		self.ui.VerticalTuneEvolutionPlot.getPlotItem().addItem(err)
		self.ui.VerticalTuneEvolutionPlot.plot(self.Qs['V']['ctimes'], self.Qs['V']['mean'],
											   pen=pg.mkPen(color='r', width=2.0), symbol='o', symbolSize=0.1, symbolBrush='r')

	def calculate_tune_correction_deltas(self):

		from k_mod_app.tune_record_correction import TuneCorrection
		self.tune_correction = TuneCorrection(self.parameters, self.simulation_parameters, self.machine,
											  self.output_path, self.ring, self.japc, self.lsa,
											  self.lsa_context, self.bbq_device_number)
		correction_option = 0
		target_model_qh = 0
		target_model_qv = 0
		if self.ui.TuneCorrectionOption2GroupBox.isChecked():
			correction_option = 2
		elif self.ui.TuneCorrectionOption1GroupBox.isChecked():
			correction_option = 1
			target_model_qh = self.ui.TargetQHModelCorrectionValue.value()
			target_model_qv = self.ui.TargetQVModelCorrectionValue.value()
		else:
			print('Select tune correction method.')
		self.kfcorr, self.kdcorr = self.tune_correction.calculate_correction_trims(self.Qs['H']['mean'], self.Qs['V']['mean'],
		                                                                           self.Qs['H']['ctimes'], self.Qs['V']['ctimes'],
																				   option=correction_option,
																				   ctimeini=self.ui.SkipTunePointsValue.value(), ctimefin=self.ui.SkipTunePointsValue_2.value(),
																				   target_model_qh=target_model_qh, target_model_qv=target_model_qv)
		# Add ramp-up and ramp-down
		self.kfcorr_ctimes = list(self.Qs['H']['ctimes'])
		self.kdcorr_ctimes = list(self.Qs['V']['ctimes'])
		'''
		if self.kfcorr[-1] != 0.0:
			self.kfcorr_ctimes.append(max(self.kfcorr_ctimes) + 0.5)
			self.kfcorr.append(0.0)
		if self.kdcorr[-1] != 0.0:
			self.kdcorr_ctimes.append(max(self.kdcorr_ctimes) + 0.5)
			self.kdcorr.append(0.0)
		if ((self.kfcorr[0] != 0.0) and (self.kfcorr_ctimes[0] >= 275.5)):
			self.kfcorr_ctimes.insert(0, min(self.kfcorr_ctimes) - 0.5)
			self.kfcorr.insert(0, 0.0)
		if ((self.kdcorr[0] != 0.0) and (self.kdcorr_ctimes[0] >= 275.5)):
			self.kdcorr_ctimes.insert(0, min(self.kdcorr_ctimes) - 0.5)
			self.kdcorr.insert(0, 0.0)
		'''

		# Plots
		self.TuneCorrectionDeltaKFPlot.clear()
		source_trim_data = DataSource(x_data=np.array(self.kfcorr_ctimes), y_data=np.array(self.kfcorr))
		self.trim_curve_tune_correction_kf: accgraph.EditablePlotCurve = self.TuneCorrectionDeltaKFPlot.addCurve(data_source=source_trim_data,
																												 symbol='s', symbolSize=6, symbolBrush=0.0,
																												 pen=pg.mkPen(color=0.0, width=1.5, style=QtCore.Qt.DashLine))
		self.trim_curve_tune_correction_kf.selection.points_labeled = True

		self.TuneCorrectionDeltaKDPlot.clear()
		source_trim_data = DataSource(x_data=np.array(self.kdcorr_ctimes), y_data=np.array(self.kdcorr))
		self.trim_curve_tune_correction_kd: accgraph.EditablePlotCurve = self.TuneCorrectionDeltaKDPlot.addCurve(
			data_source=source_trim_data,
			symbol='s', symbolSize=6, symbolBrush=0.0, pen=pg.mkPen(color=0.0, width=1.5, style=QtCore.Qt.DashLine))
		self.trim_curve_tune_correction_kd.selection.points_labeled = True

	def send_tune_corrections_to_lsa(self):

		try:

			kfcorr_ctimes_edited = list(self.trim_curve_tune_correction_kf.xData)
			kfcorr_edited = list(self.trim_curve_tune_correction_kf.yData)
			kdcorr_ctimes_edited = list(self.trim_curve_tune_correction_kd.xData)
			kdcorr_edited = list(self.trim_curve_tune_correction_kd.yData)

			print('Reading LSA...')
			(ctime_kd_trm, kd_trm,
			 ctime_kd_delta, kd_delta,
			 ctime_kf_delta, kf_delta) = self.read_lsa()

			print('Adding old and new functions...')
			(a, b, c, d,
			 kdcorr_ctimes_edited_total, kdcorr_edited_total) = add_lsa_functions(ctime_kd_delta, kd_delta, kdcorr_ctimes_edited, kdcorr_edited)
			(a, b, c, d,
			 kfcorr_ctimes_edited_total, kfcorr_edited_total) = add_lsa_functions(ctime_kf_delta, kf_delta, kfcorr_ctimes_edited, kfcorr_edited)

			print('Sending total strengths...')
			self.tune_correction.set_tune_correction_strengths(list(kfcorr_ctimes_edited_total), list(kfcorr_edited_total),
															   list(kdcorr_ctimes_edited_total), list(kdcorr_edited_total))
			print('Tune correction strengths applied to LSA.')
		except Exception as ex:
			print('Tune correction strength setting failed. Here is the exception:')
			print(ex)


def main():
	app = QtWidgets.QApplication([])
	application = MainWindow()
	application.show()
	sys.exit(app.exec())


if __name__ == '__main__':
	main()
