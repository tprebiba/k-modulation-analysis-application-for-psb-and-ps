from k_mod_app.analysis_tools import *

enable_lsa = True
class Measurement():

    def __init__(self, parameters, simulation_parameters, machine, output_path, ring, japc, lsa, lsa_context, bbq_device_number, logger):

        self.parameters = parameters
        self.simulation_parameters = simulation_parameters
        self.machine = machine
        self.output_path = output_path
        self.ring = ring

        self.bbq_device, self.bct_device = self.machine.get_device_names(self.ring,0)

        self.japc = japc
        self.lsa = lsa
        self.lsa_context = lsa_context
        self.logger = logger


    def set_deltak(self, ctime_list, deltak_list, quad_parameter_id, setDescription):
        # In order to send correction trims to LSA
        # temporary solution
        # Ignore after v. 0.6.2
        # try:
        #     import pjlsa
        #     import pyjapc
        #     cycle = 'BC_PSB_2021' # hardcoded
        #     lsa = pjlsa.LSAClient(server='psb')
        #     print('LSA instantiated on the psb server (hardcoded) on the BC_PSB_2021 cycle (hardcoded).')
        #     print('Setting strengths.')
        #     japc = pyjapc.PyJapc(noSet=False)
        #     #user = "PSB.USER.MD1"
        #     #japc.setSelector(user)
        #     japc.rbacLogin()
        #
        #     with lsa.java_api():
        #         from cern.lsa.client import ServiceLocator, ContextService, ParameterService, TrimService
        #         from cern.lsa.domain.settings import TrimRequest
        #         from cern.accsoft.commons.value import ValueFactory
        #
        #         contextService = ServiceLocator.getService(ContextService)
        #         parameterService = ServiceLocator.getService(ParameterService)
        #         trimService = ServiceLocator.getService(TrimService)
        #
        #         cycle = contextService.findStandAloneCycle(cycle)
        #         parameter = parameterService.findParameterByName("logical.%s/K" % quad_parameter_id)
        #
        #         function = ValueFactory.createFunction(
        #             ctime_list,
        #             deltak_list
        #         )
        #
        #         trimRequest = TrimRequest.builder() \
        #             .setContext(cycle) \
        #             .addFunction(parameter, function) \
        #             .setDescription(setDescription) \
        #             .build()
        #         trimService.trimSettings(trimRequest)
        #
        # except Exception as ex:
        #     print('LSA setting failed. Here is the exception:')
        #     print(ex)
        #     print('Skipping this setting (data will not be saved)')
        #     self.lsa_failed = True
        pass


    def get_deltak(self, quad_parameter_id):
        # In order to send correction trims to LSA
        # temporary solution
        # Ignore after v. 0.6.2
        # import pjlsa
        # #import pyjapc
        # cycle = 'BC_PSB_2021'
        # lsa = pjlsa.LSAClient(server='psb')
        # print('LSA instantiated on the psb server (hardcoded) on the BC_PSB_2021 cycle (hardcoded).')
        # print('Reading strengths.')
        # #japc = pyjapc.PyJapc(noSet=False)
        # #user = "PSB.USER.MD1"
        # #japc.setSelector(user)
        # #japc.rbacLogin()
        # with lsa.java_api():
        #     from cern.lsa.client import ServiceLocator, ContextService, ParameterService, SettingService
        #     from cern.lsa.domain.settings import ContextSettingsRequest, Settings
        #     from java.util import Collections
        #
        #     contextService = ServiceLocator.getService(ContextService)
        #     parameterService = ServiceLocator.getService(ParameterService)
        #     settingService = ServiceLocator.getService(SettingService)
        #
        #     cycle = contextService.findStandAloneCycle(cycle)
        #     parameter = parameterService.findParameterByName("logical.%s/K" % quad_parameter_id)
        #     cycleSettings = settingService.findContextSettings(
        #         ContextSettingsRequest.byStandAloneContextAndParameters(cycle, Collections.singleton(parameter)))
        #
        #     function = Settings.getFunction(cycleSettings, parameter)
        #     ctimes = np.array(function.toXArray()).tolist()
        #     deltak = np.array(function.toYArray()).tolist()
        #
        # return ctimes, deltak
        pass


    def start_subscriptions(self):
        pass


    def stop_subscriptions(self):
        pass


    def setup_measurement(self, mode, ctimes_values_list, deltak_values_list, skip_cycles, quads_to_modulate):
        # initializes parameters needed for the callback function
        self.mode = mode
        #self.ctimes_ref, self.deltak_ref = self.get_deltak()  # reference values
        self.ctimes_ref, self.deltak_ref = [275, 805], [0.0, 0.0]
        self.acquisition_counter = 0
        self.deltak_values_list = deltak_values_list
        self.ctimes_values_list = ctimes_values_list
        self.skip_cycles = skip_cycles  # every skip_cycles skip one cycle
        self.skip_cycle_counter = 1
        self.quads_to_modulate = quads_to_modulate  # all the quads for the k-settings
        self.quad_id_counter = 0  # to keep track the quad_id


    def static_callback_function(self, parameter, value):

        self.quad_id = self.quads_to_modulate[self.quad_id_counter]

        self.mad = self.create_Madx_instance()
        self.machine.get_MADX_lattice(self.simulation_parameters, self.mad)

        ctime_arr = list(np.arange(self.parameters.ctime_min_static, self.parameters.ctime_max_static,float(self.parameters.ctime_step_static)))
        if (ctime_arr[-1]+float(self.parameters.ctime_step_static))<=self.parameters.ctime_max_static:
            ctime_arr.append(ctime_arr[-1]+float(self.parameters.ctime_step_static))
        self.measStamp = ctime_arr

        Qy_meas = []
        Qx_meas = []

        for ctime in ctime_arr:
            self.machine.set_model_strengths(self.simulation_parameters, self.mad, ctime)

            self.machine.set_deltak(self.mad, self.quad_id,
                                    self.deltak_values_list[self.acquisition_counter])
            Qx, Qy = self.get_tune_from_madx(self.mad)
            Qx_meas.append(Qx)
            Qy_meas.append(Qy)
            self.machine.set_deltak(self.mad, self.quad_id, 0)

        now = datetime.now()
        output_file = '%s_%s_R%s.json' % (self.quad_id, now.strftime("%Y.%m.%d-%H.%M.%S.%f"), self.ring)

        metadata = {'file': output_file,
                    'machine': self.parameters.machine_flag,
                    'quad': self.quad_id,
                    'mode': 'static',
                    'cycleStamp': datetime.timestamp(now)}
        measurement = {'timestamp': datetime.timestamp(now),
                       'ctime': list(ctime_arr),
                       'deltak': list([self.deltak_values_list[self.acquisition_counter]] * len(list(ctime_arr))),
                       'Qx_meas': list(Qx_meas),
                       'Qy_meas': list(Qy_meas),
                       'intensity_at_last_ctime': 0}
        self.data = {'metadata': metadata,
                     'measurement': measurement}

        if not os.path.exists('%soutputs/%s/static/raw_data/%s' % (self.output_path, self.parameters.machine_flag, now.strftime("%Y.%m.%d"))):
            os.makedirs('%soutputs/%s/static/raw_data/%s' % (self.output_path, self.parameters.machine_flag, now.strftime("%Y.%m.%d")))
        output_path = '%soutputs/%s/static/raw_data/%s/' % (self.output_path, self.parameters.machine_flag, now.strftime("%Y.%m.%d"))
        save_data_to_json(self.data, output_path, output_file)
        self.output_path_file = output_path + output_file
        self.measurement_is_done = True  # will trigger the get_measurement_when_done

        self.mad.input('exit;')
        self.acquisition_counter += 1
        if (self.acquisition_counter == len(self.deltak_values_list) and (self.quad_id_counter<len(self.quads_to_modulate))):
            self.acquisition_counter = 0
            self.quad_id_counter+=1

    def periodic_callback_function(self, parameter, value):
        '''
        Input:
        Function: tune measurements along one cycle for a periodic deltak variation
        Output: datafile of the measurement in the format: cpm_YYYMMDD-HHMMSS.csv
        '''

        self.quad_id = self.quads_to_modulate[self.quad_id_counter]

        self.mad = self.create_Madx_instance()
        self.machine.get_MADX_lattice(self.simulation_parameters, self.mad)

        ctime_arr = list(np.arange(self.parameters.ctime_min_periodic, self.parameters.ctime_max_periodic,self.parameters.ctime_step_periodic))
        if (ctime_arr[-1]+self.parameters.ctime_step_periodic)<=self.parameters.ctime_max_periodic:
            ctime_arr.append(ctime_arr[-1]+self.parameters.ctime_step_periodic)
        ctime_arr_norm = (np.array(ctime_arr) - self.parameters.ctime_min_periodic) / (
                self.parameters.ctime_max_periodic - self.parameters.ctime_min_periodic)
        deltak_arr = periodic_function_selector(self.parameters.periodic_modulation_function_flag,
                                                ctime_arr_norm,
                                                self.parameters.periodic_cycle_delta_k,
                                                self.parameters.modulation_freq,
                                                self.parameters.deltak_offset)

        Qx_meas = []
        Qy_meas = []
        for deltak, i in zip(deltak_arr, range(len(deltak_arr))):
            # self.machine.set_model_strengths(self.simulation_parameters, mad, ctime_arr[i])

            self.machine.set_deltak(self.mad, self.quad_id, deltak)
            Qx, Qy = self.get_tune_from_madx(self.mad)
            Qx_meas.append(Qx)
            Qy_meas.append(Qy)
        self.machine.set_deltak(self.mad, self.quad_id, 0)

        now = datetime.now()
        output_file = '%s_%s_R%s.json' % (self.quad_id, now.strftime("%Y.%m.%d-%H.%M.%S.%f"), self.ring)

        metadata = {'file': output_file,
                    'machine': self.parameters.machine_flag,
                    'quad': self.quad_id,
                    'mode': 'periodic',
                    'dk': self.parameters.periodic_cycle_delta_k,
                    'dk_freq': self.parameters.modulation_freq,
                    'dk_offset': self.parameters.deltak_offset,
                    'tmax_set': self.parameters.ctime_max_periodic}
        measurement = {'timestamp': datetime.timestamp(now),
                       'ctime': list(ctime_arr),
                       'deltak': list(deltak_arr),
                       'Qx_meas': list(Qx_meas),
                       'Qy_meas': list(Qy_meas),
                       'intensity_at_last_ctime': 0}
        self.data = {'metadata': metadata, 'measurement': measurement}

        if not os.path.exists('%soutputs/%s/periodic/raw_data/%s' % (
        self.output_path, self.parameters.machine_flag, now.strftime("%Y.%m.%d"))):
            os.makedirs('%soutputs/%s/periodic/raw_data/%s' % (self.output_path, self.parameters.machine_flag, now.strftime("%Y.%m.%d")))
        output_path = '%soutputs/%s/periodic/raw_data/%s/' % (self.output_path, self.parameters.machine_flag, now.strftime("%Y.%m.%d"))
        save_data_to_json(self.data, output_path, output_file)
        self.output_path_file = output_path + output_file
        self.measurement_is_done = True  # will trigger the get_measurement_when_done

        self.mad.input('exit;')
        self.acquisition_counter += 1
        if (self.acquisition_counter == 1 and (self.quad_id_counter < len(self.quads_to_modulate))):
            self.acquisition_counter = 0
            self.quad_id_counter += 1


    def get_measurement_when_done(self):

        self.measurement_is_done = False
        while self.measurement_is_done == False:
            if self.mode == 'static':
                self.static_callback_function("", "")
            elif self.mode == 'periodic':
                self.periodic_callback_function("", "")
        else:
            self.measurement_is_done = False
            return self.data, self.output_path_file, self.quad_id_counter, self.acquisition_counter

    def model_betay(self):

        ctimes_perturbed_array, betay_model_perturbed_array, betay_model_unperturbed = self.machine.precalculated_model_betay_values()

        mask = []
        n = 0
        for ctime in self.measStamp:
            if ctime in ctimes_perturbed_array:
                mask.append(ctimes_perturbed_array.index(ctime))
            if ctime > max(ctimes_perturbed_array):
                n += 1

        return (list(betay_model_perturbed_array[mask]) + [betay_model_unperturbed] * n)


    def set_quad_id(self, quad_id):
        self.parameters = self.parameters._replace(quad_id_static=quad_id)
        self.acquisition_counter = 0  # to reset the number of cycles to acquire


    def create_Madx_instance(self):

        from cpymad.madx import Madx
        mad = Madx()
        mad.options.echo = False
        mad.options.info = False
        mad.warn = False
        mad.options.warn = False

        return mad


    def interrupt_measurement(self):
        try:
            self.mad.input('exit;')
            #self.mad.input('stop;')
        except:
            pass
        print('Madx interrupted.')
        self.logger.warning('Madx interrupted.')


    def get_tune_from_madx(self, mad):

        twtable = self.machine.get_twiss_table(mad)
        # only the decimal part (like BBQ would measure)
        Qx_meas = twtable.summary['q1']-np.floor(twtable.summary['q1'])# + np.random.normal(scale=1.5e-3)
        Qy_meas = twtable.summary['q2']-np.floor(twtable.summary['q1'])# + np.random.normal(scale=1.5e-3)

        return Qx_meas, Qy_meas

