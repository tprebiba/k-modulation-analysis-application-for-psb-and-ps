from k_mod_app.analysis_tools import *
import time
import jpype

# If False no LSA reading/writting will be performed
enable_lsa = True

class Measurement():

	def __init__(self, parameters, simulation_parameters, machine, output_path, ring, japc, lsa, lsa_context, bbq_device_number, logger):

		self.parameters = parameters
		self.simulation_parameters = simulation_parameters
		self.machine = machine
		self.output_path = output_path
		self.ring = ring

		self.bbq_device, self.bct_device = self.machine.get_device_names(self.ring, bbq_device_number)

		self.japc = japc
		self.lsa  = lsa
		self.lsa_context = lsa_context
		self.logger = logger


	def set_deltak(self, ctime_list, deltak_list, quad_parameter_id, setDescription):
		try:
			cycle = self.lsa_context
			lsa = self.lsa
			with lsa.java_api():
				from cern.lsa.client import ServiceLocator, ContextService, ParameterService, TrimService
				from cern.lsa.domain.settings import TrimRequest
				from cern.accsoft.commons.value import ValueFactory

				contextService = ServiceLocator.getService(ContextService)
				parameterService = ServiceLocator.getService(ParameterService)
				trimService = ServiceLocator.getService(TrimService)

				cycle = contextService.findStandAloneCycle(cycle)
				parameter = parameterService.findParameterByName("logical.%s/K"%quad_parameter_id)

				function = ValueFactory.createFunction(
					ctime_list,
					deltak_list
				)

				trimRequest = TrimRequest.builder() \
					.setContext(cycle) \
					.addFunction(parameter, function) \
					.setDescription(setDescription) \
					.build()
				trimService.trimSettings(trimRequest)

		except Exception as ex:
			print('LSA setting failed. Here is the exception:')
			print(ex)
			print('Skipping this setting (data will not be saved)')
			self.logger.warning('LSA setting failed. Here is the exception:')
			self.logger.warning(str(ex))
			self.logger.info('Skipping this setting (data will not be saved)')
			self.lsa_failed = True


	def get_deltak(self, quad_parameter_id):

		cycle = self.lsa_context
		lsa = self.lsa
		with lsa.java_api():
			from cern.lsa.client import ServiceLocator, ContextService, ParameterService, SettingService
			from cern.lsa.domain.settings import ContextSettingsRequest, Settings
			from java.util import Collections

			contextService = ServiceLocator.getService(ContextService)
			parameterService = ServiceLocator.getService(ParameterService)
			settingService = ServiceLocator.getService(SettingService)

			cycle = contextService.findStandAloneCycle(cycle)
			parameter = parameterService.findParameterByName("logical.%s/K"%quad_parameter_id)
			cycleSettings = settingService.findContextSettings(
				ContextSettingsRequest.byStandAloneContextAndParameters(cycle, Collections.singleton(parameter)))

			function = Settings.getFunction(cycleSettings, parameter)
			ctimes = np.array(function.toXArray()).tolist()
			deltak = np.array(function.toYArray()).tolist()

		return ctimes, deltak


	def subscribe_to_bbq(self):
		self.set_bbq_acq_state(self.bbq_device, True)
		if self.mode == 'static':
			self.japc.subscribeParam("%s/Acquisition" % self.bbq_device, self.bbq_callback, getHeader=True)
		elif self.mode == 'periodic':
			self.japc.subscribeParam("%s/Acquisition" % self.bbq_device, self.bbq_callback_periodic, getHeader=True)
		print('Subscribed to %s.'%self.bbq_device)
		self.logger.info('Subscribed to %s.'%self.bbq_device)


	def subscribe_to_bct(self):
		#self.japc.subscribeParam("%s/Samples"%self.bct_device, self.bct_callback, getHeader=True)
		self.japc.subscribeParam("%s/Tuples" % self.bct_device, self.bct_callback, getHeader=True) # changed naming convention
		print('Subscribed to %s.'%self.bct_device)
		self.logger.info('Subscribed to %s.' % self.bct_device)


	def start_subscriptions(self):
		self.subscribe_to_bct()
		self.subscribe_to_bbq()
		print('Subscriptions started.')
		print('-----------------------')
		self.logger.info('Subscriptions started.')
		self.logger.info('-----------------------')
		self.japc.startSubscriptions()


	def stop_subscriptions(self):
		#self.set_bbq_acq_state(self.bbq_device, False)
		#self.japc.setParam('%s/Setting#exMode'%self.bbq_device, 0)  # set exMode to NONE when measurement is done
		#print('BBQ excitation mode set to NONE.')
		self.japc.stopSubscriptions()
		self.japc.clearSubscriptions()
		print('Subscriptions stopped.')
		self.logger.info('Subscriptions stopped.')


	def setup_measurement(self, mode, ctimes_values_list, deltak_values_list, skip_cycles, quads_to_modulate):
		# initializes parameters needed for the callback function
		self.mode = mode # flag to separate the callback functions according to the mode
		self.ctimes_ref = {} # the reference ctimes for all the quadrupoles to be modulated
		self.deltak_ref = {} # the reference deltak for all the quadrupoles to be modulated
		print('Reading reference values for all quads in LSA...')
		self.logger.info('Reading reference values for all quads in LSA...')
		if enable_lsa:
			for quad in quads_to_modulate:
				if self.parameters.machine_flag.lower() == 'psb':
					self.ctimes_ref[quad], self.deltak_ref[quad] = self.get_deltak('BR%s.%s_CORR'%(self.ring, quad.partition('.')[2])) # reference values
				elif self.parameters.machine_flag.lower() == 'ps':
					self.ctimes_ref[quad], self.deltak_ref[quad] = self.get_deltak('PR.%s' % (quad.partition('.')[2]))  # reference values
		else:
			for quad in quads_to_modulate:
				self.ctimes_ref[quad] = [275, 805]
				self.deltak_ref[quad] = [0.0, 0.0]
		print('Done.')
		self.logger.info('Done')
		self.acquisition_counter = 0
		self.deltak_values_list = deltak_values_list # target values (to be applied)
		self.ctimes_values_list = ctimes_values_list # target values (to be applied)
		self.skip_cycles = skip_cycles # the number of cycles to be skipped after the setting of lsa
		self.skip_cycle_counter = 0
		self.lsa_failed = False # flag to check skip cycle if lsa setting fails
		self.quads_to_modulate = quads_to_modulate # all the quads for the k-settings
		self.quad_id_counter = 0 # to keep track the quad_id
		print('Measurement parameters have been setup.')
		self.logger.info('Measurement parameters have been setup.')


	def bbq_callback(self, parameter, value, header):

		try:
			if header['isFirstUpdate']:
				print('Initial call of the bbq_callback. Ignoring.')
				self.logger.info('Initial call of the bbq_callback. Ignoring.')
			elif self.acquisition_counter>len(self.deltak_values_list):
				print('acquisition_counter > number of measurements. Skipping bbq_callback.')
				self.logger.info('acquisition_counter > number of measurements. Skipping bbq_callback.')
			else:
				# Get params from BBQ
				# instance fields: will be used by the get_measurement_when_done
				self.quad_id = self.quads_to_modulate[self.quad_id_counter]
				#self.estimatedTuneH = value['estimatedTuneH']
				#self.estimatedTuneV = value['estimatedTuneV']
				self.estimatedTuneH = value['tuneH']
				self.estimatedTuneV = value['tuneV']
				self.measStamp      = value['measStamp']
				try:
					self.measStamp = self.measStamp.tolist()
				except:
					# if only one measurement, BBQ gives a number instead of an array
					self.measStamp = [float(self.measStamp)]
				self.doneNbOfMeas   = value['doneNbOfMeas']
				self.bbq_cycleStamp = header['cycleStamp']
				print('BBQ readout.')
				#print(self.bbq_cycleStamp)
				self.logger.info('BBQ readout.')

				# Read LSA
				if ((enable_lsa) and (self.acquisition_counter <= len(self.deltak_values_list)) and (self.lsa_failed==False)):
					if (self.acquisition_counter > 0):
						# if acquisition_counter>0 the quad strength has been already set
						if self.parameters.machine_flag.lower() == 'psb':
							ctimes_list, deltak_list = self.get_deltak('BR%s.%s_CORR' % (self.ring, self.quad_id.partition('.')[2]))  # total strength
						elif self.parameters.machine_flag.lower() == 'ps':
							ctimes_list, deltak_list = self.get_deltak('PR.%s' % (self.quad_id.partition('.')[2]))  # total strength
						deltak_list = list(np.array(deltak_list) - np.array(self.deltak_old)) # substract the initial values
						measured_ctimes_indexes = [i for i in range(len(ctimes_list)) if ctimes_list[i] in self.measStamp]
						self.deltak_list = [deltak_list[i] for i in measured_ctimes_indexes] # keep deltak only for the measured ctimes
						print('LSA readout.')
						self.logger.info('LSA readout.')
					else:
						pass
				else:
					self.deltak_list = [self.deltak_values_list[self.acquisition_counter-1]]*(self.doneNbOfMeas)
					print('Getting deltak_list from input.')
					self.logger.info('Getting deltak_list from input.')

				# To trigger the get_measurement_when_done
				if ((self.acquisition_counter > 0) and (self.acquisition_counter <= len(self.deltak_values_list)) and (self.skip_cycle_counter == 0)):
					self.measurement_is_done = True
					self.measurement_counter = self.acquisition_counter

				# Set deltak on LSA
				if ((self.acquisition_counter < len(self.deltak_values_list)) and (self.skip_cycle_counter == 0)):
					# Send strengths to LSA
					# Apply new strength values on top of the old ones
					ctime_range = self.ctimes_values_list
					ctimes_new, deltak_new = trapezoidal_function(list(ctime_range),
																  self.deltak_values_list[self.acquisition_counter],
																  2, 2, 20, 20, 275) # to be reviewed #TBA
					(ctimes_old, self.deltak_old,
					 ctimes_new, deltak_new,
					 ctimes_total, deltak_total) = add_lsa_functions(self.ctimes_ref[self.quad_id], self.deltak_ref[self.quad_id], ctimes_new,deltak_new)
					self.lsa_failed=False
					try:
						if enable_lsa:
							if self.parameters.machine_flag.lower() == 'psb':
								self.set_deltak(ctimes_total, deltak_total,
												'BR%s.%s_CORR' % (self.ring, self.quad_id.partition('.')[2]),
												'K-mod shift %+1.4f between C%d and C%1d.' % (self.deltak_values_list[self.acquisition_counter],
																							  ctime_range[0],ctime_range[-1]))
							elif self.parameters.machine_flag.lower() == 'ps':
								self.set_deltak(ctimes_total, deltak_total,'PR.%s' % (self.quad_id.partition('.')[2]),
												'K-mod shift %+1.4f between C%d and C%1d.' % (self.deltak_values_list[self.acquisition_counter],
												ctime_range[0], ctime_range[-1]))
						print('LSA set to new value: K-mod shift %+1.4f between C%d and C%1d.'%(self.deltak_values_list[self.acquisition_counter],
																					  ctime_range[0],ctime_range[-1]))
						self.logger.info('LSA set to new value: K-mod shift %+1.4f between C%d and C%1d.'%(self.deltak_values_list[self.acquisition_counter],
																					  ctime_range[0],ctime_range[-1]))
					except:
						print('LSA was not set properly. Ignoring measurement.')
						self.logger.warning('LSA was not set properly. Ignoring measurement.')

				elif ((self.acquisition_counter == (len(self.deltak_values_list))) and (self.skip_cycle_counter == 0)):
					if enable_lsa:
						# reset strength
						if self.parameters.machine_flag.lower() == 'psb':
							self.set_deltak(self.ctimes_ref[self.quad_id], self.deltak_ref[self.quad_id], 'BR%s.%s_CORR'%(self.ring, self.quad_id.partition('.')[2]),
											'K-mod restored initial value.')
						elif self.parameters.machine_flag.lower() == 'ps':
							self.set_deltak(self.ctimes_ref[self.quad_id], self.deltak_ref[self.quad_id],'PR.%s' % (self.quad_id.partition('.')[2]),
											'K-mod restored initial value.')
					print('LSA set to initial value.')
					self.logger.info('LSA set to initial value')

				# the logic
				if self.skip_cycle_counter < self.skip_cycles:
					self.skip_cycle_counter+=1
					if self.acquisition_counter == len(self.deltak_values_list):
						if self.quad_id_counter<len(self.quads_to_modulate):
							self.quad_id_counter += 1
							self.acquisition_counter = 0
							self.skip_cycle_counter = 0
							print('Changing modulating quadrupole.')
							self.logger.info('Changing modulating quadrupole.')
						else:
							self.acquisition_counter += 1
							print('Exiting callbacks.')
							self.logger.info('Exiting callbacks')
					else:
						print('Skipping next cycle.')
						self.logger.info('Skipping next cycle.')
				elif self.skip_cycle_counter == self.skip_cycles:
					self.acquisition_counter += 1
					self.skip_cycle_counter = 0
					if (self.acquisition_counter > len(self.deltak_values_list) and (self.quad_id_counter<len(self.quads_to_modulate))):
						# when the measurement in one quad is finished for all the cycles, go to the next quad and repeat
						self.quad_id_counter += 1
						self.acquisition_counter = 0
				print('-----------------------')
				self.logger.info('-----------------------')

		except jpype.java.lang.Exception as ex:
			print('BBQ callback failed. Here is the exception:')
			print(ex)
			self.logger.error('BBQ callback failed. Here is the exception:')
			self.logger.error(str(ex))


	def bbq_callback_periodic(self, parameter, value, header):

		try:
			if header['isFirstUpdate']:
				print('Initial call of the bbq_callback. Ignoring.')
				self.logger.info('Initial call of the bbq_callback. Ignoring.')
			elif self.quad_id_counter>len(self.quads_to_modulate):
				print('acquisition_counter > 1. Skipping bbq_callback.')
				self.logger.info('acquisition_counter > 1. Skipping bbq_callback.')
			else:
				# Get params from BBQ
				# instance fields: will be used by the get_measurement_when_done
				self.quad_id = self.quads_to_modulate[self.quad_id_counter]
				self.estimatedTuneH = value['estimatedTuneH']
				self.estimatedTuneV = value['estimatedTuneV']
				self.measStamp      = value['measStamp']
				try:
					self.measStamp = self.measStamp.tolist()
				except:
					# if only one measurement, BBQ gives a number instead of an array
					self.measStamp = [float(self.measStamp)]
				self.doneNbOfMeas   = value['doneNbOfMeas']
				self.bbq_cycleStamp = header['cycleStamp']
				print('BBQ readout.')
				self.logger.info('BBQ readout.')

				# Read LSA
				if ((enable_lsa) and (self.acquisition_counter <= 1) and (self.lsa_failed==False)):
					if (self.acquisition_counter > 0): # if acquisition_counter>0 the quad strength has already been set
						if self.parameters.machine_flag.lower() == 'psb':
							ctimes_list, deltak_list = self.get_deltak('BR%s.%s_CORR' % (self.ring, self.quad_id.partition('.')[2]))  # acquisition values (total strength)
						elif self.parameters.machine_flag.lower() == 'ps':
							ctimes_list, deltak_list = self.get_deltak('PR.%s' % (self.quad_id.partition('.')[2]))  # acquisition values (total strength)
						deltak_list = list(np.array(deltak_list) - np.array(self.deltak_old)) # from acquisition, subtract the reference values with the intermediate points
						measured_ctimes_indexes = [i for i in range(len(ctimes_list)) if ctimes_list[i] in self.measStamp]
						self.deltak_list = [deltak_list[i] for i in measured_ctimes_indexes] # keep deltak only for the measured ctimes
					else:
						pass
				else:
					self.deltak_list = self.deltak_values_list
				print('LSA readout.')
				self.logger.info('LSA readout.')

				# To trigger the get_measurement_when_done
				if ((self.acquisition_counter > 0) and (self.acquisition_counter <= 1) and (self.skip_cycle_counter == 0)):
					self.measurement_is_done = True
					self.measurement_counter = self.acquisition_counter

				# Set deltak on LSA
				if ((self.acquisition_counter < 1) and (self.skip_cycle_counter == 0)):
					# Send strengths to LSA

					# target values with ramp-down and ramp-up values
					# in Python the simple assignment just copies the reference to the list; not an actual clone
					ctimes_new = self.ctimes_values_list.copy()
					deltak_new = self.deltak_values_list.copy()
					if deltak_new[0]!=0.0:
						ctimes_new.insert(0, min(ctimes_new)-1.0)
						deltak_new.insert(0, 0.0)
					if deltak_new[-1]!=0.0:
						ctimes_new.append(max(ctimes_new) + 1.0)
						deltak_new.append(0.0)

					# Apply new strength values on top of the old ones
					(ctimes_old, self.deltak_old, # reference values with intermediate points
					 ctimes_new, deltak_new,      # target values with intermediate points (and ramp-down/up values)
					 ctimes_total, deltak_total) = add_lsa_functions(self.ctimes_ref[self.quad_id], self.deltak_ref[self.quad_id], ctimes_new,deltak_new) # total values that are sent to lsa
					self.lsa_failed = False
					try:
						if enable_lsa:
							if self.parameters.machine_flag.lower() == 'psb':
								self.set_deltak(ctimes_total, deltak_total,
												'BR%s.%s_CORR' % (self.ring, self.quad_id.partition('.')[2]),
												'K-mod sinusoidal between C%d and C%1d.' % (self.ctimes_values_list[0],self.ctimes_values_list[-1]))
							elif self.parameters.machine_flag.lower() == 'ps':
								self.set_deltak(ctimes_total, deltak_total,'PR.%s' % (self.quad_id.partition('.')[2]),
												'K-mod sinusoidal between C%d and C%1d.' % (self.ctimes_values_list[0], self.ctimes_values_list[-1]))
						print('LSA set to new value: K-mod sinusoidal between C%d and C%1d.'%(self.ctimes_values_list[0],self.ctimes_values_list[-1]))
						self.logger.info('LSA set to new value: K-mod sinusoidal between C%d and C%1d.'%(self.ctimes_values_list[0],self.ctimes_values_list[-1]))
					except:
						print('LSA was not set properly. Ignoring measurement.')
						self.logger.warning('LSA was not set properly. Ignoring measurement.')

				elif ((self.acquisition_counter == 1) and (self.skip_cycle_counter == 0)):
					if enable_lsa:
						# reset strength
						if self.parameters.machine_flag.lower() == 'psb':
							self.set_deltak(self.ctimes_ref[self.quad_id], self.deltak_ref[self.quad_id], 'BR%s.%s_CORR'%(self.ring, self.quad_id.partition('.')[2]),
											'K-mod restored initial value.')
						elif self.parameters.machine_flag.lower() == 'ps':
							self.set_deltak(self.ctimes_ref[self.quad_id], self.deltak_ref[self.quad_id],'PR.%s' % (self.quad_id.partition('.')[2]),
											'K-mod restored initial value.')
					print('LSA set to initial value.')
					self.logger.info('LSA set to initial value.')

				# the logic
				if self.skip_cycle_counter < self.skip_cycles:
					self.skip_cycle_counter += 1
					if self.acquisition_counter == 1:
						if self.quad_id_counter < len(self.quads_to_modulate):
							self.quad_id_counter += 1
							self.acquisition_counter = 0
							self.skip_cycle_counter = 0
							print('Changing modulating quadrupole.')
							self.logger.info('Changing modulating quadrupole.')
						else:
							self.acquisition_counter += 1
							print('Exiting callbacks.')
							self.logger.info('Exiting callbacks.')
					else:
						print('Skipping next cycle.')
						self.logger.info('Skipping next cycle.')
				elif self.skip_cycle_counter == self.skip_cycles:
					self.acquisition_counter += 1
					self.skip_cycle_counter = 0
					if (self.acquisition_counter > 1 and (self.quad_id_counter < len(self.quads_to_modulate))):
						# when the measurement in one quad is finished for all the cycles, go to the next quad and repeat
						self.quad_id_counter += 1
						self.acquisition_counter = 0
				print('-----------------------')
				self.logger.info('-----------------------')

		except jpype.java.lang.Exception as ex:
			print('BBQ callback failed. Here is the exception:')
			print(ex)
			self.logger.error('BBQ callback failed. Here is the exception:')
			self.logger.error(str(ex))


	def bct_callback(self, parameter, value, header):

		try:
			if header['isFirstUpdate']:
				print('Initial call of the bct_callback. Ignoring.')
				self.logger.info('Initial call of the bct_callback. Ignoring.')
			elif self.acquisition_counter>len(self.deltak_values_list):
				print('acquisition_counter > number of measurements. Skipping bct_callback.')
				self.logger.info('acquisition_counter > number of measurements. Skipping bct_callback.')
			else:
				# instance fields: will be used by the get_measurement_when_done
				self.bct_cycleStamp       = header['cycleStamp']
				self.bct_firstSampleTime  = value['firstSampleTime']
				#self.bct_intensitySamples = value['samples']
				self.bct_intensitySamples = value['tuples'][0]['Y'] # changed naming convention
				print('BCT readout.')
				#print(self.bct_cycleStamp)
				self.logger.info('BCT readout.')
		except jpype.java.lang.Exception as ex:
			print('BCT callback failed. Here is the exception:')
			print(ex)
			self.logger.error('BCT callback failed. Here is the exception:')
			self.logger.error(str(ex))


	def get_measurement_when_done(self):

		self.measurement_is_done = False
		while self.measurement_is_done==False:
			time.sleep(0.1) # BCT callback is slower than BBQ callback, this dead time should be enough for the BCT value to update
			if self.measurement_is_done == 'interrupted':
				break
		else:

			time.sleep(3)  # BCT callback is slower than BBQ callback, this dead time should be enough for the BCT value to update
			# grab all instance fields for further analysis
			estimatedTuneH = self.estimatedTuneH
			estimatedTuneV = self.estimatedTuneV
			measStamp = self.measStamp # already processed before
			doneNbOfMeas = self.doneNbOfMeas
			bbq_cycleStamp = self.bbq_cycleStamp
			bct_cycleStamp = self.bct_cycleStamp
			#print(bct_cycleStamp, bbq_cycleStamp)
			bct_firstSampleTime = self.bct_firstSampleTime
			bct_intensitySamples = self.bct_intensitySamples
			deltak_list = self.deltak_list
			quad_id = self.quad_id
			quad_id_counter = self.quad_id_counter
			measurement_counter = self.measurement_counter
			lsa_failed_flag = self.lsa_failed

			# change datatypes for .json storage
			try:
				estimatedTuneH = estimatedTuneH.tolist()
				estimatedTuneV = estimatedTuneV.tolist()
			except:
				# if only one measurement, BBQ gives a number instead of an array
				estimatedTuneH = [float(estimatedTuneH)]
				estimatedTuneV = [float(estimatedTuneV)]
			doneNbOfMeas = float(doneNbOfMeas)
			bbq_cycleStamp = datetime.timestamp(bbq_cycleStamp)
			bct_cycleStamp = datetime.timestamp(bct_cycleStamp)
			bct_firstSampleTime = float(bct_firstSampleTime)
			bct_intensitySamples = bct_intensitySamples.tolist()
			#if bbq_cycleStamp == bct_cycleStamp:
			if (bbq_cycleStamp - bct_cycleStamp < 1): # relaxing the cycleStamp comparison
				ctimes = list(np.linspace(bct_firstSampleTime,
										  bct_firstSampleTime + len(bct_intensitySamples) - 1,
										  len(bct_intensitySamples)))
				last_measurement_index = ctimes.index(measStamp[-1])
				last_measurement_intensity = float(bct_intensitySamples[last_measurement_index])
				cycleStamp = bct_cycleStamp
			else:
				last_measurement_intensity = 0
				cycleStamp = 'not_synced'

			# Save .json
			now = datetime.now()
			output_file = '%s_%s_R%s.json' % (quad_id, now.strftime("%Y.%m.%d-%H.%M.%S.%f"),self.ring)

			metadata = {'file': output_file,
						'machine': self.parameters.machine_flag,
						'quad': quad_id,
						'mode': self.mode,
						'cycleStamp': cycleStamp}
			if self.mode == 'periodic':
				metadata.update({'dk': self.parameters.periodic_cycle_delta_k,
								 'dk_freq':self.parameters.modulation_freq,
								 'dk_offset': self.parameters.deltak_offset,
				                 'tmax_set': self.parameters.ctime_max_periodic})
			measurement = {'timestamp': datetime.timestamp(now),
						   'ctime': measStamp,
						   'deltak': deltak_list,
						   'Qx_meas': estimatedTuneH,
						   'Qy_meas': estimatedTuneV,
						   'doneNbOfMeas': doneNbOfMeas,
						   'intensity_at_last_ctime': last_measurement_intensity}
			data = {'metadata': metadata,
					'measurement': measurement}

			if lsa_failed_flag == False:
				if not os.path.exists('%soutputs/%s/%s/raw_data/%s' % (self.output_path, self.parameters.machine_flag, self.mode, now.strftime("%Y.%m.%d"))):
					os.makedirs('%soutputs/%s/%s/raw_data/%s' % (self.output_path, self.parameters.machine_flag, self.mode, now.strftime("%Y.%m.%d")))
				output_path = '%soutputs/%s/%s/raw_data/%s/' % (self.output_path, self.parameters.machine_flag, self.mode, now.strftime("%Y.%m.%d"))
				save_data_to_json(data, output_path, output_file)
				print('Output file saved to %s' % output_file)
				self.logger.info('Output file saved to %s' % output_file)
				return data, (output_path + output_file), quad_id_counter, measurement_counter
			elif lsa_failed_flag == True:
				print('LSA setting failed. Data were not saved.')
				self.logger.warning('LSA setting failed. Data were not saved.')
				return data, None, quad_id_counter, measurement_counter

			self.measurement_is_done = False


	# =================================================================================================================
	# Other

	def set_quad_id(self, quad_id):
		if self.mode == 'static':
			self.parameters = self.parameters._replace(quad_id_static=quad_id)
		elif self.mode == 'periodic':
			self.parameters = self.parameters._replace(quad_id_periodic=quad_id)
		self.acquisition_counter = 0 # to reset the number of cycles to acquire


	def interrupt_measurement(self):
		self.japc.stopSubscriptions()
		self.japc.clearSubscriptions()
		self.measurement_is_done = 'interrupted'
		print('All japc subscriptions stopped and cleared.')
		self.logger.info('All japc subscriptions stopped and cleared.')
		#self.set_bbq_acq_state(self.bbq_device, False)
		if enable_lsa:
			# reset strength
			if self.parameters.machine_flag.lower() == 'psb':
				self.set_deltak(self.ctimes_ref[self.quad_id], self.deltak_ref[self.quad_id],'BR%s.%s_CORR' % (self.ring, self.quad_id.partition('.')[2]),
								'K-mod restored initial value.')
			elif self.parameters.machine_flag.lower() == 'ps':
				self.set_deltak(self.ctimes_ref[self.quad_id], self.deltak_ref[self.quad_id],'PR.%s' % (self.quad_id.partition('.')[2]),
								'K-mod restored initial value.')
			print('LSA set to initial value.')
			self.logger.info('LSA set to initial value.')


	def set_bbq_acq_state(self, bbq_device, boolean):
		bool_to_int = {True: 1, False: 0}
		self.japc.setParam('%s/Setting#acqState'%bbq_device, bool_to_int[boolean])
		print('%s/Setting#acqState set to %s'%(bbq_device, boolean))
		self.logger.info('%s/Setting#acqState set to %s'%(bbq_device, boolean))
