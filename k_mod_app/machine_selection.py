from PyQt5.QtWidgets import *
import sys

def machine_selection():

	# Terminal-based machine selection
	if len(sys.argv)>1:

		if sys.argv[1].upper()=='PS':
			machine = 'PS'
			import k_mod_app.ps as machine_functions
			ring = ""
		elif str(sys.argv[1].upper())[0:3] =='PSB':
			machine = 'PSB'
			import k_mod_app.psb as machine_functions
			ring = sys.argv[1][-1]

		output_path = './'
		mode = ' in standard mode'
		import k_mod_app.real_measurement as measurement_mode

		if len(sys.argv) > 2:
			try:
				if sys.argv[2]=='default':
					output_path = './'
				else:
					output_path = sys.argv[2]
			except:
				print('python -m k_mod_app machine path')

			if len(sys.argv) > 3:
				if sys.argv[3] == 'expert':
					mode = ' in expert mode'
					import k_mod_app.measurement as measurement_mode
				elif sys.argv[3] == 'standard':
					mode = ''
					import k_mod_app.real_measurement as measurement_mode

		print('%s%s is selected%s.' % (machine, ring, mode))
		print('Path to output: %s' % output_path)
		return machine_functions, ring, measurement_mode, output_path


	# GUI-based machine selection
	else:

		d = QDialog()
		d.setWindowTitle("K-modulation app")

		wid = QWidget(d)
		grid = QGridLayout()

		MainText = QLabel()
		MainText.setText("Select the CERN accelerator:")
		MainText.setWordWrap(True)
		grid.addWidget(MainText, 0, 0, 1, 2)

		acceleratorButtonsGroup = QButtonGroup()
		PSB1Button = QRadioButton("PSB Ring 1")
		PSB1Button.setChecked(True)
		acceleratorButtonsGroup.addButton(PSB1Button)
		grid.addWidget(PSB1Button, 1, 0)
		PSB2Button = QRadioButton("PSB Ring 2")
		acceleratorButtonsGroup.addButton(PSB2Button)
		grid.addWidget(PSB2Button, 2, 0)
		PSB3Button = QRadioButton("PSB Ring 3")
		acceleratorButtonsGroup.addButton(PSB3Button)
		grid.addWidget(PSB3Button, 3, 0)
		PSB4Button = QRadioButton("PSB Ring 4")
		acceleratorButtonsGroup.addButton(PSB4Button)
		grid.addWidget(PSB4Button, 4, 0)
		PSButton = QRadioButton("PS")
		acceleratorButtonsGroup.addButton(PSButton)
		grid.addWidget(PSButton, 5, 0)

		OutputText = QLabel()
		OutputText.setText("Output directory: ")
		OutputText.setWordWrap(True)
		grid.addWidget(OutputText, 6, 0, 1, 2)

		PathToOutput = QLineEdit()
		PathToOutput.setMinimumWidth(250)
		grid.addWidget(PathToOutput, 7, 0, 1, 2)
		# output_path = './' # default
		output_path = '/afs/cern.ch/user/t/tprebiba/Desktop/'
		PathToOutput.setText(output_path)

		OKButton = QPushButton('OK')
		grid.addWidget(OKButton, 11,0,1,2)
		OKButton.clicked.connect(lambda:d.close())

		wid.setLayout(grid)
		d.show()
		d.adjustSize()
		d.exec_()

		machine = acceleratorButtonsGroup.checkedButton().text().split()
		if machine[0] == 'PSB':
			import k_mod_app.psb as machine_functions
			ring = machine[2]
		elif machine[0] == 'PS':
			import k_mod_app.ps as machine_functions
			ring = ""

		import k_mod_app.real_measurement as measurement_mode

		print('%s%s is selected.' % (machine[0], ring))
		print('Path to output: %s'%str(PathToOutput.text()))
		return machine_functions, ring, measurement_mode, str(PathToOutput.text())
